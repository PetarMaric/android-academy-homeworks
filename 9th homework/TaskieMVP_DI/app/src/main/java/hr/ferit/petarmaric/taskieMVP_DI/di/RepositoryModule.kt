package hr.ferit.petarmaric.taskieMVP_DI.di

import hr.ferit.petarmaric.taskieMVP_DI.repositories.LoginRepository
import hr.ferit.petarmaric.taskieMVP_DI.repositories.LoginRepositoryImpl
import hr.ferit.petarmaric.taskieMVP_DI.repositories.RegisterRepository
import hr.ferit.petarmaric.taskieMVP_DI.repositories.RegisterRepositoryImpl
import org.koin.core.qualifier.named
import org.koin.dsl.module


val repositoryModule = module {
    factory<LoginRepository> { LoginRepositoryImpl(get(named(RETROFIT_CLIENT))) }

    factory<RegisterRepository> { RegisterRepositoryImpl(get(named(RETROFIT_CLIENT))) }
}