package hr.ferit.petarmaric.taskieMVP_DI.domain.persistence

import hr.ferit.petarmaric.taskieMVP_DI.model.Task
import hr.ferit.petarmaric.taskieMVP_DI.model.TaskToSave
import hr.ferit.petarmaric.taskieMVP_DI.persistence.database.daos.TasksDao
import hr.ferit.petarmaric.taskieMVP_DI.persistence.database.daos.TasksToSaveDao


class PersistenceUseCaseImpl(private val tasksDao: TasksDao, private val tasksToSaveDao: TasksToSaveDao) :
    PersistenceUseCase {
    override fun save(task: Task): Task {
        val taskie = Task(
            title = task.title,
            content = task.content,
            taskPriority = task.taskPriority,
            isCompleted = false,
            isFavorite = false,
            retrofitTaskId = task.retrofitTaskId
        )
        tasksDao.storeTask(taskie)
        return taskie
    }

    override fun get(id: String): Task {
        return tasksDao.getTask(id)
    }

    override fun getTaskByRetrofitId(stringId: String): Task {
        return tasksDao.getTaskByRetrofitId(stringId)
    }

    override fun getAllTasks(): MutableList<Task> {
        return tasksDao.getAllTasks()
    }

    override fun storeAllTasks(taskList: MutableList<Task>) {
        tasksDao.storeAllTasks(taskList)
    }

    override fun getSortedTasks(): List<Task> {
        return tasksDao.getTasksOrderedByPriority()
    }

    override fun getAllPendingTasks(): MutableList<TaskToSave> {
        return tasksToSaveDao.getAllTasks()
    }

    override fun updateTask(task: Task) {
        tasksDao.updateTask(task)
    }

    override fun deleteTask(task: Task) {
        tasksDao.deleteTask(task)
    }

    override fun deleteAllTasks() {
        tasksDao.deleteAllTasks()
    }

    override fun deleteAllPendingTasks() {
        tasksToSaveDao.deleteAllPendingTasks()
    }

    override fun storePendingTask(task: TaskToSave) {
        tasksToSaveDao.storeTask(task)
    }
}