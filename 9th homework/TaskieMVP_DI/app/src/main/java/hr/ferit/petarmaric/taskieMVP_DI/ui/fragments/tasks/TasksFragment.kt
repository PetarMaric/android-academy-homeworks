package hr.ferit.petarmaric.taskieMVP_DI.ui.fragments.tasks

import android.app.AlertDialog
import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import hr.ferit.petarmaric.taskieMVP_DI.R
import hr.ferit.petarmaric.taskieMVP_DI.Taskie
import hr.ferit.petarmaric.taskieMVP_DI.common.displayToast
import hr.ferit.petarmaric.taskieMVP_DI.common.gone
import hr.ferit.petarmaric.taskieMVP_DI.common.showFragment
import hr.ferit.petarmaric.taskieMVP_DI.common.visible
import hr.ferit.petarmaric.taskieMVP_DI.model.BackendTask
import hr.ferit.petarmaric.taskieMVP_DI.model.request.DeleteTaskRequest
import hr.ferit.petarmaric.taskieMVP_DI.ui.adapters.recyclerview.TaskAdapter
import hr.ferit.petarmaric.taskieMVP_DI.ui.fragments.base.BaseFragment
import kotlinx.android.synthetic.main.fragment_tasks.*
import org.koin.android.ext.android.inject

class TasksFragment : BaseFragment(), TasksContract.View, AddTaskFragmentDialog.TaskAddedListener {

    private val presenter by inject<TasksContract.Presenter>()

    private val adapter by lazy { TaskAdapter { onItemSelected(it) } }

    companion object {
        fun newInstance(): Fragment {
            return TasksFragment()
        }

        const val TASKS_TO_SAVE = "TASKS_TO_SAVE"
        const val REQUEST_ERROR = "REQUEST_ERROR"
    }

    override fun deletionErrorWhileOffline() {
        activity?.displayToast(getString(R.string.tasksFragmentDeletionInfoWhileOfflineInfoText))
        adapter.notifyDataSetChanged()
    }

    override fun onTaskAdded(task: BackendTask) {
        adapter.addData(task)
    }

    override fun onNoInternetConnectionTaskFetchInfoText() {
        activity?.displayToast(getString(R.string.tasksFragmentLastStoredReponseMessage))
    }

    override fun showErrorToast() {
        this.activity?.displayToast(getString(R.string.errorToastText))
    }

    override fun onSwipeRefreshLayoutShow() {
        swipeRefreshLayout.isRefreshing = true
    }

    override fun onSwipeRefreshLayoutHide() {
        swipeRefreshLayout.isRefreshing = false
    }

    override fun noDataInfoShow() {
        noData?.visible()
    }

    override fun noDataInfoHide() {
        noData?.gone()
    }

    override fun onProgressShow() {
        progress?.visible()
    }

    override fun onProgressHide() {
        progress?.gone()
    }

    override fun onTaskListReceived(taskies: MutableList<BackendTask>) {
        adapter.setData(taskies)
    }

    override fun enableRefreshWheel() {
        swipeRefreshLayout.isRefreshing = true
    }

    override fun disableRefreshWheel() {
        swipeRefreshLayout.isRefreshing = false
    }

    override fun onTaskDeleteSuccess() {
        Taskie.getApplicationContext().displayToast(getString(R.string.taskDeletionText))
    }

    override fun onTaskDeleteFailure() {
        Taskie.getApplicationContext().displayToast(getString(R.string.errorToastText))
    }

    override fun getLayoutResourceId() = R.layout.fragment_tasks

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter.setView(this)
        initUi()
        initListeners()
    }

    private fun initUi() {
        progress?.visible()
        noData?.visible()

        tasksRecyclerView.layoutManager = LinearLayoutManager(context)
        tasksRecyclerView.adapter = adapter

        presenter.onTasksFragmentUiLoaded()
    }

    private fun setRecyclerViewItemTouchListener() {
        val itemTouchCallback = object : ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT) {
            override fun onMove(
                recyclerView: RecyclerView,
                viewHolder: RecyclerView.ViewHolder,
                target: RecyclerView.ViewHolder
            ): Boolean {
                return false
            }

            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
                val position = viewHolder.adapterPosition
                val task = adapter.getTaskAtPosition(position)
                showDeleteConfirmationDialog(task)
            }
        }
        val itemTouchHelper = ItemTouchHelper(itemTouchCallback)
        itemTouchHelper.attachToRecyclerView(tasksRecyclerView)

    }

    private fun showDeleteConfirmationDialog(task: BackendTask) {
        AlertDialog.Builder(context, R.style.AppCompatAlertDialogStyle)
            .setTitle("Do you want to delete task ${task.title} with ${task.taskPriority} priority?")
            .setPositiveButton("Delete") { _, _ ->
                val deleteRequest = DeleteTaskRequest(task.id)
                presenter.deleteTask(deleteRequest)
            }
            .setNegativeButton("Cancel") { _, _ ->
                adapter.notifyDataSetChanged()
            }
            .create()
            .show()
    }

    private fun initListeners() {
        addTask.setOnClickListener { addTask() }
        swipeRefreshLayout.setOnRefreshListener { refreshTasks() }
        setRecyclerViewItemTouchListener()
    }

    private fun onItemSelected(task: BackendTask) {
        activity?.showFragment(
            R.id.fragmentContainer,
            TaskDetailsFragment.newInstance(task.id),
            shouldAddToBackStack = true
        )
    }

    private fun refreshTasks() {
        presenter.onInitializedRefresh()
    }

    private fun addTask() {
        val dialog = AddTaskFragmentDialog.newInstance()
        dialog.setTaskAddedListener(this)
        dialog.show(childFragmentManager, dialog.tag)
    }

    fun getAdapterInstance() = adapter
}