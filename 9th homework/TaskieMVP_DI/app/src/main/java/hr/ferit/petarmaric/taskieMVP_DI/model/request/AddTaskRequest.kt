package hr.ferit.petarmaric.taskieMVP_DI.model.request

data class AddTaskRequest(val title: String, val content: String, val taskPriority: Int)