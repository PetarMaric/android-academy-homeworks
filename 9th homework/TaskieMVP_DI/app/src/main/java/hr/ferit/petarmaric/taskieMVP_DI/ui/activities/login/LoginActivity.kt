package hr.ferit.petarmaric.taskieMVP_DI.ui.activities.login

import android.content.Intent
import hr.ferit.petarmaric.taskieMVP_DI.R
import hr.ferit.petarmaric.taskieMVP_DI.common.displayToast
import hr.ferit.petarmaric.taskieMVP_DI.common.onClick
import hr.ferit.petarmaric.taskieMVP_DI.ui.activities.MainActivity
import hr.ferit.petarmaric.taskieMVP_DI.ui.activities.base.BaseActivity
import hr.ferit.petarmaric.taskieMVP_DI.ui.activities.register.RegisterActivity
import kotlinx.android.synthetic.main.activity_login.*
import org.koin.android.ext.android.inject

class LoginActivity : BaseActivity(), LoginContract.View {

    private val presenter by inject<LoginContract.Presenter>()

    override fun getLayoutResourceId(): Int = R.layout.activity_login

    override fun setUpUi() {

        presenter.setView(this)

        login.onClick { signInClicked() }
        goToRegister.onClick { goToRegistrationClicked() }
    }

    override fun onLoginSuccess() {
        this.displayToast(getString(R.string.successfulLoginText))
        val intent = Intent(this, MainActivity::class.java)
        startActivity(intent)
        finish()
    }

    override fun onLoginFailure() {
        this.displayToast(getString(R.string.errorToastText))
    }

    private fun signInClicked() {
        presenter.onLoginClicked(emailParam = email.text.toString(), passwordParam = password.text.toString())
    }

    private fun goToRegistrationClicked() {
        val intent = Intent(this, RegisterActivity::class.java)
        startActivity(intent)
        finish()
    }
}