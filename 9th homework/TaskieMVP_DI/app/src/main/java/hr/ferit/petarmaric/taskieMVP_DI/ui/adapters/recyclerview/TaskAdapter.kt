package hr.ferit.petarmaric.taskieMVP_DI.ui.adapters.recyclerview

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import hr.ferit.petarmaric.taskieMVP_DI.R
import hr.ferit.petarmaric.taskieMVP_DI.model.BackendTask

class TaskAdapter(private val onItemSelected: (BackendTask) -> Unit) : RecyclerView.Adapter<TaskHolder>() {

    private val data: MutableList<BackendTask> = mutableListOf()

    companion object {
        lateinit var recyclerViewInstance: RecyclerView
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TaskHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_task, parent, false)
        return TaskHolder(view)
    }

    override fun getItemCount() = data.size

    override fun onBindViewHolder(holder: TaskHolder, position: Int) {
        holder.bindData(data[position], onItemSelected)
    }

    override fun onAttachedToRecyclerView(recyclerView: RecyclerView) {
        super.onAttachedToRecyclerView(recyclerView)

        recyclerViewInstance = recyclerView
    }

    fun setData(data: List<BackendTask>) {
        this.data.clear()
        this.data.addAll(data)
        notifyDataSetChanged()
    }

    fun sortTasks() {
        data.sortByDescending { it.taskPriority }
        notifyDataSetChanged()
    }

    fun getTaskAtPosition(position: Int) = data[position]

    fun addData(item: BackendTask) {
        data.add(item)
        notifyItemInserted(data.size)
    }

    fun getData() = data
}





