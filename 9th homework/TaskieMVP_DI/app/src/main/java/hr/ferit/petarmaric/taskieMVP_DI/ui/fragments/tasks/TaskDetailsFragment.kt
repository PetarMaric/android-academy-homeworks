package hr.ferit.petarmaric.taskieMVP_DI.ui.fragments.tasks

import android.os.Bundle
import android.text.TextUtils
import android.view.View
import android.widget.TextView
import hr.ferit.petarmaric.taskieMVP_DI.R
import hr.ferit.petarmaric.taskieMVP_DI.Taskie
import hr.ferit.petarmaric.taskieMVP_DI.common.EXTRA_TASK_ID
import hr.ferit.petarmaric.taskieMVP_DI.common.displayToast
import hr.ferit.petarmaric.taskieMVP_DI.common.priorityFactory
import hr.ferit.petarmaric.taskieMVP_DI.common.showFragment
import hr.ferit.petarmaric.taskieMVP_DI.model.BackendTask
import hr.ferit.petarmaric.taskieMVP_DI.model.PriorityColor
import hr.ferit.petarmaric.taskieMVP_DI.model.request.TaskRequest
import hr.ferit.petarmaric.taskieMVP_DI.ui.adapters.spinneradapter.PriorityAdapter
import hr.ferit.petarmaric.taskieMVP_DI.ui.fragments.base.BaseFragment
import kotlinx.android.synthetic.main.fragment_task_details.*
import org.koin.android.ext.android.inject

class TaskDetailsFragment : BaseFragment(), TaskDetailsContract.View {

    private val presenter by inject<TaskDetailsContract.Presenter>()

    private var taskID = NO_TASK_STRING

    override fun getLayoutResourceId(): Int {
        return R.layout.fragment_task_details
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        arguments?.getString(EXTRA_TASK_ID)?.let { taskID = it }
        presenter.setView(this)
        tryDisplayTask(taskID)
    }

    private fun tryDisplayTask(id: String) {
        presenter.fetchTaskToDisplay(TaskRequest(id))
    }

    override fun onNoInternetConnectionTaskFetchInfoText() {
        activity?.displayToast(getString(R.string.taskDetailsNoInternetConnectionInfoText))
        navigateToTaskList()
    }

    override fun taskUpdatedInfo() {
        Taskie.getApplicationContext().displayToast(getString(R.string.taskDetailsSuccessfulTaskUpdateInfoText))
        navigateToTaskList()
    }

    private fun navigateToTaskList() {
        activity?.showFragment(R.id.fragmentContainer, TasksFragment.newInstance())
    }

    override fun displayError() {
        Taskie.getApplicationContext().displayToast(getString(R.string.errorToastText))
    }

    override fun displayTask(task: BackendTask) {

        detailsTaskTitle.setText(task.title, TextView.BufferType.EDITABLE)
        detailsTaskDescription.setText(task.content, TextView.BufferType.EDITABLE)

        spinnerPriorities.adapter = PriorityAdapter(
            Taskie.getApplicationContext(),
            R.layout.item_priority_spinner_item,
            // ne radi se ništa s repozitorijem te stoga nisam odvajao u prezenter
            PriorityColor.values().toList()
        )

        spinnerPriorities.setSelection(
            presenter.mapTaskPriority(task.taskPriority)
        )

        submitTask.setOnClickListener { updateTask() }
    }


    private fun updateTask() {
        if (isInputEmpty()) {
            context?.displayToast(getString(R.string.emptyFields))
            return
        }

        val title = detailsTaskTitle.text.toString()
        val description = detailsTaskDescription.text.toString()
        val priorityBackendPriorityTask = spinnerPriorities.priorityFactory()

        presenter.updateTask(
            title, description, priorityBackendPriorityTask.getValue()
        )
    }

    private fun isInputEmpty(): Boolean = TextUtils.isEmpty(detailsTaskTitle.text) || TextUtils.isEmpty(
        detailsTaskDescription.text
    )

    companion object {
        const val NO_TASK = -1
        const val NO_TASK_STRING = "No text"

        fun newInstance(taskId: String): TaskDetailsFragment {
            val bundle = Bundle().apply { putString(EXTRA_TASK_ID, taskId) }
            return TaskDetailsFragment().apply { arguments = bundle }
        }
    }
}
