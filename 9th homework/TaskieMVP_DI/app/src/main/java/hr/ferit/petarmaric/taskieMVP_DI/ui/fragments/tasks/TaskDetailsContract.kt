package hr.ferit.petarmaric.taskieMVP_DI.ui.fragments.tasks

import hr.ferit.petarmaric.taskieMVP_DI.model.BackendTask
import hr.ferit.petarmaric.taskieMVP_DI.model.request.TaskRequest

interface TaskDetailsContract {

    interface View {
        // zamjena za fetchSuccess
        fun displayTask(task: BackendTask)

        fun displayError()

        fun taskUpdatedInfo()

        fun onNoInternetConnectionTaskFetchInfoText()
    }

    interface Presenter {
        fun setView(view: TaskDetailsContract.View)

        fun fetchTaskToDisplay(taskRequest: TaskRequest)

        fun mapTaskPriority(taskPriority: Int): Int

        fun updateTask(title: String, description: String, priority: Int)
    }
}