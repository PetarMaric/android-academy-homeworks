package hr.ferit.petarmaric.taskieMVP_DI.ui.fragments.tasks

import hr.ferit.petarmaric.taskieMVP_DI.model.BackendTask
import hr.ferit.petarmaric.taskieMVP_DI.model.request.DeleteTaskRequest

interface TasksContract {

    interface View {
        fun onTaskListReceived(taskies: MutableList<BackendTask>)

        fun enableRefreshWheel()

        fun disableRefreshWheel()

        fun onTaskDeleteSuccess()

        fun onTaskDeleteFailure()

        fun onProgressShow()

        fun onProgressHide()

        fun noDataInfoShow()

        fun noDataInfoHide()

        fun onSwipeRefreshLayoutShow()

        fun onSwipeRefreshLayoutHide()

        fun showErrorToast()

        fun onNoInternetConnectionTaskFetchInfoText()

        fun deletionErrorWhileOffline()


    }

    interface Presenter {
        fun setView(view: TasksContract.View)

        fun onTasksFragmentUiLoaded()

        fun onInitializedRefresh()

        fun deleteTask(task: DeleteTaskRequest)

    }
}