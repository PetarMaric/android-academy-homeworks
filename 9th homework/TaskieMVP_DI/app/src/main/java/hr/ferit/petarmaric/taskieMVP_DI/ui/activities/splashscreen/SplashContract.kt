package hr.ferit.petarmaric.taskieMVP_DI.ui.activities.splashscreen

interface SplashContract {
    interface View {
        fun goToSignIn()

        fun goToHomescreen()
    }

    interface Presenter {
        fun setView(view: SplashContract.View)

        fun checkUserToken()
    }
}