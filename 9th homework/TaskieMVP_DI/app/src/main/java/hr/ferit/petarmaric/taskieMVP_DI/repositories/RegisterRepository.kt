package hr.ferit.petarmaric.taskieMVP_DI.repositories

import hr.ferit.petarmaric.taskieMVP_DI.model.request.UserDataRequest
import hr.ferit.petarmaric.taskieMVP_DI.model.response.RegisterResponse
import retrofit2.Call

interface RegisterRepository {
    fun registerUser(body: UserDataRequest): Call<RegisterResponse>
}