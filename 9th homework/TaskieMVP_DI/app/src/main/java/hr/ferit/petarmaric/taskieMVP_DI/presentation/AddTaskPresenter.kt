package hr.ferit.petarmaric.taskieMVP_DI.presentation

import android.util.Log
import hr.ferit.petarmaric.taskieMVP_DI.common.RESPONSE_BAD_REQUEST
import hr.ferit.petarmaric.taskieMVP_DI.common.RESPONSE_NOT_FOUND
import hr.ferit.petarmaric.taskieMVP_DI.common.RESPONSE_OK
import hr.ferit.petarmaric.taskieMVP_DI.common.SERVER_ERROR
import hr.ferit.petarmaric.taskieMVP_DI.domain.persistence.PersistenceUseCase
import hr.ferit.petarmaric.taskieMVP_DI.domain.preferences.PreferencesUseCase
import hr.ferit.petarmaric.taskieMVP_DI.model.BackendTask
import hr.ferit.petarmaric.taskieMVP_DI.model.PriorityColor
import hr.ferit.petarmaric.taskieMVP_DI.model.TaskToSave
import hr.ferit.petarmaric.taskieMVP_DI.model.request.AddTaskRequest
import hr.ferit.petarmaric.taskieMVP_DI.networking.connectionmanager.ConnectionManager
import hr.ferit.petarmaric.taskieMVP_DI.networking.interactors.TaskieInteractor
import hr.ferit.petarmaric.taskieMVP_DI.networking.showBadRequestToast
import hr.ferit.petarmaric.taskieMVP_DI.networking.showNotFoundToast
import hr.ferit.petarmaric.taskieMVP_DI.networking.showServerErrorToast
import hr.ferit.petarmaric.taskieMVP_DI.ui.fragments.tasks.AddTaskContract
import hr.ferit.petarmaric.taskieMVP_DI.ui.fragments.tasks.AddTaskFragmentDialog.Companion.TASK_TO_SAVE
import hr.ferit.petarmaric.taskieMVP_DI.ui.fragments.tasks.TasksFragment
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class AddTaskPresenter(
    private val interactor: TaskieInteractor,
    private val repository: PersistenceUseCase,
    private val prefs: PreferencesUseCase
) : AddTaskContract.Presenter {

    private lateinit var view: AddTaskContract.View

    companion object {
        val DEFAULT_PRIORITY = PriorityColor.values().first().toString()
        const val STORED_PRIORITY = "STORED_PRIORITY"
    }

    override fun setView(view: AddTaskContract.View) {
        this.view = view
    }

    override fun onAddTask(
        title: String,
        description: String,
        priority: PriorityColor,
        priorityBackendPriorityTask: Int
    ) {
        if (isInternetConnectionAvailable()) {
            interactor.save(
                AddTaskRequest(title, description, priorityBackendPriorityTask),
                addTaskCallback()
            )
        } else {
            val taskToSave = TaskToSave(title = title, content = description, priority = priority.getPriorityOrder())
            Log.d(TASK_TO_SAVE, taskToSave.toString())
            repository.storePendingTask(taskToSave)
            // ne mogu se prikazati dodani zadaci u listi ako su napravljeni offline zato što nisu tipa BackendTask i zato što još nisu spremljeni online
            view.onTaskSavedLocallyMessage()
        }
    }

    private fun addTaskCallback(): Callback<BackendTask> = object : Callback<BackendTask> {
        override fun onFailure(call: Call<BackendTask>?, t: Throwable?) {
            Log.d(TasksFragment.REQUEST_ERROR, t?.localizedMessage)
            Log.d(TasksFragment.REQUEST_ERROR, call?.request().toString())
        }

        override fun onResponse(call: Call<BackendTask>?, response: Response<BackendTask>) {
            if (response.isSuccessful) {
                when (response.code()) {
                    RESPONSE_OK -> handleOkResponse(response.body())
                    RESPONSE_BAD_REQUEST -> showBadRequestToast()
                    RESPONSE_NOT_FOUND -> showNotFoundToast()
                    SERVER_ERROR -> showServerErrorToast()
                    else -> handleSomethingWentWrong()
                }
            }
        }
    }

    private fun handleOkResponse(task: BackendTask?) = task?.run { view.onTaskStored(this) }

    private fun handleSomethingWentWrong() = view.onTaskAddFailure()

    private fun isInternetConnectionAvailable() = ConnectionManager.getNetworkInfo()?.isConnected ?: false

    override fun onFetchStoredPriority(): String {
        return prefs.getPrefsString(STORED_PRIORITY, DEFAULT_PRIORITY)
    }

    override fun onStorePriority(priority: PriorityColor) {
        prefs.storePrefs(STORED_PRIORITY, priority.toString())
    }

}