package hr.ferit.petarmaric.taskieMVP_DI.di

import hr.ferit.petarmaric.taskieMVP_DI.domain.login.LoginUseCase
import hr.ferit.petarmaric.taskieMVP_DI.domain.login.LoginUseCaseImpl
import hr.ferit.petarmaric.taskieMVP_DI.domain.persistence.PersistenceUseCase
import hr.ferit.petarmaric.taskieMVP_DI.domain.persistence.PersistenceUseCaseImpl
import hr.ferit.petarmaric.taskieMVP_DI.domain.preferences.PreferencesUseCase
import hr.ferit.petarmaric.taskieMVP_DI.domain.preferences.PreferencesUseCaseImpl
import hr.ferit.petarmaric.taskieMVP_DI.domain.register.RegisterUseCase
import hr.ferit.petarmaric.taskieMVP_DI.domain.register.RegisterUseCaseImpl
import hr.ferit.petarmaric.taskieMVP_DI.networking.interactors.TaskieInteractor
import hr.ferit.petarmaric.taskieMVP_DI.networking.interactors.TaskieInteractorImplementation
import org.koin.core.qualifier.named
import org.koin.dsl.module

val domainModule = module {
    factory<PreferencesUseCase> { PreferencesUseCaseImpl(get(named(SHARED_PREFERENCES))) }
    factory<LoginUseCase> { LoginUseCaseImpl(get()) }
    factory<RegisterUseCase> { RegisterUseCaseImpl(get()) }
    factory<TaskieInteractor> { TaskieInteractorImplementation(get(named(RETROFIT_CLIENT))) }
    factory<PersistenceUseCase> { PersistenceUseCaseImpl(get(named(TASKS_DAO)), get(named(TASKS_TO_SAVE_DAO))) }
}