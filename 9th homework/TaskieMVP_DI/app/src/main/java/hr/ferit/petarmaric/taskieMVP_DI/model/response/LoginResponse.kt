package hr.ferit.petarmaric.taskieMVP_DI.model.response

data class LoginResponse(val token: String? = "")