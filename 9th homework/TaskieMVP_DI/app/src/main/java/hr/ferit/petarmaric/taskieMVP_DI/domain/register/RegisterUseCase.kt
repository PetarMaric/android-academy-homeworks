package hr.ferit.petarmaric.taskieMVP_DI.domain.register

import hr.ferit.petarmaric.taskieMVP_DI.common.ErrorLambda
import hr.ferit.petarmaric.taskieMVP_DI.common.SuccessLambda
import hr.ferit.petarmaric.taskieMVP_DI.model.request.UserDataRequest
import hr.ferit.petarmaric.taskieMVP_DI.model.response.RegisterResponse

interface RegisterUseCase {
    fun execute(body: UserDataRequest, onSuccess: SuccessLambda<RegisterResponse>, onFailure: ErrorLambda)
}