package hr.ferit.petarmaric.taskieMVP_DI.ui.activities.login

interface LoginContract {

    interface View {
        fun onLoginSuccess()

        fun onLoginFailure()
    }

    interface Presenter {
        fun setView(view: LoginContract.View)

        fun onLoginClicked(emailParam: String, passwordParam: String)
    }
}