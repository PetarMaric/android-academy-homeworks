package hr.ferit.petarmaric.taskieMVP_DI.common

typealias SuccessLambda<T> = (T) -> Unit
typealias ErrorLambda = (Throwable) -> Unit