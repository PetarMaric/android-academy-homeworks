package hr.ferit.petarmaric.taskieMVP_DI.networking

import hr.ferit.petarmaric.taskieMVP_DI.R
import hr.ferit.petarmaric.taskieMVP_DI.Taskie
import hr.ferit.petarmaric.taskieMVP_DI.common.displayToast

fun showBadRequestToast() =
    Taskie.getApplicationContext().let { it.displayToast(it.getString(R.string.badRequestText)) }

fun showNotFoundToast() = Taskie.getApplicationContext().let { it.displayToast(it.getString(R.string.notFoundText)) }
fun showServerErrorToast() =
    Taskie.getApplicationContext().let { it.displayToast(it.getString(R.string.serverErrorText)) }

fun unidentifiedErrorToast() =
    Taskie.getApplicationContext().let { it.displayToast(it.getString(R.string.unidentifiedServerErrorText)) }