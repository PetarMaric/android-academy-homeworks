package hr.ferit.petarmaric.taskieMVP_DI.ui.adapters.fragment

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import hr.ferit.petarmaric.taskieMVP_DI.R
import hr.ferit.petarmaric.taskieMVP_DI.Taskie
import hr.ferit.petarmaric.taskieMVP_DI.ui.fragments.about.AboutApplication
import hr.ferit.petarmaric.taskieMVP_DI.ui.fragments.about.AboutAuthor

class AboutFragmentAdapter(fragmentManager: FragmentManager) : FragmentPagerAdapter(fragmentManager) {

    private val aboutFragments: List<Fragment> =
        listOf(AboutApplication.getInstance(), AboutAuthor.getInstance())

    private val titles = arrayOf(
        Taskie.getApplicationContext().getString(R.string.aboutApplicationFragmentTitle),
        Taskie.getApplicationContext().getString(
            R.string.aboutAuthorFragmentTitle
        )
    )

    override fun getItem(position: Int): Fragment {
        return aboutFragments[position]
    }

    override fun getCount(): Int = aboutFragments.size

    override fun getPageTitle(position: Int): CharSequence? = titles[position]


}