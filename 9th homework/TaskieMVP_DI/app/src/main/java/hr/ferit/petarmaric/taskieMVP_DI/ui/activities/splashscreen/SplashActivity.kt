package hr.ferit.petarmaric.taskieMVP_DI.ui.activities.splashscreen

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import hr.ferit.petarmaric.taskieMVP_DI.ui.activities.MainActivity
import hr.ferit.petarmaric.taskieMVP_DI.ui.activities.register.RegisterActivity
import org.koin.android.ext.android.inject

class SplashActivity : AppCompatActivity(), SplashContract.View {
    private val presenter by inject<SplashContract.Presenter>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        presenter.setView(this)
        checkPrefs()
    }

    private fun checkPrefs() {
        presenter.checkUserToken()
    }

    override fun goToSignIn() {
        startActivity(Intent(this, RegisterActivity::class.java))
        finish()
    }

    override fun goToHomescreen() {
        startActivity(Intent(this, MainActivity::class.java))
        finish()
    }
}