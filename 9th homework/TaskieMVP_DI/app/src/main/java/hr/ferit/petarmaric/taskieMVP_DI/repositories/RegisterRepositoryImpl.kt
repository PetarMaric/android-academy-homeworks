package hr.ferit.petarmaric.taskieMVP_DI.repositories

import hr.ferit.petarmaric.taskieMVP_DI.model.request.UserDataRequest
import hr.ferit.petarmaric.taskieMVP_DI.model.response.RegisterResponse
import hr.ferit.petarmaric.taskieMVP_DI.networking.TaskieApiService
import retrofit2.Call

class RegisterRepositoryImpl(private val taskieService: TaskieApiService) : RegisterRepository {
    override fun registerUser(body: UserDataRequest): Call<RegisterResponse> {
        return taskieService.register(body)
    }

}