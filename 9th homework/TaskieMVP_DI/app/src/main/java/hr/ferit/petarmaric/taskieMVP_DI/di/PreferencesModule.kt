package hr.ferit.petarmaric.taskieMVP_DI.di

import android.content.Context
import hr.ferit.petarmaric.taskieMVP_DI.BuildConfig
import hr.ferit.petarmaric.taskieMVP_DI.Taskie
import org.koin.android.ext.koin.androidContext
import org.koin.core.qualifier.named
import org.koin.dsl.module

const val sharedPreferencesFile = BuildConfig.APPLICATION_ID
const val SHARED_PREFERENCES = "shared_preferences"
const val KEY_USER_TOKEN = "user_token"

val preferencesModule = module {

    single(named(SHARED_PREFERENCES)) {
        androidContext().getSharedPreferences(sharedPreferencesFile, Context.MODE_PRIVATE)
    }

}