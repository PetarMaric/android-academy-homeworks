package hr.ferit.petarmaric.taskieMVP_DI.model.request

data class UpdateTaskRequest(val id: String, val title: String, val content: String, val taskPriority: Int,
                             val isFavorite: Boolean = false, val isCompleted: Boolean = false, val dueDate: String = "")