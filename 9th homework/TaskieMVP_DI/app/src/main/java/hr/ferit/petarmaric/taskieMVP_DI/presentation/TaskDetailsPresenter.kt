package hr.ferit.petarmaric.taskieMVP_DI.presentation

import android.util.Log
import hr.ferit.petarmaric.taskieMVP_DI.common.RESPONSE_BAD_REQUEST
import hr.ferit.petarmaric.taskieMVP_DI.common.RESPONSE_NOT_FOUND
import hr.ferit.petarmaric.taskieMVP_DI.common.RESPONSE_OK
import hr.ferit.petarmaric.taskieMVP_DI.common.SERVER_ERROR
import hr.ferit.petarmaric.taskieMVP_DI.model.BackendTask
import hr.ferit.petarmaric.taskieMVP_DI.model.request.TaskRequest
import hr.ferit.petarmaric.taskieMVP_DI.model.request.UpdateTaskRequest
import hr.ferit.petarmaric.taskieMVP_DI.networking.connectionmanager.ConnectionManager
import hr.ferit.petarmaric.taskieMVP_DI.networking.interactors.TaskieInteractor
import hr.ferit.petarmaric.taskieMVP_DI.networking.showBadRequestToast
import hr.ferit.petarmaric.taskieMVP_DI.networking.showNotFoundToast
import hr.ferit.petarmaric.taskieMVP_DI.networking.showServerErrorToast
import hr.ferit.petarmaric.taskieMVP_DI.ui.fragments.tasks.TaskDetailsContract
import hr.ferit.petarmaric.taskieMVP_DI.ui.fragments.tasks.TasksFragment
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class TaskDetailsPresenter(private val interactor: TaskieInteractor) : TaskDetailsContract.Presenter {

    private lateinit var view: TaskDetailsContract.View
    private lateinit var originalTask: BackendTask

    override fun updateTask(title: String, description: String, priority: Int) {
        interactor.update(
            UpdateTaskRequest(
                id = originalTask.id,
                title = title,
                content = description,
                taskPriority = priority
            ), updateTaskCallback()
        )
    }

    private fun updateTaskCallback(): Callback<BackendTask> = object : Callback<BackendTask> {
        override fun onFailure(call: Call<BackendTask>, t: Throwable) {
            Log.d(TasksFragment.REQUEST_ERROR, t?.localizedMessage)
            Log.d(TasksFragment.REQUEST_ERROR, call?.request().toString())
        }

        override fun onResponse(call: Call<BackendTask>, response: Response<BackendTask>) {

            if (response.isSuccessful) {
                when (response.code()) {
                    RESPONSE_OK -> handleUpdateOkResponse()
                    RESPONSE_BAD_REQUEST -> showBadRequestToast()
                    RESPONSE_NOT_FOUND -> showNotFoundToast()
                    SERVER_ERROR -> showServerErrorToast()
                    else -> handleSomethingWentWrong()
                }
            } else {
                handleSomethingWentWrong()
            }
        }

    }

    private fun handleUpdateOkResponse() {
        view.taskUpdatedInfo()
    }

    override fun mapTaskPriority(taskPriority: Int): Int = when (taskPriority) {
        1 -> 0
        2 -> 1
        else -> 2
    }

    override fun setView(view: TaskDetailsContract.View) {
        this.view = view
    }

    override fun fetchTaskToDisplay(taskRequest: TaskRequest) {
        if (isInternetConnectionAvaliable()) {
            interactor.getTask(taskRequest, getTaskCallback())
        } else {
            view.onNoInternetConnectionTaskFetchInfoText()
        }
    }

    private fun isInternetConnectionAvaliable() = ConnectionManager.getNetworkInfo()?.isConnected ?: false

    private fun getTaskCallback(): Callback<BackendTask> = object : Callback<BackendTask> {
        override fun onFailure(call: Call<BackendTask>, t: Throwable) {
            Log.d(TasksFragment.REQUEST_ERROR, t?.localizedMessage)
            Log.d(TasksFragment.REQUEST_ERROR, call?.request().toString())
        }

        override fun onResponse(call: Call<BackendTask>, response: Response<BackendTask>) {

            if (response.isSuccessful) {
                when (response.code()) {
                    RESPONSE_OK -> handleOkResponse(response?.body())
                    RESPONSE_BAD_REQUEST -> showBadRequestToast()
                    RESPONSE_NOT_FOUND -> showNotFoundToast()
                    SERVER_ERROR -> showServerErrorToast()
                    else -> handleSomethingWentWrong()
                }
            }
        }
    }

    private fun handleSomethingWentWrong() = view.displayError()

    private fun handleOkResponse(task: BackendTask?) {
        originalTask = task ?: BackendTask(
            content = "-1",
            id = "-1",
            userId = "-1",
            title = "-1",
            isFavorite = false,
            isCompleted = false,
            taskPriority = 0
        )
        task?.run { view.displayTask(this) }
    }

}