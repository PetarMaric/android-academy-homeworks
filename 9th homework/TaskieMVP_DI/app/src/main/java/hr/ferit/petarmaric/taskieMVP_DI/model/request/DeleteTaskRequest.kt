package hr.ferit.petarmaric.taskieMVP_DI.model.request

data class DeleteTaskRequest(val taskNoteId: String)