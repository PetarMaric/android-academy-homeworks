package hr.ferit.petarmaric.taskieMVP_DI.di

import hr.ferit.petarmaric.taskieMVP_DI.presentation.*
import hr.ferit.petarmaric.taskieMVP_DI.ui.activities.MainActivityContract
import hr.ferit.petarmaric.taskieMVP_DI.ui.activities.login.LoginContract
import hr.ferit.petarmaric.taskieMVP_DI.ui.activities.register.RegisterContract
import hr.ferit.petarmaric.taskieMVP_DI.ui.activities.splashscreen.SplashContract
import hr.ferit.petarmaric.taskieMVP_DI.ui.fragments.tasks.AddTaskContract
import hr.ferit.petarmaric.taskieMVP_DI.ui.fragments.tasks.TaskDetailsContract
import hr.ferit.petarmaric.taskieMVP_DI.ui.fragments.tasks.TasksContract
import org.koin.dsl.module

val presentationModule = module {
    factory<LoginContract.Presenter> { LoginPresenter(get(), get()) }
    factory<RegisterContract.Presenter> { RegisterPresenter(get()) }
    factory<SplashContract.Presenter> { SplashPresenter(get()) }
    factory<TasksContract.Presenter> { TasksPresenter(get(), get()) }
    factory<AddTaskContract.Presenter> { AddTaskPresenter(get(), get(), get()) }
    factory<TaskDetailsContract.Presenter> { TaskDetailsPresenter(get()) }
    factory<MainActivityContract.Presenter> { MainActivityPresenter(get()) }
}