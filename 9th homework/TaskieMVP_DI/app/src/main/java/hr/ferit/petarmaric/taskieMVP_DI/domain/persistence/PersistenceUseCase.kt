package hr.ferit.petarmaric.taskieMVP_DI.domain.persistence

import hr.ferit.petarmaric.taskieMVP_DI.model.Task
import hr.ferit.petarmaric.taskieMVP_DI.model.TaskToSave

interface PersistenceUseCase {
    fun save(task: Task): Task

    fun get(id: String): Task

    fun getTaskByRetrofitId(stringId: String): Task

    fun getSortedTasks(): List<Task>

    fun getAllTasks(): MutableList<Task>

    fun getAllPendingTasks(): MutableList<TaskToSave>

    fun storeAllTasks(taskList: MutableList<Task>)

    fun storePendingTask(task: TaskToSave)

    fun updateTask(task: Task)

    fun deleteTask(task: Task)

    fun deleteAllTasks()

    fun deleteAllPendingTasks()
}