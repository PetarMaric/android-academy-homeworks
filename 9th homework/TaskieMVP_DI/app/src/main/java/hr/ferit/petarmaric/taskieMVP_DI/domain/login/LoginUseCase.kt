package hr.ferit.petarmaric.taskieMVP_DI.domain.login

import hr.ferit.petarmaric.taskieMVP_DI.common.ErrorLambda
import hr.ferit.petarmaric.taskieMVP_DI.common.SuccessLambda
import hr.ferit.petarmaric.taskieMVP_DI.model.request.UserDataRequest
import hr.ferit.petarmaric.taskieMVP_DI.model.response.LoginResponse

interface LoginUseCase {
    fun execute(body: UserDataRequest, onSuccess: SuccessLambda<LoginResponse>, onFailure: ErrorLambda)
}