package hr.ferit.petarmaric.taskieMVP_DI.presentation

import android.util.Log
import hr.ferit.petarmaric.taskieMVP_DI.common.*
import hr.ferit.petarmaric.taskieMVP_DI.domain.persistence.PersistenceUseCase
import hr.ferit.petarmaric.taskieMVP_DI.model.BackendTask
import hr.ferit.petarmaric.taskieMVP_DI.model.request.AddTaskRequest
import hr.ferit.petarmaric.taskieMVP_DI.model.request.DeleteTaskRequest
import hr.ferit.petarmaric.taskieMVP_DI.model.response.DeleteResponse
import hr.ferit.petarmaric.taskieMVP_DI.model.response.GetTasksResponse
import hr.ferit.petarmaric.taskieMVP_DI.networking.connectionmanager.ConnectionManager
import hr.ferit.petarmaric.taskieMVP_DI.networking.interactors.TaskieInteractor
import hr.ferit.petarmaric.taskieMVP_DI.networking.showBadRequestToast
import hr.ferit.petarmaric.taskieMVP_DI.networking.showNotFoundToast
import hr.ferit.petarmaric.taskieMVP_DI.networking.showServerErrorToast
import hr.ferit.petarmaric.taskieMVP_DI.ui.fragments.tasks.TasksContract
import hr.ferit.petarmaric.taskieMVP_DI.ui.fragments.tasks.TasksFragment
import hr.ferit.petarmaric.taskieMVP_DI.ui.fragments.tasks.TasksFragment.Companion.REQUEST_ERROR
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class TasksPresenter(private val interactor: TaskieInteractor, private val repository: PersistenceUseCase) :
    TasksContract.Presenter {

    private lateinit var view: TasksContract.View

    private var tasks: MutableList<BackendTask> = mutableListOf()

    override fun setView(view: TasksContract.View) {
        this.view = view
    }

    private fun isInternetConnectionAvaliable() = ConnectionManager.getNetworkInfo()?.isConnected ?: false

    override fun onTasksFragmentUiLoaded() {
        if (isInternetConnectionAvaliable()) {
            storePendingTasks()
            getAllTasks()
        } else {
            fetchStoredTasks()
        }
    }

    private fun fetchStoredTasks() {
        val tasksFromDatabase = repository.getAllTasks()
        val tasksBackendTask = taskToBackendTaskMapper(tasksFromDatabase)
        view.onTaskListReceived(tasksBackendTask)
        view.onNoInternetConnectionTaskFetchInfoText()
    }

    override fun onInitializedRefresh() {
        if (isInternetConnectionAvaliable()) {
            getAllTasks()
        } else {
            view.disableRefreshWheel()
            view.onNoInternetConnectionTaskFetchInfoText()
        }
    }

    private fun storePendingTasks() {
        val pendingTasks = repository.getAllPendingTasks()
        if (pendingTasks.size == 0) {
            return
        }

        Log.d(TasksFragment.TASKS_TO_SAVE, pendingTasks.toString())

        pendingTasks.forEach {
            interactor.save(
                AddTaskRequest(
                    title = it.title,
                    content = it.content,
                    taskPriority = it.priority
                ), pendingTaskCallback()
            )
        }

        repository.deleteAllPendingTasks()
    }

    private fun pendingTaskCallback(): Callback<BackendTask> = object : Callback<BackendTask> {
        override fun onFailure(call: Call<BackendTask>, t: Throwable) {
            view.onProgressHide()

            Log.d(REQUEST_ERROR, t.localizedMessage)
            Log.d(REQUEST_ERROR, call.request().toString())

        }

        override fun onResponse(call: Call<BackendTask>, response: Response<BackendTask>) {
            view.onProgressHide()
            view.noDataInfoHide()
            view.disableRefreshWheel()

            if (response.isSuccessful) {
                when (response.code()) {
                    RESPONSE_OK -> Log.d("TASKS_TO_SAVE", "Saved task: {${response.body()}}\n\r")
                    RESPONSE_BAD_REQUEST -> showBadRequestToast()
                    RESPONSE_NOT_FOUND -> showNotFoundToast()
                    SERVER_ERROR -> showServerErrorToast()
                    else -> handleSomethingWentWrong()
                }
            } else {
                handleSomethingWentWrong()
            }
        }
    }

    private fun handleSomethingWentWrong() {
        view.disableRefreshWheel()
        view.showErrorToast()
    }

    private fun getAllTasks() {
        view.onProgressShow()
        view.enableRefreshWheel()
        interactor.getTasks(getTaskieCallback())
    }

    private fun getTaskieCallback(): Callback<GetTasksResponse> = object : Callback<GetTasksResponse> {
        override fun onFailure(call: Call<GetTasksResponse>?, t: Throwable?) {
            view.onProgressHide()
            view.disableRefreshWheel()

            Log.d(REQUEST_ERROR, t?.localizedMessage)
            Log.d(REQUEST_ERROR, call?.request().toString())
        }

        override fun onResponse(call: Call<GetTasksResponse>?, response: Response<GetTasksResponse>) {
            view.onProgressHide()
            view.noDataInfoHide()
            view.disableRefreshWheel()

            if (response.isSuccessful) {
                when (response.code()) {
                    RESPONSE_OK -> handleOkResponse(response)
                    RESPONSE_BAD_REQUEST -> showBadRequestToast()
                    RESPONSE_NOT_FOUND -> showNotFoundToast()
                    SERVER_ERROR -> showServerErrorToast()
                    else -> handleSomethingWentWrong()
                }
            } else {
                handleSomethingWentWrong()
            }
        }
    }

    private fun handleOkResponse(response: Response<GetTasksResponse>) {
        response.body()?.notes?.run {
            checkList(this)
            setTasks(this)
            view.onTaskListReceived(this)
            repository.deleteAllTasks()
            val backendTasks = backendTaskToTaskMapper(this)
            repository.storeAllTasks(backendTasks)
        }
    }

    private fun setTasks(tasksParam: MutableList<BackendTask>) {
        tasks.clear()
        tasks.addAll(tasksParam)
    }

    private fun checkList(notes: MutableList<BackendTask>) {
        if (notes.isEmpty()) {
            view.noDataInfoShow()
        } else {
            view.noDataInfoHide()
        }
    }

    override fun deleteTask(task: DeleteTaskRequest) {
        if (isInternetConnectionAvaliable()) {
            interactor.delete(task, deleteCallback())
        } else {
            view.deletionErrorWhileOffline()
        }

    }

    private fun deleteCallback(): Callback<DeleteResponse> = object : Callback<DeleteResponse> {
        override fun onFailure(call: Call<DeleteResponse>?, t: Throwable?) {
            Log.d(REQUEST_ERROR, t?.localizedMessage)
            Log.d(REQUEST_ERROR, call?.request().toString())
        }

        override fun onResponse(call: Call<DeleteResponse>?, response: Response<DeleteResponse>) {
            if (response.isSuccessful) {
                when (response.code()) {
                    RESPONSE_OK -> handleDeletionOkResponse()
                    RESPONSE_BAD_REQUEST -> showBadRequestToast()
                    RESPONSE_NOT_FOUND -> showNotFoundToast()
                    SERVER_ERROR -> showServerErrorToast()
                    else -> handleSomethingWentWrongWithDeletion()
                }
            }
        }
    }

    private fun handleDeletionOkResponse() {
        view.onTaskDeleteSuccess()
        getAllTasks()
    }

    private fun handleSomethingWentWrongWithDeletion() {
        view.onTaskDeleteFailure()
    }
}