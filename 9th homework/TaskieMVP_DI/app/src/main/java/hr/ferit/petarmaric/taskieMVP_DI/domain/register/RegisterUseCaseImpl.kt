package hr.ferit.petarmaric.taskieMVP_DI.domain.register

import android.util.Log
import hr.ferit.petarmaric.taskieMVP_DI.common.*
import hr.ferit.petarmaric.taskieMVP_DI.model.request.UserDataRequest
import hr.ferit.petarmaric.taskieMVP_DI.model.response.RegisterResponse
import hr.ferit.petarmaric.taskieMVP_DI.networking.showBadRequestToast
import hr.ferit.petarmaric.taskieMVP_DI.networking.showNotFoundToast
import hr.ferit.petarmaric.taskieMVP_DI.networking.showServerErrorToast
import hr.ferit.petarmaric.taskieMVP_DI.networking.unidentifiedErrorToast
import hr.ferit.petarmaric.taskieMVP_DI.repositories.RegisterRepository
import hr.ferit.petarmaric.taskieMVP_DI.ui.fragments.tasks.TasksFragment
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class RegisterUseCaseImpl(private val registerRepository: RegisterRepository) : RegisterUseCase {
    override fun execute(body: UserDataRequest, onSuccess: SuccessLambda<RegisterResponse>, onFailure: ErrorLambda) {
        registerRepository.registerUser(body).enqueue(object : Callback<RegisterResponse> {
            override fun onFailure(call: Call<RegisterResponse>?, t: Throwable?) {
                Log.d(TasksFragment.REQUEST_ERROR, t?.localizedMessage)
                Log.d(TasksFragment.REQUEST_ERROR, call?.request().toString())
            }

            override fun onResponse(call: Call<RegisterResponse>?, response: Response<RegisterResponse>) {
                if (response.isSuccessful) {
                    when (response.code()) {
                        RESPONSE_OK -> response.body()?.run(onSuccess)
                        RESPONSE_BAD_REQUEST -> showBadRequestToast()
                        RESPONSE_NOT_FOUND -> showNotFoundToast()
                        SERVER_ERROR -> showServerErrorToast()
                        else -> unidentifiedErrorToast()
                    }
                }

                response.errorBody()?.run { onFailure(IllegalStateException("Something has broken. Try again later.")) }
            }

        })
    }
}