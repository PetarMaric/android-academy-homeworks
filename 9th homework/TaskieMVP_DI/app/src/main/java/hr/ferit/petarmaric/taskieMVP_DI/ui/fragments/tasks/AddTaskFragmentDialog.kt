package hr.ferit.petarmaric.taskieMVP_DI.ui.fragments.tasks

import android.os.Bundle
import android.text.TextUtils.isEmpty
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.ArrayAdapter
import androidx.fragment.app.DialogFragment
import hr.ferit.petarmaric.taskieMVP_DI.R
import hr.ferit.petarmaric.taskieMVP_DI.Taskie
import hr.ferit.petarmaric.taskieMVP_DI.common.determinePriorityLocation
import hr.ferit.petarmaric.taskieMVP_DI.common.displayToast
import hr.ferit.petarmaric.taskieMVP_DI.common.priorityFactory
import hr.ferit.petarmaric.taskieMVP_DI.model.BackendTask
import hr.ferit.petarmaric.taskieMVP_DI.model.PriorityColor
import kotlinx.android.synthetic.main.fragment_dialog_new_task.*
import org.koin.android.ext.android.inject

class AddTaskFragmentDialog : DialogFragment(), AddTaskContract.View {
    private var taskAddedListener: TaskAddedListener? = null

    private val presenter by inject<AddTaskContract.Presenter>()

    companion object {
        fun newInstance(): AddTaskFragmentDialog {
            return AddTaskFragmentDialog()
        }

        const val TASK_TO_SAVE = "TASK_TO_SAVE"
    }

    interface TaskAddedListener {
        fun onTaskAdded(task: BackendTask)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NO_TITLE, R.style.FragmentDialogTheme)
        presenter.setView(this)
    }

    override fun onTaskSavedLocallyMessage() {
        Taskie.getApplicationContext().displayToast(getString(R.string.locallySavedTaskText))
    }

    override fun onTaskStored(task: BackendTask) {
        taskAddedListener?.onTaskAdded(task)
        dismiss()
    }

    override fun onTaskAddFailure() {
        this.activity?.displayToast(getString(R.string.errorToastText))
    }

    fun setTaskAddedListener(listener: TaskAddedListener) {
        taskAddedListener = listener
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_dialog_new_task, container)
    }

    override fun onStart() {
        super.onStart()
        dialog?.window?.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initUi()
        initListeners()
    }

    private fun initUi() {


        context?.let {
            prioritySelector.adapter =
                ArrayAdapter<PriorityColor>(it, android.R.layout.simple_spinner_dropdown_item, PriorityColor.values())

            val storedPriority = presenter.onFetchStoredPriority()

            prioritySelector.setSelection(determinePriorityLocation(storedPriority))


        }
    }

    private fun initListeners() {
        saveTaskAction.setOnClickListener { saveTask() }
    }

    private fun saveTask() {
        if (isInputEmpty()) {
            context?.displayToast(getString(R.string.emptyFields))
            return
        }

        val title = newTaskTitleInput.text.toString()
        val description = newTaskDescriptionInput.text.toString()
        val priority = prioritySelector.selectedItem as PriorityColor
        val priorityBackendPriorityTask = prioritySelector.priorityFactory()

        presenter.onAddTask(title, description, priority, priorityBackendPriorityTask.getValue())

        storePriority(priority)
        clearUi()
        dismiss()
    }

    private fun storePriority(priority: PriorityColor) {
        presenter.onStorePriority(priority)
    }

    private fun clearUi() {
        newTaskTitleInput.text.clear()
        newTaskDescriptionInput.text.clear()
        prioritySelector.setSelection(0)
    }

    private fun isInputEmpty(): Boolean = isEmpty(newTaskTitleInput.text) || isEmpty(newTaskDescriptionInput.text)
}