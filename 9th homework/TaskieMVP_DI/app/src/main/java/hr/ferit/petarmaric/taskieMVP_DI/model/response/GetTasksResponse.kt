package hr.ferit.petarmaric.taskieMVP_DI.model.response

import hr.ferit.petarmaric.taskieMVP_DI.model.BackendTask

data class GetTasksResponse(val notes: MutableList<BackendTask>? = mutableListOf())