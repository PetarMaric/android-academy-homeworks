package hr.ferit.petarmaric.taskieMVP_DI.repositories

import hr.ferit.petarmaric.taskieMVP_DI.model.request.UserDataRequest
import hr.ferit.petarmaric.taskieMVP_DI.model.response.LoginResponse
import retrofit2.Call

interface LoginRepository {
    fun loginUser(body: UserDataRequest): Call<LoginResponse>
}