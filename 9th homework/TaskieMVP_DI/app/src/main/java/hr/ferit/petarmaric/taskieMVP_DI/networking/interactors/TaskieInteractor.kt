package hr.ferit.petarmaric.taskieMVP_DI.networking.interactors

import hr.ferit.petarmaric.taskieMVP_DI.model.BackendTask
import hr.ferit.petarmaric.taskieMVP_DI.model.request.*
import hr.ferit.petarmaric.taskieMVP_DI.model.response.DeleteResponse
import hr.ferit.petarmaric.taskieMVP_DI.model.response.GetTasksResponse
import hr.ferit.petarmaric.taskieMVP_DI.model.response.LoginResponse
import hr.ferit.petarmaric.taskieMVP_DI.model.response.RegisterResponse
import retrofit2.Callback

interface TaskieInteractor {

    fun getTasks(taskieResponseCallback: Callback<GetTasksResponse>)

    fun getTask(request: TaskRequest, taskResponseCallback: Callback<BackendTask>)

    fun register(request: UserDataRequest, registerCallback: Callback<RegisterResponse>)

    fun login(request: UserDataRequest, loginCallback: Callback<LoginResponse>)

    fun save(request: AddTaskRequest, saveCallback: Callback<BackendTask>)

    fun update(request: UpdateTaskRequest, updateCallback: Callback<BackendTask>)

    fun delete(request: DeleteTaskRequest, deleteCallback: Callback<DeleteResponse>)
}