package hr.ferit.petarmaric.taskieMVP_DI.presentation

import hr.ferit.petarmaric.taskieMVP_DI.domain.register.RegisterUseCase
import hr.ferit.petarmaric.taskieMVP_DI.model.request.UserDataRequest
import hr.ferit.petarmaric.taskieMVP_DI.model.response.RegisterResponse
import hr.ferit.petarmaric.taskieMVP_DI.ui.activities.register.RegisterContract

class RegisterPresenter(private val registerUseCase: RegisterUseCase) : RegisterContract.Presenter {

    private lateinit var view: RegisterContract.View
    override fun setView(view: RegisterContract.View) {
        this.view = view
    }

    override fun onRegisterClicked(user: UserDataRequest) {
        registerUseCase.execute(user, ::onRegisterSuccess, ::onRegisterFailure)
    }

    private fun onRegisterSuccess(registerResponse: RegisterResponse) {
        view.onRegisterSuccess()
    }

    private fun onRegisterFailure(error: Throwable) {
        view.onRegisterFailure()
    }
}