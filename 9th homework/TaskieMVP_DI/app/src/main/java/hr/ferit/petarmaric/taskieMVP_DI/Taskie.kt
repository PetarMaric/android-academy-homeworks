package hr.ferit.petarmaric.taskieMVP_DI

import android.app.Application
import androidx.appcompat.app.AppCompatDelegate
import hr.ferit.petarmaric.taskieMVP_DI.di.*
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class Taskie : Application() {

    companion object {
        private lateinit var instance: Taskie


        fun getApplicationContext() = instance
    }

    override fun onCreate() {
        super.onCreate()
        instance = this
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true)

        startKoin {
            modules(
                listOf(
                    domainModule,
                    networkingModule,
                    preferencesModule,
                    repositoryModule,
                    presentationModule,
                    persistenceModule
                )
            )
            androidContext(this@Taskie)
        }
    }
}