package hr.ferit.petarmaric.taskieMVP_DI.ui.activities

import android.app.AlertDialog
import android.view.Menu
import android.view.MenuItem
import androidx.fragment.app.FragmentManager
import hr.ferit.petarmaric.taskieMVP_DI.R
import hr.ferit.petarmaric.taskieMVP_DI.Taskie
import hr.ferit.petarmaric.taskieMVP_DI.common.displayToast
import hr.ferit.petarmaric.taskieMVP_DI.model.BackendTask
import hr.ferit.petarmaric.taskieMVP_DI.ui.activities.base.BaseActivity
import hr.ferit.petarmaric.taskieMVP_DI.ui.adapters.recyclerview.TaskAdapter
import hr.ferit.petarmaric.taskieMVP_DI.ui.fragments.tasks.TasksFragment
import hr.ferit.petarmaric.taskieMVP_DI.ui.listeners.NavigationMenuListener
import kotlinx.android.synthetic.main.navigation_bottom_navigation.*
import org.koin.android.ext.android.inject

class MainActivity : BaseActivity(), MainActivityContract.View {
    private val presenter by inject<MainActivityContract.Presenter>()

    override fun getLayoutResourceId() = R.layout.activity_main

    override fun setUpUi() {
        showFragment(TasksFragment.newInstance())
        presenter.setView(this)
        bottomNavigationMaterial.setOnNavigationItemSelectedListener(NavigationMenuListener(this))
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        super.onCreateOptionsMenu(menu)
        menuInflater.inflate(R.menu.menu_options, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.sortTasksByPriority -> sortTasksByPriority()
            R.id.deleteAllTasks -> deleteAllTasks()
        }
        return super.onOptionsItemSelected(item)
    }

    private fun deleteAllTasks() {
        AlertDialog.Builder(this, R.style.AppCompatAlertDialogStyle)
            .setTitle(getString(R.string.deleteAllTasksFirstConfirmationDialogText))
            .setPositiveButton(getString(R.string.deleteAllTasksFirstPositiveButton)) { _, _ ->

                AlertDialog.Builder(this, R.style.AppCompatAlertDialogStyle)
                    .setTitle(getString(R.string.deleteAllTasksSecondConfirmationDialogText))
                    .setPositiveButton(getString(R.string.deleteAllTasksSecondPositiveButton)) { _, _ ->
                        presenter.onDeleteAllTasks()
                    }
                    .setNegativeButton(getString(R.string.deleteAllTasksNegativeButton)) { _, _ ->
                    }
                    .create()
                    .show()
            }
            .setNegativeButton(getString(R.string.deleteAllTasksNegativeButton)) { _, _ ->
            }
            .create()
            .show()
    }

    private fun sortTasksByPriority() {
        presenter.onTaskSortAction()
    }

    override fun showError() {
        displayToast(getString(R.string.errorToastText))
    }

    override fun onTaskSort() {
        /**
         * Nažalost ne znam elegantnije rješenje od ovoga
         */
        // (TaskAdapter.recyclerViewInstance.adapter as TaskAdapter).sortTasks()

        /*
        if (isTasksFragmentInitialized()) {
            val tasksFragment = getTasksFragment()
            val adapter = tasksFragment.getAdapterInstance()
            adapter.sortTasks()
        } else {
            showToastNoFragmentPresentForDeletion()
        }
        */

        val tasksFragment = getTasksFragment()
        val adapter = tasksFragment.getAdapterInstance()
        adapter.sortTasks()

    }

    override fun onSetData(tasks: MutableList<BackendTask>) {
        /**
         * Nažalost ne znam elegantnije rješenje od ovoga
         */
        // val adapterInstance = getRecyclerViewAdapterInstance()
        // adapterInstance.setData(tasks)

        /*
        if (isTasksFragmentInitialized()) {
            val tasksFragment = getTasksFragment()
            val adapter = tasksFragment.getAdapterInstance()
            adapter.setData(tasks)
        } else {
            showToastNoFragmentPresentForDeletion()
        }
        */

        val tasksFragment = getTasksFragment()
        val adapter = tasksFragment.getAdapterInstance()
        adapter.setData(tasks)
    }

    override fun onDeleteAllTasksSuccess() {
        Taskie.getApplicationContext().displayToast(getString(R.string.taskDeletionText))
    }

    override fun onDeleteAllTasksFailure() {
        Taskie.getApplicationContext().displayToast(getString(R.string.errorToastText))
    }

    private fun getTasksFragment(): TasksFragment {
        return supportFragmentManager.findFragmentById(R.id.fragmentContainer) as TasksFragment
    }

    private fun isTasksFragmentInitialized() =
        supportFragmentManager.findFragmentById(R.id.fragmentContainer) is TasksFragment

    private fun showToastNoFragmentPresentForDeletion() {
        displayToast(getString(R.string.deleteAllTasksErrorText))
    }

    // private fun getRecyclerViewAdapterInstance() = (TaskAdapter.recyclerViewInstance.adapter as TaskAdapter)

    override fun deletionErrorWhileOffline() {
        displayToast(getString(R.string.deleteAllTasksOfflineText))
    }

    // jako lose
    override fun sendSupportFragmentManager(): FragmentManager {
        return supportFragmentManager
    }

    override fun showNoFragmentPresentForDeletionError() {
        displayToast(getString(R.string.deleteAllTasksErrorText))
    }
}