package hr.ferit.petarmaric.taskieMVP_DI.ui.activities.register

import android.content.Intent
import hr.ferit.petarmaric.taskieMVP_DI.R
import hr.ferit.petarmaric.taskieMVP_DI.common.displayToast
import hr.ferit.petarmaric.taskieMVP_DI.common.onClick
import hr.ferit.petarmaric.taskieMVP_DI.model.request.UserDataRequest
import hr.ferit.petarmaric.taskieMVP_DI.ui.activities.base.BaseActivity
import hr.ferit.petarmaric.taskieMVP_DI.ui.activities.login.LoginActivity
import kotlinx.android.synthetic.main.activity_register.*
import org.koin.android.ext.android.inject

class RegisterActivity : BaseActivity(), RegisterContract.View {

    private val presenter by inject<RegisterContract.Presenter>()

    override fun getLayoutResourceId(): Int = R.layout.activity_register

    override fun setUpUi() {
        presenter.setView(this)
        register.onClick { signInClicked() }
        goToLogin.onClick { goToLoginClicked() }
    }

    override fun onRegisterSuccess() {
        this.displayToast(getString(R.string.successfulRegisterText))
        val intent = Intent(this, LoginActivity::class.java)
        startActivity(intent)
        finish()
    }

    override fun onRegisterFailure() {
        this.displayToast(getString(R.string.errorToastText))
    }

    private fun signInClicked() {
        presenter.onRegisterClicked(
            UserDataRequest(
                email = email.text.toString(),
                password = password.text.toString(),
                name = name.text.toString()
            )
        )
    }

    private fun goToLoginClicked() {
        val intent = Intent(this, LoginActivity::class.java)
        startActivity(intent)
        finish()
    }
}