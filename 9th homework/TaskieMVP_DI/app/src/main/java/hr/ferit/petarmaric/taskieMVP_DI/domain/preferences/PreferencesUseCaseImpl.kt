package hr.ferit.petarmaric.taskieMVP_DI.domain.preferences

import android.content.SharedPreferences
import hr.ferit.petarmaric.taskieMVP_DI.di.KEY_USER_TOKEN


class PreferencesUseCaseImpl(private val preferences: SharedPreferences) : PreferencesUseCase {
    override fun getUserToken(): String = preferences.getString(KEY_USER_TOKEN, "")

    override fun storeUserToken(token: String) = preferences.edit().putString(KEY_USER_TOKEN, token).apply()

    override fun clearUserToken() = preferences.edit().remove(KEY_USER_TOKEN).apply()

    override fun getPrefsString(key: String, defaultValue: String) = preferences.getString(key, defaultValue)

    override fun storePrefs(key: String, value: String) = preferences.edit().putString(key, value).apply()
}