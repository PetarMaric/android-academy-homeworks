package hr.ferit.petarmaric.taskieMVP_DI.domain.preferences

interface PreferencesUseCase {
    fun getUserToken(): String

    fun storeUserToken(token: String)

    fun clearUserToken()

    fun getPrefsString(key: String, defaultValue: String): String

    fun storePrefs(key: String, value: String)
}