package hr.ferit.petarmaric.taskieMVP_DI.di

import androidx.room.Room
import hr.ferit.petarmaric.taskieMVP_DI.persistence.database.TasksDatabase
import org.koin.android.ext.koin.androidApplication
import org.koin.core.qualifier.named
import org.koin.dsl.module

const val DB_NAME = "tasks_database"
const val DB_INSTANCE = "room_db_instance"
const val TASKS_DAO = "tasks_dao"
const val TASKS_TO_SAVE_DAO = "tasks_to_save_dao"

// https://android.jlelse.eu/painless-android-testing-with-room-koin-bb949eefcbee
val persistenceModule = module {

    single(named(DB_INSTANCE)) {
        Room.databaseBuilder(
            androidApplication(), TasksDatabase::class.java,
            DB_NAME
        ).allowMainThreadQueries()
            .build()
    }

    single(named(TASKS_DAO)) {
        get<TasksDatabase>(named(DB_INSTANCE)).getTasksDao()
    }

    single(named(TASKS_TO_SAVE_DAO)) {
        get<TasksDatabase>(named(DB_INSTANCE)).getTasksToSaveDao()
    }

}