package hr.ferit.petarmaric.taskieMVP_DI.persistence.database.daos

import androidx.room.*
import hr.ferit.petarmaric.taskieMVP_DI.model.Task

@Dao
interface TasksDao {

    @Query("SELECT * FROM tasks")
    fun getAllTasks(): MutableList<Task>

    @Query("SELECT * FROM tasks WHERE id=:id")
    fun getTask(id: String): Task

    @Query("SELECT * FROM tasks WHERE retrofitTaskId=:stringId")
    fun getTaskByRetrofitId(stringId: String): Task

    @Query("SELECT * FROM tasks ORDER BY taskPriority")
    fun getTasksOrderedByPriority(): List<Task>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun storeTask(task: Task)

    @Insert
    fun storeAllTasks(taskList: MutableList<Task>)

    @Update
    fun updateTask(task: Task)

    @Query("DELETE from tasks")
    fun deleteAllTasks()

    @Delete
    fun deleteTask(task: Task)
}