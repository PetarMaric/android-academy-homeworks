package hr.ferit.petarmaric.taskieMVP_DI.model.response

data class DeleteResponse(val message: String)