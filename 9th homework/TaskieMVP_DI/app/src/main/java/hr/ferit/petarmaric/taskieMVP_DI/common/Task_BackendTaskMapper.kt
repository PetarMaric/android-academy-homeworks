package hr.ferit.petarmaric.taskieMVP_DI.common

import hr.ferit.petarmaric.taskieMVP_DI.model.BackendTask
import hr.ferit.petarmaric.taskieMVP_DI.model.Task

fun taskToBackendTaskMapper(taskList: MutableList<Task>): MutableList<BackendTask> = taskList.map {
    BackendTask(
        id = it.retrofitTaskId,
        // userId = it.retrofitUserId,
        title = it.title,
        content = it.content,
        isFavorite = it.isFavorite,
        isCompleted = it.isCompleted,
        taskPriority = it.taskPriority
    )
}.toMutableList()


fun backendTaskToTaskMapper(backendTaskList: MutableList<BackendTask>): MutableList<Task> = backendTaskList.map {
    Task(
        retrofitTaskId = it.id,
        // retrofitUserId = it.userId,
        title = it.title,
        content = it.content,
        isFavorite = it.isFavorite,
        isCompleted = it.isCompleted,
        taskPriority = it.taskPriority
    )
}.toMutableList()