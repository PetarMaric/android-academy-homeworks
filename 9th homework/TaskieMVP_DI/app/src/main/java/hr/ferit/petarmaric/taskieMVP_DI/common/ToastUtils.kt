package hr.ferit.petarmaric.taskieMVP_DI.common

import android.content.Context
import android.widget.Toast

fun Context.displayToast(text: String) = Toast.makeText(this, text, Toast.LENGTH_LONG).show()

fun Context.displayToast(resourceId: Int) = Toast.makeText(this, getString(resourceId), Toast.LENGTH_SHORT).show()