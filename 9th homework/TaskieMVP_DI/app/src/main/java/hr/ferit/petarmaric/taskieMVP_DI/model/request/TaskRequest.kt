package hr.ferit.petarmaric.taskieMVP_DI.model.request

data class TaskRequest(val id: String)