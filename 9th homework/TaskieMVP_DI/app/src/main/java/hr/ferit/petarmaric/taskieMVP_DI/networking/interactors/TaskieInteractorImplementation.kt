package hr.ferit.petarmaric.taskieMVP_DI.networking.interactors

import hr.ferit.petarmaric.taskieMVP_DI.model.BackendTask
import hr.ferit.petarmaric.taskieMVP_DI.model.request.*
import hr.ferit.petarmaric.taskieMVP_DI.model.response.DeleteResponse
import hr.ferit.petarmaric.taskieMVP_DI.model.response.GetTasksResponse
import hr.ferit.petarmaric.taskieMVP_DI.model.response.LoginResponse
import hr.ferit.petarmaric.taskieMVP_DI.model.response.RegisterResponse
import hr.ferit.petarmaric.taskieMVP_DI.networking.TaskieApiService
import retrofit2.Callback

class TaskieInteractorImplementation(private val apiService: TaskieApiService) : TaskieInteractor {

    override fun getTasks(taskieResponseCallback: Callback<GetTasksResponse>) {
        apiService.getTasks().enqueue(taskieResponseCallback)
    }

    override fun getTask(request: TaskRequest, taskResponseCallback: Callback<BackendTask>) {
        apiService.getTask(request.id).enqueue(taskResponseCallback)
    }

    override fun register(request: UserDataRequest, registerCallback: Callback<RegisterResponse>) {
        apiService.register(request).enqueue(registerCallback)
    }

    override fun login(request: UserDataRequest, loginCallback: Callback<LoginResponse>) {
        apiService.login(request).enqueue(loginCallback)
    }

    override fun save(request: AddTaskRequest, saveCallback: Callback<BackendTask>) {
        apiService.save(request).enqueue(saveCallback)
    }

    override fun update(request: UpdateTaskRequest, updateCallback: Callback<BackendTask>) {
        apiService.updateTask(request).enqueue(updateCallback)
    }

    override fun delete(request: DeleteTaskRequest, deleteCallback: Callback<DeleteResponse>) {
        apiService.deleteTask(request.taskNoteId).enqueue(deleteCallback)
    }
}