package hr.ferit.petarmaric.taskieMVP_DI.presentation

import android.util.Log
import hr.ferit.petarmaric.taskieMVP_DI.domain.preferences.PreferencesUseCase
import hr.ferit.petarmaric.taskieMVP_DI.ui.activities.splashscreen.SplashContract

class SplashPresenter(private val prefs: PreferencesUseCase) : SplashContract.Presenter {

    private lateinit var view: SplashContract.View

    override fun setView(view: SplashContract.View) {
        this.view = view
    }

    override fun checkUserToken() {
        Log.d("TOKEN", prefs.getUserToken())
        if (prefs.getUserToken().isEmpty()) showSignIn() else showHomeScreen()
    }

    private fun showSignIn() {
        view.goToSignIn()
    }

    private fun showHomeScreen() {
        view.goToHomescreen()
    }
}