package hr.ferit.petarmaric.taskieMVP_DI.model

import androidx.annotation.ColorRes
import hr.ferit.petarmaric.taskieMVP_DI.R

enum class PriorityColor(@ColorRes private val colorRes: Int, private val priorityOrder: Int) {
    LOW(R.color.colorLow, 1),
    MEDIUM(R.color.colorMedium, 2),
    HIGH(R.color.colorHigh, 3);

    fun getColor(): Int = colorRes

    fun getPriorityOrder(): Int = priorityOrder
}


enum class BackendPriorityTask(private val num: Int) {
    LOW(1),
    MEDIUM(2),
    HIGH(3);

    fun getValue(): Int = num
}

