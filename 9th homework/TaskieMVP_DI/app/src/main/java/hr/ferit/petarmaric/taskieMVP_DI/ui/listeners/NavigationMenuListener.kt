package hr.ferit.petarmaric.taskieMVP_DI.ui.listeners

import android.view.MenuItem
import com.google.android.material.bottomnavigation.BottomNavigationView
import hr.ferit.petarmaric.taskieMVP_DI.R
import hr.ferit.petarmaric.taskieMVP_DI.common.showFragment
import hr.ferit.petarmaric.taskieMVP_DI.ui.activities.base.BaseActivity
import hr.ferit.petarmaric.taskieMVP_DI.ui.fragments.about.About
import hr.ferit.petarmaric.taskieMVP_DI.ui.fragments.tasks.TasksFragment

class NavigationMenuListener(private val context: BaseActivity) :
    BottomNavigationView.OnNavigationItemSelectedListener {


    override fun onNavigationItemSelected(item: MenuItem): Boolean {

        when (item.itemId) {
            R.id.taskList -> {
                context.showFragment(R.id.fragmentContainer, TasksFragment.newInstance())
            }
            R.id.about
            -> {
                context.showFragment(R.id.fragmentContainer, About.newInstance())
            }
        }
        return true
    }

}