package hr.ferit.petarmaric.taskieMVP_DI.ui.fragments.about

import hr.ferit.petarmaric.taskieMVP_DI.R
import hr.ferit.petarmaric.taskieMVP_DI.ui.fragments.base.BaseFragment

class AboutApplication : BaseFragment() {
    override fun getLayoutResourceId(): Int = R.layout.fragment_about_application

    companion object {
        fun getInstance(): AboutApplication = AboutApplication()
    }

}