package hr.ferit.petarmaric.taskieMVP_DI.presentation

import hr.ferit.petarmaric.taskieMVP_DI.domain.login.LoginUseCase
import hr.ferit.petarmaric.taskieMVP_DI.domain.preferences.PreferencesUseCase
import hr.ferit.petarmaric.taskieMVP_DI.model.request.UserDataRequest
import hr.ferit.petarmaric.taskieMVP_DI.model.response.LoginResponse
import hr.ferit.petarmaric.taskieMVP_DI.ui.activities.login.LoginContract

class LoginPresenter(private val loginUseCase: LoginUseCase, private val prefs: PreferencesUseCase) :
    LoginContract.Presenter {

    private lateinit var view: LoginContract.View

    override fun setView(view: LoginContract.View) {
        this.view = view
    }

    override fun onLoginClicked(emailParam: String, passwordParam: String) {
        loginUseCase.execute(UserDataRequest(emailParam, passwordParam), ::onLoginSuccess, ::onLoginFailure)
    }

    private fun onLoginSuccess(response: LoginResponse) {
        storeUserToken(response)
        view.onLoginSuccess()
    }

    private fun storeUserToken(response: LoginResponse) {
        response.token?.let { prefs.storeUserToken(it) }
    }

    private fun onLoginFailure(error: Throwable) {
        view.onLoginFailure()
    }

}