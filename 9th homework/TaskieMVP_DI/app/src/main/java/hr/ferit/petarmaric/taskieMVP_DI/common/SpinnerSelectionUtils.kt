package hr.ferit.petarmaric.taskieMVP_DI.common

import hr.ferit.petarmaric.taskieMVP_DI.model.PriorityColor

fun determinePriorityLocation(storedPriority: String): Int {
    val priorities = PriorityColor.values().map { it.toString() }.toSet()

    val index = priorities.indexOf(storedPriority)
    return index
}