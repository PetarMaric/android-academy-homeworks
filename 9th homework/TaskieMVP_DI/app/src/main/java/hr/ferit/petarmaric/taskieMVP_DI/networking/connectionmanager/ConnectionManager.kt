package hr.ferit.petarmaric.taskieMVP_DI.networking.connectionmanager

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkInfo
import hr.ferit.petarmaric.taskieMVP_DI.Taskie

object ConnectionManager {
    private fun getConnectivityManager() =
        Taskie.getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

    fun getNetworkInfo(): NetworkInfo? = getConnectivityManager().activeNetworkInfo
}