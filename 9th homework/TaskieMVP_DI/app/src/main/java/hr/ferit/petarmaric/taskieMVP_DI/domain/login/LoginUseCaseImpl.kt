package hr.ferit.petarmaric.taskieMVP_DI.domain.login

import android.util.Log
import hr.ferit.petarmaric.taskieMVP_DI.common.*
import hr.ferit.petarmaric.taskieMVP_DI.model.request.UserDataRequest
import hr.ferit.petarmaric.taskieMVP_DI.model.response.LoginResponse
import hr.ferit.petarmaric.taskieMVP_DI.networking.showBadRequestToast
import hr.ferit.petarmaric.taskieMVP_DI.networking.showNotFoundToast
import hr.ferit.petarmaric.taskieMVP_DI.networking.showServerErrorToast
import hr.ferit.petarmaric.taskieMVP_DI.networking.unidentifiedErrorToast
import hr.ferit.petarmaric.taskieMVP_DI.repositories.LoginRepository
import hr.ferit.petarmaric.taskieMVP_DI.ui.fragments.tasks.TasksFragment
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class LoginUseCaseImpl(private val loginRepository: LoginRepository) : LoginUseCase {
    override fun execute(body: UserDataRequest, onSuccess: SuccessLambda<LoginResponse>, onFailure: ErrorLambda) {
        loginRepository.loginUser(body).enqueue(object : Callback<LoginResponse> {
            override fun onFailure(call: Call<LoginResponse>?, t: Throwable) {
                Log.d(TasksFragment.REQUEST_ERROR, t.localizedMessage)
                Log.d(TasksFragment.REQUEST_ERROR, call?.request().toString())
                onFailure(t)
            }

            override fun onResponse(call: Call<LoginResponse>?, response: Response<LoginResponse>) {
                if (response.isSuccessful) {
                    when (response.code()) {
                        RESPONSE_OK -> response.body()?.run(onSuccess)
                        RESPONSE_BAD_REQUEST -> showBadRequestToast()
                        RESPONSE_NOT_FOUND -> showNotFoundToast()
                        SERVER_ERROR -> showServerErrorToast()
                        else -> unidentifiedErrorToast()
                    }
                }

                response.errorBody()?.run { onFailure(IllegalStateException("Something has broken. Try again later.")) }
            }
        })
    }
}