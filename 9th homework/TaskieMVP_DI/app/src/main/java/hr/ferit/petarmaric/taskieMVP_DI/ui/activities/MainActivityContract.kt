package hr.ferit.petarmaric.taskieMVP_DI.ui.activities

import androidx.fragment.app.FragmentManager
import hr.ferit.petarmaric.taskieMVP_DI.model.BackendTask

interface MainActivityContract {
    interface View {
        fun onDeleteAllTasksSuccess()

        fun onDeleteAllTasksFailure()

        fun onSetData(tasks: MutableList<BackendTask>)

        fun onTaskSort()

        fun showError()

        fun deletionErrorWhileOffline()

        // znam da jako krsi MVP, ali kada ne znam kako drugacije omoguciti provjeru trenutno prikazanog fragmenta u MainActivityPresenteru posto treba za validaciju postoji li uopce taj fragment u
        // frame layoutu
        fun sendSupportFragmentManager(): FragmentManager

        fun showNoFragmentPresentForDeletionError()
    }

    interface Presenter {
        fun setView(view: MainActivityContract.View)

        fun onDeleteAllTasks()

        fun onTaskSortAction()
    }
}