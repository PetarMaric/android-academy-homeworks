package hr.ferit.petarmaric.taskieMVP_DI.ui.activities.register

import hr.ferit.petarmaric.taskieMVP_DI.model.request.UserDataRequest

interface RegisterContract {

    interface View {
        fun onRegisterSuccess()

        fun onRegisterFailure()
    }

    interface Presenter {
        fun setView(view: RegisterContract.View)

        fun onRegisterClicked(user: UserDataRequest)
    }
}