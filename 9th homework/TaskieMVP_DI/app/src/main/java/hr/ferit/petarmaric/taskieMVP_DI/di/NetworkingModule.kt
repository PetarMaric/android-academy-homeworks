package hr.ferit.petarmaric.taskieMVP_DI.di

import hr.ferit.petarmaric.taskieMVP_DI.BuildConfig
import hr.ferit.petarmaric.taskieMVP_DI.common.BASE_URL
import hr.ferit.petarmaric.taskieMVP_DI.common.KEY_AUTHORIZATION
import hr.ferit.petarmaric.taskieMVP_DI.domain.preferences.PreferencesUseCase
import hr.ferit.petarmaric.taskieMVP_DI.networking.TaskieApiService
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.core.qualifier.named
import org.koin.dsl.module
import retrofit2.Converter
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

const val GSON_CONVERTER_FACTORY = "gson_converter_factory"
const val LOGGING_INTERCEPTOR = "logging_interceptor"
const val AUTH_INTERCEPTOR = "auth_interceptor"
const val OKHTTPCLIENT = "okhttpclient"
const val RETROFIT = "retrofit"
const val RETROFIT_CLIENT = "retrofit_client"

val networkingModule = module {

    single(named(GSON_CONVERTER_FACTORY)) { GsonConverterFactory.create() as Converter.Factory }

    single(named(LOGGING_INTERCEPTOR)) { HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY) }

    single(named(AUTH_INTERCEPTOR)) {
        Interceptor {
            val request = it.request().newBuilder()
                .addHeader(KEY_AUTHORIZATION, get<PreferencesUseCase>().getUserToken())
                .build()
            it.proceed(request)
        }
    }

    single(named(OKHTTPCLIENT)) {
        OkHttpClient.Builder().apply {
            if (BuildConfig.DEBUG) addInterceptor(get(named(LOGGING_INTERCEPTOR)))
        }
            .addInterceptor(get(named(LOGGING_INTERCEPTOR)))
            .addInterceptor(get(named(AUTH_INTERCEPTOR)))
            .build()
    }

    single(named(RETROFIT)) {
        Retrofit.Builder()
            .client(get(named(OKHTTPCLIENT)))
            .baseUrl(BASE_URL)
            .addConverterFactory(get(named(GSON_CONVERTER_FACTORY)))
            .build()
    }

    single(named(RETROFIT_CLIENT)) {
        get<Retrofit>(named(RETROFIT)).create(TaskieApiService::class.java)
    }

}