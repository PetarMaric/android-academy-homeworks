package hr.ferit.petarmaric.taskieMVP_DI.presentation

import android.util.Log
import androidx.fragment.app.FragmentManager
import hr.ferit.petarmaric.taskieMVP_DI.R
import hr.ferit.petarmaric.taskieMVP_DI.common.RESPONSE_BAD_REQUEST
import hr.ferit.petarmaric.taskieMVP_DI.common.RESPONSE_NOT_FOUND
import hr.ferit.petarmaric.taskieMVP_DI.common.RESPONSE_OK
import hr.ferit.petarmaric.taskieMVP_DI.common.SERVER_ERROR
import hr.ferit.petarmaric.taskieMVP_DI.model.request.DeleteTaskRequest
import hr.ferit.petarmaric.taskieMVP_DI.model.response.DeleteResponse
import hr.ferit.petarmaric.taskieMVP_DI.model.response.GetTasksResponse
import hr.ferit.petarmaric.taskieMVP_DI.networking.connectionmanager.ConnectionManager
import hr.ferit.petarmaric.taskieMVP_DI.networking.interactors.TaskieInteractor
import hr.ferit.petarmaric.taskieMVP_DI.networking.showBadRequestToast
import hr.ferit.petarmaric.taskieMVP_DI.networking.showNotFoundToast
import hr.ferit.petarmaric.taskieMVP_DI.networking.showServerErrorToast
import hr.ferit.petarmaric.taskieMVP_DI.ui.activities.MainActivityContract
import hr.ferit.petarmaric.taskieMVP_DI.ui.adapters.recyclerview.TaskAdapter
import hr.ferit.petarmaric.taskieMVP_DI.ui.fragments.tasks.TasksFragment
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MainActivityPresenter(private val interactor: TaskieInteractor) : MainActivityContract.Presenter {

    private lateinit var view: MainActivityContract.View

    private lateinit var fragmentManager: FragmentManager

    override fun setView(view: MainActivityContract.View) {
        this.view = view
    }

    override fun onTaskSortAction() {
        view.onTaskSort()
    }

    override fun onDeleteAllTasks() {
        /*
        if (isInternetConnectionAvaliable()) {
            val recyclerViewAdapterInstance = (TaskAdapter.recyclerViewInstance.adapter as TaskAdapter)
            val dataList = recyclerViewAdapterInstance.getData()
            dataList.forEach {
                interactor.delete(DeleteTaskRequest(it.id), deleteCallback())
            }
        } else {
            view.deletionErrorWhileOffline()
        }
        */

        fragmentManager = view.sendSupportFragmentManager()


        if (isTasksFragmentInitialized()) {
            if (isInternetConnectionAvaliable()) {
                val tasksFragment = getTasksFragment()
                val adapter = tasksFragment.getAdapterInstance()
                val dataList = adapter.getData()
                dataList.forEach {
                    interactor.delete(DeleteTaskRequest(it.id), deleteCallback())
                }
            } else {
                view.deletionErrorWhileOffline()
            }
        } else {
            view.showNoFragmentPresentForDeletionError()
        }

    }

    private fun getTasksFragment(): TasksFragment {
        return fragmentManager.findFragmentById(R.id.fragmentContainer) as TasksFragment
    }

    private fun isTasksFragmentInitialized() =
        fragmentManager.findFragmentById(R.id.fragmentContainer) is TasksFragment

    private fun deleteCallback(): Callback<DeleteResponse> = object : Callback<DeleteResponse> {
        override fun onFailure(call: Call<DeleteResponse>?, t: Throwable?) {
            Log.d(TasksFragment.REQUEST_ERROR, t?.localizedMessage)
            Log.d(TasksFragment.REQUEST_ERROR, call?.request().toString())
        }

        override fun onResponse(call: Call<DeleteResponse>?, response: Response<DeleteResponse>) {
            if (response.isSuccessful) {
                when (response.code()) {
                    RESPONSE_OK -> handleDeletionOkResponse()
                    RESPONSE_BAD_REQUEST -> showBadRequestToast()
                    RESPONSE_NOT_FOUND -> showNotFoundToast()
                    SERVER_ERROR -> showServerErrorToast()
                    else -> handleSomethingWentWrongWithDeletion()
                }
            } else {
                handleSomethingWentWrongWithDeletion()
            }
        }
    }

    private fun handleDeletionOkResponse() {
        view.onDeleteAllTasksSuccess()
        getAllTasks()
    }

    private fun handleSomethingWentWrongWithDeletion() {
        view.onDeleteAllTasksFailure()
    }


    private fun getAllTasks() {
        interactor.getTasks(getTaskieCallback())
    }

    private fun getTaskieCallback(): Callback<GetTasksResponse> = object : Callback<GetTasksResponse> {
        override fun onFailure(call: Call<GetTasksResponse>?, t: Throwable?) {
            Log.d(TasksFragment.REQUEST_ERROR, t?.localizedMessage)
            Log.d(TasksFragment.REQUEST_ERROR, call?.request().toString())
        }

        override fun onResponse(call: Call<GetTasksResponse>?, response: Response<GetTasksResponse>) {
            if (response.isSuccessful) {
                when (response.code()) {
                    RESPONSE_OK -> handleOkResponse(response)
                    RESPONSE_BAD_REQUEST -> showBadRequestToast()
                    RESPONSE_NOT_FOUND -> showNotFoundToast()
                    SERVER_ERROR -> showServerErrorToast()
                    else -> handleSomethingWentWrong()
                }
            } else {
                handleSomethingWentWrong()
            }
        }
    }

    private fun handleSomethingWentWrong() {
        view.showError()
    }

    private fun isInternetConnectionAvaliable() = ConnectionManager.getNetworkInfo()?.isConnected ?: false

    private fun handleOkResponse(response: Response<GetTasksResponse>) {
        response.body()?.notes?.run {
            view.onSetData(this)
        }
    }

}