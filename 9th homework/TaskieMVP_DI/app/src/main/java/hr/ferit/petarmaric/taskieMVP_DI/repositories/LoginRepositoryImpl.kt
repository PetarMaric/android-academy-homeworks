package hr.ferit.petarmaric.taskieMVP_DI.repositories

import hr.ferit.petarmaric.taskieMVP_DI.model.request.UserDataRequest
import hr.ferit.petarmaric.taskieMVP_DI.model.response.LoginResponse
import hr.ferit.petarmaric.taskieMVP_DI.networking.TaskieApiService
import retrofit2.Call

class LoginRepositoryImpl(private val taskieService: TaskieApiService) : LoginRepository {
    override fun loginUser(body: UserDataRequest): Call<LoginResponse> =
        taskieService.login(body)
}