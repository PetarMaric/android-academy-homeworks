package hr.ferit.petarmaric.taskieMVP_DI.model.request

data class UserDataRequest(val email: String, val password: String, val name: String? = null)