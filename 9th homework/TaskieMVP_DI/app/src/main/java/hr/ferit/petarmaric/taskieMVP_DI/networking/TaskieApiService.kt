package hr.ferit.petarmaric.taskieMVP_DI.networking

import hr.ferit.petarmaric.taskieMVP_DI.model.BackendTask
import hr.ferit.petarmaric.taskieMVP_DI.model.request.AddTaskRequest
import hr.ferit.petarmaric.taskieMVP_DI.model.request.UpdateTaskRequest
import hr.ferit.petarmaric.taskieMVP_DI.model.request.UserDataRequest
import hr.ferit.petarmaric.taskieMVP_DI.model.response.DeleteResponse
import hr.ferit.petarmaric.taskieMVP_DI.model.response.GetTasksResponse
import hr.ferit.petarmaric.taskieMVP_DI.model.response.LoginResponse
import hr.ferit.petarmaric.taskieMVP_DI.model.response.RegisterResponse
import retrofit2.Call
import retrofit2.http.*

interface TaskieApiService {

    @POST("/api/register")
    fun register(@Body userData: UserDataRequest): Call<RegisterResponse>

    @POST("/api/login")
    fun login(@Body userData: UserDataRequest): Call<LoginResponse>

    @GET("/api/note")
    fun getTasks(): Call<GetTasksResponse>

    @GET("/api/note/{noteId}")
    fun getTask(@Path("noteId") taskNoteId: String): Call<BackendTask>

    @POST("/api/note/edit")
    fun updateTask(@Body task: UpdateTaskRequest): Call<BackendTask>

    @POST("/api/note")
    fun save(@Body taskData: AddTaskRequest): Call<BackendTask>

    @POST("/api/note/delete")
    fun deleteTask(@Query("id") taskNoteId: String): Call<DeleteResponse>
}