package hr.ferit.petarmaric.taskieMVP_DI.ui.fragments.tasks

import hr.ferit.petarmaric.taskieMVP_DI.model.BackendTask
import hr.ferit.petarmaric.taskieMVP_DI.model.PriorityColor

interface AddTaskContract {

    interface View {
        fun onTaskStored(task: BackendTask)

        fun onTaskAddFailure()

        fun onTaskSavedLocallyMessage()
    }

    interface Presenter {
        fun setView(view: AddTaskContract.View)

        fun onAddTask(title: String, description: String, priority: PriorityColor, priorityBackendPriorityTask: Int)

        fun onFetchStoredPriority(): String

        fun onStorePriority(priority: PriorityColor)
    }
}