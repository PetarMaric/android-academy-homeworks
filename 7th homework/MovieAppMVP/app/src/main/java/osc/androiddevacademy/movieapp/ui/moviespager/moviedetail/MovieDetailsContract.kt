package osc.androiddevacademy.movieapp.ui.moviespager.moviedetail

import osc.androiddevacademy.movieapp.model.Review

interface MovieDetailsContract {

    interface View {
        fun onReviewsFetchSuccess(reviews: MutableList<Review>)

        fun onReviewsFetchFailure()
    }

    interface Presenter {
        fun setView(view: MovieDetailsContract.View)

        fun onGetReviewsAction(movieId: Int)
    }
}