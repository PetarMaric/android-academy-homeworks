package osc.androiddevacademy.movieapp.ui.topmovies

import osc.androiddevacademy.movieapp.model.Movie

interface TopMoviesContract {

    interface View {

        fun topMoviesFetchSuccess(movies: MutableList<Movie>)

        fun topMoviesFetchFailure()

        fun onFavoriteMovieAdded(movie: Movie)

        fun onFavoriteMovieRemoved(movie: Movie)

    }

    interface Presenter {

        fun setView(view: TopMoviesContract.View)

        fun onFragmentLoadGetTopMovies()

        fun setFavoriteMovie(movie: Movie)

        fun requestTopMovies(): ArrayList<Movie>
    }
}
