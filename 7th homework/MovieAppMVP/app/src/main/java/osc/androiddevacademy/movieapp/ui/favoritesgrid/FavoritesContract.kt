package osc.androiddevacademy.movieapp.ui.favoritesgrid

import osc.androiddevacademy.movieapp.model.Movie

interface FavoritesContract {

    interface View {
        fun favoritesFetchSuccess(movies: MutableList<Movie>)

        fun favoritesFetchFail()

        fun favoritesFetchEmpty()

        fun onFavoriteMovieRemovedNotify(movie: Movie)

    }

    interface Presenter {
        fun setView(view: FavoritesContract.View)

        fun onFragmentLoadGetMovies()

        fun requestFavoriteMovies(): ArrayList<Movie>

        fun unfavoriteMovie(movie: Movie)
    }
}