package osc.androiddevacademy.movieapp.ui.topmovies

import osc.androiddevacademy.movieapp.model.Movie

interface TopMoviesViewHolderContract {
    fun setMovie(movie: Movie)

    fun setMovieTitle(movie: Movie)

    fun setMovieAverageScore(movie: Movie)

    fun setMovieReleasteDate(movie: Movie)
}