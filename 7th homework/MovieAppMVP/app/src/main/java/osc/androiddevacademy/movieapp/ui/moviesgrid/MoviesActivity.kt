package osc.androiddevacademy.movieapp.ui.moviesgrid

import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import osc.androiddevacademy.movieapp.R
import osc.androiddevacademy.movieapp.common.showFragment
import osc.androiddevacademy.movieapp.ui.favoritesgrid.FavoritesFragment
import osc.androiddevacademy.movieapp.ui.moviesgrid.fragment.MoviesGridFragment
import osc.androiddevacademy.movieapp.ui.topmovies.TopMoviesFragment

class MoviesActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_movies)
        initMoviesGridFragment()
    }

    private fun initMoviesGridFragment() {
        showFragment(
            R.id.mainFragmentHolder,
            MoviesGridFragment()
        )
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        super.onCreateOptionsMenu(menu)
        menuInflater.inflate(R.menu.menu_movie_options, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            R.id.showPopularMovies -> {
                showFragment(
                    R.id.mainFragmentHolder,
                    MoviesGridFragment()
                )
            }
            R.id.showFavorites -> {
                showFragment(
                    R.id.mainFragmentHolder,
                    FavoritesFragment()
                )
            }
            R.id.showTopMovies -> {
                showFragment(
                    R.id.mainFragmentHolder,
                    TopMoviesFragment()
                )
            }
        }
        return super.onOptionsItemSelected(item)
    }
}
