package osc.androiddevacademy.movieapp.ui.favoritesgrid

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import kotlinx.android.synthetic.main.fragment_movie_grid.*
import osc.androiddevacademy.movieapp.R
import osc.androiddevacademy.movieapp.common.displayToast
import osc.androiddevacademy.movieapp.common.showFragment
import osc.androiddevacademy.movieapp.database.MoviesRoomRepository
import osc.androiddevacademy.movieapp.model.Movie
import osc.androiddevacademy.movieapp.presentation.MovieFavoriteFragmentPresenter
import osc.androiddevacademy.movieapp.ui.adapters.gridadapter.MoviesGridAdapter
import osc.androiddevacademy.movieapp.ui.moviespager.fragment.MoviesPagerFragment


class FavoritesFragment : Fragment(), FavoritesContract.View {
    private val SPAN_COUNT = 2

    private val gridAdapter by lazy {
        MoviesGridAdapter(
            { onMovieClicked(it) },
            { onFavoriteClicked(it) })
    }

    private fun onMovieClicked(movie: Movie) {

        activity?.showFragment(
            R.id.mainFragmentHolder,
            MoviesPagerFragment.getInstance(
                presenter.requestFavoriteMovies(),
                movie
            ),
            true
        )
    }

    private fun onFavoriteClicked(movie: Movie) {
        presenter.unfavoriteMovie(movie)
    }

    private val presenter: FavoritesContract.Presenter by lazy {
        MovieFavoriteFragmentPresenter(
            MoviesRoomRepository()
        )
    }

    override fun onFavoriteMovieRemovedNotify(movie: Movie) {
        gridAdapter.removeMovie(movie)
        activity?.displayToast("${movie.title} removed from favorites.")
    }

    override fun favoritesFetchEmpty() {
        activity?.displayToast("There is no favorites to show. Select some :)")
    }

    override fun favoritesFetchSuccess(movies: MutableList<Movie>) {
        gridAdapter.setMovies(movies)
    }

    override fun favoritesFetchFail() {
        activity?.displayToast("There was an error while fetching favorite movies.")
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_movie_grid, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter.setView(this)
        presenter.onFragmentLoadGetMovies()
        moviesGrid.apply {
            adapter = gridAdapter
            layoutManager = GridLayoutManager(context, SPAN_COUNT)
        }
    }

    override fun onResume() {
        super.onResume()
        presenter.onFragmentLoadGetMovies()
    }


}