package osc.androiddevacademy.movieapp.ui.moviesgrid.fragment

import osc.androiddevacademy.movieapp.model.Movie

interface MoviesContract {

    interface View {

        fun onMoviesFetchSuccess(movies: MutableList<Movie>)

        fun onMoviesFetchFailure()

        fun onFavoriteMovieAdded(movie: Movie)

        fun onFavoriteMovieRemoved(movie: Movie)


    }

    interface Presenter {

        fun setView(view: MoviesContract.View)

        fun onGridFragmentLoadGetMovies()

        fun requestMovies(): ArrayList<Movie>

        fun getMovieListSize(): Int

        fun setFavoriteMovie(movie: Movie)
    }
}