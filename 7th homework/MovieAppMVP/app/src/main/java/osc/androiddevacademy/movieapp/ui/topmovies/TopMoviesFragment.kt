package osc.androiddevacademy.movieapp.ui.topmovies

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import kotlinx.android.synthetic.main.fragment_movie_grid.*
import osc.androiddevacademy.movieapp.R
import osc.androiddevacademy.movieapp.common.displayToast
import osc.androiddevacademy.movieapp.common.showFragment
import osc.androiddevacademy.movieapp.database.MoviesRoomRepository
import osc.androiddevacademy.movieapp.model.Movie
import osc.androiddevacademy.movieapp.networking.BackendFactory
import osc.androiddevacademy.movieapp.presentation.TopMoviesFragmentPresenter
import osc.androiddevacademy.movieapp.ui.adapters.gridadapter.MoviesGridAdapter
import osc.androiddevacademy.movieapp.ui.moviespager.fragment.MoviesPagerFragment


class TopMoviesFragment : Fragment(), TopMoviesContract.View {

    private val gridAdapter by lazy {
        MoviesGridAdapter(
            { onMovieClicked(it) },
            { onFavoriteClicked(it) })
    }

    private val presenter: TopMoviesContract.Presenter by lazy {
        TopMoviesFragmentPresenter(
            BackendFactory.getMovieInteractor(),
            MoviesRoomRepository()
        )
    }

    private val SPAN_COUNT = 2

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_movie_grid, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter.setView(this)
        presenter.onFragmentLoadGetTopMovies()
        moviesGrid.apply {
            adapter = gridAdapter
            layoutManager = GridLayoutManager(context, SPAN_COUNT)
        }
    }

    override fun onResume() {
        super.onResume()
        presenter.onFragmentLoadGetTopMovies()
    }

    private fun onMovieClicked(movie: Movie) {
        activity?.showFragment(
            R.id.mainFragmentHolder,
            MoviesPagerFragment.getInstance(
                presenter.requestTopMovies(),
                movie
            ),
            shouldAddToBackStack = true
        )
    }

    private fun onFavoriteClicked(movie: Movie) {
        presenter.setFavoriteMovie(movie)
    }

    override fun onFavoriteMovieAdded(movie: Movie) {
        activity?.displayToast("'${movie.title}' added to favorites.")
    }

    override fun onFavoriteMovieRemoved(movie: Movie) {
        activity?.displayToast("'${movie.title}' removed from favorites.")
    }

    override fun topMoviesFetchSuccess(movies: MutableList<Movie>) {
        gridAdapter.setMovies(movies)
    }

    override fun topMoviesFetchFailure() {
        activity?.displayToast("Can not fetch top movies.")
    }
}