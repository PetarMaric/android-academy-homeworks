package osc.androiddevacademy.movieapp.ui.moviesgrid.fragment

import osc.androiddevacademy.movieapp.model.Movie

// https://android.jlelse.eu/recyclerview-in-mvp-passive-views-approach-8dd74633158
interface MovieGridHolder {

    fun setMovie(movie: Movie)

    fun setMovieClickListener(movie: Movie, onMovieClickListener: (Movie) -> Unit)

    fun setFavoriteClickListener(movie: Movie, onFavoriteClickListener: (Movie) -> Unit)

}