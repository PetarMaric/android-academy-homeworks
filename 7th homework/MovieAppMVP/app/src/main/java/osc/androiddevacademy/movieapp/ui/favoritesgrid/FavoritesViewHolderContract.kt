package osc.androiddevacademy.movieapp.ui.favoritesgrid

import osc.androiddevacademy.movieapp.model.Movie

interface FavoritesViewHolderContract {
    fun setMovie(movie: Movie)
}