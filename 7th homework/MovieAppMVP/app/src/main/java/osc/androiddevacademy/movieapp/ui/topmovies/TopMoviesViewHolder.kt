package osc.androiddevacademy.movieapp.ui.topmovies

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.item_top_movie.*
import osc.androiddevacademy.movieapp.common.loadImage
import osc.androiddevacademy.movieapp.model.Movie

class TopMoviesViewHolder(override val containerView: View) :
    RecyclerView.ViewHolder(containerView), LayoutContainer, TopMoviesViewHolderContract {

    override fun setMovie(movie: Movie) {
        movieImage.loadImage(movie.poster)
    }

    override fun setMovieTitle(movie: Movie) {
        movieTitle.text = movie.title
    }

    override fun setMovieAverageScore(movie: Movie) {
        movieVoteAverage.text = movie.averageVote.toString()
    }

    override fun setMovieReleasteDate(movie: Movie) {
        movieReleaseDate.text = movie.releaseDate
    }

}