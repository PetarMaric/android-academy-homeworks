package osc.androiddevacademy.movieapp.presentation

import osc.androiddevacademy.movieapp.common.networking.*
import osc.androiddevacademy.movieapp.database.MoviesRepository
import osc.androiddevacademy.movieapp.model.Movie
import osc.androiddevacademy.movieapp.model.MoviesResponse
import osc.androiddevacademy.movieapp.networking.interactors.MovieInteractor
import osc.androiddevacademy.movieapp.ui.moviesgrid.fragment.MoviesContract
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MoviesGridFragmentPresenter(
    private val apiInteractor: MovieInteractor,
    private val moviesRepository: MoviesRepository
) : MoviesContract.Presenter {
    private val movies = ArrayList<Movie>()

    private val favoriteMovieList = mutableListOf<Movie>()

    private lateinit var view: MoviesContract.View

    override fun setView(view: MoviesContract.View) {
        this.view = view
    }

    override fun setFavoriteMovie(movie: Movie) {
        movie.isFavorite = !movie.isFavorite
        if (movie.isFavorite) {
            movies[movies.indexOf(movie)] = movie
            view.onMoviesFetchSuccess(movies)
            view.onFavoriteMovieAdded(movie)
            storeFavoriteMovie(movie)

        } else {
            movies[movies.indexOf(movie)] = movie
            moviesRepository.deleteFavoriteMovie(movie)
            view.onMoviesFetchSuccess(movies)
            view.onFavoriteMovieRemoved(movie)
        }
    }

    private fun fetchFavoriteMovies() {
        favoriteMovieList.clear()
        favoriteMovieList.addAll(moviesRepository.getFavoriteMovies())
    }

    private fun storeFavoriteMovie(movie: Movie) {
        moviesRepository.addFavoriteMovie(movie)
    }

    override fun onGridFragmentLoadGetMovies() {
        apiInteractor.getPopularMovies(popularMoviesCallback())
    }

    private fun popularMoviesCallback(): Callback<MoviesResponse> =
        object : Callback<MoviesResponse> {
            override fun onFailure(call: Call<MoviesResponse>, t: Throwable) {
                t.printStackTrace()
            }

            override fun onResponse(
                call: Call<MoviesResponse>,
                response: Response<MoviesResponse>
            ) {

                if (response.isSuccessful) {
                    when (response.code()) {
                        RESPONSE_OK ->
                            response.body()?.movies?.run { handleOkResponse(this) }
                        RESPONSE_BAD_REQUEST -> showBadRequestToast()
                        RESPONSE_NOT_FOUND -> showNotFoundToast()
                        SERVER_ERROR -> showServerErrorToast()
                        else -> handleSomethingWentWrong()
                    }
                }
            }
        }

    private fun modifyMoviesArray(movies: MutableList<Movie>) {
        this.movies.clear()
        this.movies.addAll(movies)

        mergeFavoriteMovies()
    }

    private fun mergeFavoriteMovies() {
        fetchFavoriteMovies()

        if (!favoriteMovieList.isNullOrEmpty()) {
            favoriteMovieList.forEach {
                if (!it.isTopFavorite) {
                    val index = movies.indexOf(it)
                    movies.elementAt(index).isFavorite = it.isFavorite
                }

            }
        }
    }

    private fun handleOkResponse(movies: MutableList<Movie>) {
        modifyMoviesArray(movies)
        view.onMoviesFetchSuccess(movies)
    }

    private fun handleSomethingWentWrong() = view.onMoviesFetchFailure()

    override fun requestMovies() = getMoviesArray()

    override fun getMovieListSize(): Int = movies.size

    private fun getMoviesArray() = movies
}