package osc.androiddevacademy.movieapp.ui.moviespager.moviedetail

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.fragment_details.*
import osc.androiddevacademy.movieapp.R
import osc.androiddevacademy.movieapp.common.displayToast
import osc.androiddevacademy.movieapp.common.loadImage
import osc.androiddevacademy.movieapp.model.Movie
import osc.androiddevacademy.movieapp.model.Review
import osc.androiddevacademy.movieapp.networking.BackendFactory
import osc.androiddevacademy.movieapp.presentation.MovieDetailsFragmentPresenter
import osc.androiddevacademy.movieapp.ui.adapters.ReviewAdapter

class MovieDetailsFragment : Fragment(), MovieDetailsContract.View {

    private val presenter: MovieDetailsContract.Presenter by lazy {
        MovieDetailsFragmentPresenter(
            BackendFactory.getMovieInteractor()
        )
    }

    private val reviewsAdapter by lazy { ReviewAdapter() }

    companion object {
        private const val MOVIE_EXTRA = "movie_extra"

        fun getInstance(movie: Movie): MovieDetailsFragment {
            val fragment =
                MovieDetailsFragment()
            val bundle = Bundle()
            bundle.putParcelable(MOVIE_EXTRA, movie)
            fragment.arguments = bundle
            return fragment
        }
    }

    private lateinit var movie: Movie

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_details, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter.setView(this)
        movie = arguments?.getParcelable(MOVIE_EXTRA) as Movie
        initUi()
        getReviews()
    }

    private fun initUi() {
        movieImage.loadImage(movie.poster)
        movieTitle.text = movie.title
        movieOverview.text = movie.overview
        movieReleaseDate.text = movie.releaseDate
        movieVoteAverage.text = movie.averageVote.toString()

        movieReviewList.apply {
            adapter = reviewsAdapter
            layoutManager = LinearLayoutManager(context)
        }
    }

    private fun getReviews() {
        presenter.onGetReviewsAction(movie.id)
    }

    override fun onReviewsFetchSuccess(reviews: MutableList<Review>) {
        reviewsAdapter.setReviews(reviews)
    }

    override fun onReviewsFetchFailure() {
        activity?.displayToast("Experienced error while fetching reviews. Please try again later.")
    }
}