package osc.androiddevacademy.movieapp.database

import osc.androiddevacademy.movieapp.model.Movie

interface MoviesRepository {

    fun addFavoriteMovie(movie: Movie)

    fun deleteFavoriteMovie(movie: Movie)

    fun getFavoriteMovies(): List<Movie>
}