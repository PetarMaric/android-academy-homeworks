package osc.androiddevacademy.movieapp.presentation

import osc.androiddevacademy.movieapp.common.networking.*
import osc.androiddevacademy.movieapp.database.MoviesRepository
import osc.androiddevacademy.movieapp.model.Movie
import osc.androiddevacademy.movieapp.model.Review
import osc.androiddevacademy.movieapp.model.ReviewsResponse
import osc.androiddevacademy.movieapp.networking.interactors.MovieInteractor
import osc.androiddevacademy.movieapp.ui.moviespager.moviedetail.MovieDetailsContract
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MovieDetailsFragmentPresenter(
    private val apiInteractor: MovieInteractor
) :
    MovieDetailsContract.Presenter {

    private lateinit var view: MovieDetailsContract.View

    private val reviewList = mutableListOf<Review>()

    override fun setView(view: MovieDetailsContract.View) {
        this.view = view
    }

    override fun onGetReviewsAction(movieId: Int) {
        apiInteractor.getReviewsForMovie(movieId, reviewsCallback())
    }

    private fun reviewsCallback(): Callback<ReviewsResponse> = object : Callback<ReviewsResponse> {
        override fun onFailure(call: Call<ReviewsResponse>, t: Throwable) {
            t.printStackTrace()
        }

        override fun onResponse(call: Call<ReviewsResponse>, response: Response<ReviewsResponse>) {
            if (response.isSuccessful) {

                when (response.code()) {
                    RESPONSE_OK ->
                        response.body()?.reviews?.run { handleOkResponse(this) }
                    RESPONSE_BAD_REQUEST -> showBadRequestToast()
                    RESPONSE_NOT_FOUND -> showNotFoundToast()
                    SERVER_ERROR -> showServerErrorToast()
                    else -> handleSomethingWentWrong()
                }
            }
        }
    }

    private fun modifyReviewList(reviews: MutableList<Review>) {
        reviewList.clear()
        reviewList.addAll(reviews)
    }

    private fun handleOkResponse(reviews: MutableList<Review>) {
        modifyReviewList(reviews)
        view.onReviewsFetchSuccess(reviews)
    }

    private fun handleSomethingWentWrong() = view.onReviewsFetchFailure()


}