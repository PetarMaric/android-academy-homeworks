package osc.androiddevacademy.movieapp.database

import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import osc.androiddevacademy.movieapp.App
import osc.androiddevacademy.movieapp.model.Movie

@Database(entities = [Movie::class], version = 1)
abstract class MoviesDatabase : RoomDatabase() {

    abstract fun getMoviesDao(): MoviesDao

    companion object {
        private const val DB_NAME = "MoviesDatabase"

        private val dbInstance by lazy {
            Room.databaseBuilder(App.getAppContext(), MoviesDatabase::class.java, DB_NAME)
                .allowMainThreadQueries()
                .build()
        }

        val moviesDao by lazy { dbInstance.getMoviesDao() }
    }

}