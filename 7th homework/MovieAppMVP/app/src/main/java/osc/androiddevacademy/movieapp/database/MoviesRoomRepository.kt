package osc.androiddevacademy.movieapp.database

import osc.androiddevacademy.movieapp.model.Movie

class MoviesRoomRepository : MoviesRepository {
    private val moviesDao = MoviesDatabase.moviesDao

    override fun addFavoriteMovie(movie: Movie) {
        moviesDao.addFavoriteMovie(movie)
    }

    override fun deleteFavoriteMovie(movie: Movie) {
        moviesDao.deleteFavoriteMovie(movie)
    }

    override fun getFavoriteMovies(): List<Movie> = moviesDao.getFavoriteMovies()


}