package osc.androiddevacademy.movieapp.presentation

import osc.androiddevacademy.movieapp.common.networking.*
import osc.androiddevacademy.movieapp.database.MoviesRepository
import osc.androiddevacademy.movieapp.model.Movie
import osc.androiddevacademy.movieapp.model.MoviesResponse
import osc.androiddevacademy.movieapp.networking.interactors.MovieInteractor
import osc.androiddevacademy.movieapp.ui.topmovies.TopMoviesContract
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class TopMoviesFragmentPresenter(
    private val apiInteractor: MovieInteractor,
    private val moviesRepository: MoviesRepository
) : TopMoviesContract.Presenter {
    override fun requestTopMovies(): ArrayList<Movie> = topMovies

    override fun setFavoriteMovie(movie: Movie) {
        movie.isFavorite = !movie.isFavorite
        movie.isTopFavorite = true
        if (movie.isFavorite) {
            topMovies[topMovies.indexOf(movie)] = movie
            view.topMoviesFetchSuccess(topMovies)
            view.onFavoriteMovieAdded(movie)
            storeFavoriteMovie(movie)

        } else {
            topMovies[topMovies.indexOf(movie)] = movie
            moviesRepository.deleteFavoriteMovie(movie)
            view.topMoviesFetchSuccess(topMovies)
            view.onFavoriteMovieRemoved(movie)
        }
    }

    private fun storeFavoriteMovie(movie: Movie) {
        moviesRepository.addFavoriteMovie(movie)
    }

    private lateinit var view: TopMoviesContract.View

    private val topMovies = arrayListOf<Movie>()
    private val storedTopFavorites = arrayListOf<Movie>()

    override fun setView(view: TopMoviesContract.View) {
        this.view = view
    }

    override fun onFragmentLoadGetTopMovies() {
        apiInteractor.getTopMovies(topMoviesCallback())
    }

    private fun topMoviesCallback(): Callback<MoviesResponse> =
        object : Callback<MoviesResponse> {
            override fun onFailure(call: Call<MoviesResponse>, t: Throwable) {
                t.printStackTrace()
            }

            override fun onResponse(
                call: Call<MoviesResponse>,
                response: Response<MoviesResponse>
            ) {

                if (response.isSuccessful) {
                    when (response.code()) {
                        RESPONSE_OK ->
                            response.body()?.movies?.run { handleOkResponse(this) }
                        RESPONSE_BAD_REQUEST -> showBadRequestToast()
                        RESPONSE_NOT_FOUND -> showNotFoundToast()
                        SERVER_ERROR -> showServerErrorToast()
                        else -> handleSomethingWentWrong()
                    }
                }
            }
        }


    private fun handleSomethingWentWrong() {
        view.topMoviesFetchFailure()
    }

    private fun handleOkResponse(topMovies: ArrayList<Movie>) {
        modifyTopMoviesList(topMovies)
        view.topMoviesFetchSuccess(topMovies)
    }

    private fun modifyTopMoviesList(topMovies: ArrayList<Movie>) {
        this.topMovies.clear()
        this.topMovies.addAll(topMovies)
        mergeTopFavoriteMovies()
    }

    private fun mergeTopFavoriteMovies() {
        fetchTopFavoriteMovies()

        if (!storedTopFavorites.isNullOrEmpty()) {
            storedTopFavorites.forEach {
                if (it.isTopFavorite) {
                    val index = topMovies.indexOf(it)
                    topMovies.elementAt(index).isFavorite = it.isFavorite
                }
            }
        }
    }

    private fun fetchTopFavoriteMovies() {
        storedTopFavorites.clear()
        storedTopFavorites.addAll(moviesRepository.getFavoriteMovies())
    }


}