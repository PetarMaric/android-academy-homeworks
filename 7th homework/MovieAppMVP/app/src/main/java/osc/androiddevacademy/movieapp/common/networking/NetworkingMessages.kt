package osc.androiddevacademy.movieapp.common.networking

import osc.androiddevacademy.movieapp.App
import osc.androiddevacademy.movieapp.R
import osc.androiddevacademy.movieapp.common.displayToast

fun showBadRequestToast() =
    App.getAppContext().let { it.displayToast(it.getString(R.string.badRequestText)) }

fun showNotFoundToast() =
    App.getAppContext().let { it.displayToast(it.getString(R.string.notFoundText)) }

fun showServerErrorToast() =
    App.getAppContext().let { it.displayToast(it.getString(R.string.serverErrorText)) }