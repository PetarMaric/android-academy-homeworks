package osc.androiddevacademy.movieapp.ui.moviespager.moviedetail

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.item_review.*

class ReviewViewHolder(override val containerView: View) : RecyclerView.ViewHolder(containerView),
    LayoutContainer {

    fun bind(author: String, description: String) {
        reviewAuthor.text = author
        reviewContent.text = description
    }
}