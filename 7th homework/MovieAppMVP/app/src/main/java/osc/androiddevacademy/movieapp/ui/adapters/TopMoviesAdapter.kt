package osc.androiddevacademy.movieapp.ui.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import osc.androiddevacademy.movieapp.R
import osc.androiddevacademy.movieapp.ui.topmovies.TopMoviesContract
import osc.androiddevacademy.movieapp.ui.topmovies.TopMoviesViewHolder

class TopMoviesAdapter(private val presenter: TopMoviesContract.Presenter) :
    RecyclerView.Adapter<TopMoviesViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TopMoviesViewHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.item_top_movie, parent, false)
        return TopMoviesViewHolder(view)
    }

    override fun getItemCount(): Int = presenter.getTopMoviesListSize()

    override fun onBindViewHolder(holder: TopMoviesViewHolder, position: Int) {
        presenter.onBindTopMovieAtPosition(holder, position)
    }


}