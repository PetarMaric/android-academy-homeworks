package osc.androiddevacademy.movieapp.common.networking

//Status Codes
const val RESPONSE_OK = 200
const val RESPONSE_BAD_REQUEST = 400
const val RESPONSE_NOT_FOUND = 404
const val SERVER_ERROR = 500