package osc.androiddevacademy.movieapp.ui.moviesgrid.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import kotlinx.android.synthetic.main.fragment_movie_grid.*
import osc.androiddevacademy.movieapp.R
import osc.androiddevacademy.movieapp.common.displayToast
import osc.androiddevacademy.movieapp.common.showFragment
import osc.androiddevacademy.movieapp.database.MoviesRoomRepository
import osc.androiddevacademy.movieapp.model.Movie
import osc.androiddevacademy.movieapp.networking.BackendFactory
import osc.androiddevacademy.movieapp.presentation.MoviesGridFragmentPresenter
import osc.androiddevacademy.movieapp.ui.adapters.gridadapter.MoviesGridAdapter
import osc.androiddevacademy.movieapp.ui.moviespager.fragment.MoviesPagerFragment

class MoviesGridFragment : Fragment(), MoviesContract.View {
    private val SPAN_COUNT = 2

    private val gridAdapter by lazy {
        MoviesGridAdapter(
            { onMovieClicked(it) },
            { onFavoriteClicked(it) })
    }


    private val presenter: MoviesContract.Presenter by lazy {
        MoviesGridFragmentPresenter(
            BackendFactory.getMovieInteractor(),
            MoviesRoomRepository()
        )
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_movie_grid, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        presenter.onGridFragmentLoadGetMovies()

        moviesGrid.apply {
            adapter = gridAdapter
            layoutManager = GridLayoutManager(context, SPAN_COUNT)
        }
    }

    override fun onResume() {
        super.onResume()
        presenter.setView(this)
    }

    override fun onMoviesFetchSuccess(movies: MutableList<Movie>) {
        onMoviesReceived(movies)
    }

    private fun onMoviesReceived(movies: MutableList<Movie>) = gridAdapter.setMovies(movies)

    override fun onMoviesFetchFailure() {
        activity?.displayToast("There was unexpected error. Please try again later.")
    }

    override fun onFavoriteMovieAdded(movie: Movie) {
        activity?.displayToast("'${movie.title}' added to favorites.")
    }

    override fun onFavoriteMovieRemoved(movie: Movie) {
        activity?.displayToast("'${movie.title}' removed from favorites.")
    }

    private fun onMovieClicked(movie: Movie) {
        activity?.showFragment(
            R.id.mainFragmentHolder,
            MoviesPagerFragment.getInstance(
                presenter.requestMovies(),
                movie
            ),
            true
        )
    }

    private fun onFavoriteClicked(movie: Movie) {
        presenter.setFavoriteMovie(movie)
    }

}