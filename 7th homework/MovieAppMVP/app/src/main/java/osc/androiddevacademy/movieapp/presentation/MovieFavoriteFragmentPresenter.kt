package osc.androiddevacademy.movieapp.presentation

import osc.androiddevacademy.movieapp.database.MoviesRepository
import osc.androiddevacademy.movieapp.model.Movie
import osc.androiddevacademy.movieapp.ui.favoritesgrid.FavoritesContract

class MovieFavoriteFragmentPresenter(
    private val moviesRepository: MoviesRepository
) : FavoritesContract.Presenter {

    private val favoriteMovieList = arrayListOf<Movie>()

    private lateinit var view: FavoritesContract.View

    override fun unfavoriteMovie(movie: Movie) {
        moviesRepository.deleteFavoriteMovie(movie)
        view.onFavoriteMovieRemovedNotify(movie)
    }

    override fun requestFavoriteMovies(): ArrayList<Movie> = favoriteMovieList


    override fun onFragmentLoadGetMovies() {
        favoriteMovieList.clear()
        val retrievedFavoriteMovies = moviesRepository.getFavoriteMovies()
        favoriteMovieList.addAll(retrievedFavoriteMovies)

        if (favoriteMovieList.isNullOrEmpty()) {
            view.favoritesFetchEmpty()
        } else {
            view.favoritesFetchSuccess(favoriteMovieList)
        }
    }

    override fun setView(view: FavoritesContract.View) {
        this.view = view
    }
}