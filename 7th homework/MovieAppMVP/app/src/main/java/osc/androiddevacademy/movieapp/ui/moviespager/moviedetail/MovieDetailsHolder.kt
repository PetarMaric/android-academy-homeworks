package osc.androiddevacademy.movieapp.ui.moviespager.moviedetail

import osc.androiddevacademy.movieapp.model.Review

interface MovieDetailsHolder {

    fun setReviewAuthor(review: Review)

    fun setReviewContent(review: Review)
}