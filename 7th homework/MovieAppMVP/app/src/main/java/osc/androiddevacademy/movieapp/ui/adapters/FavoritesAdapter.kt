package osc.androiddevacademy.movieapp.ui.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import osc.androiddevacademy.movieapp.R
import osc.androiddevacademy.movieapp.ui.favoritesgrid.FavoritesContract
import osc.androiddevacademy.movieapp.ui.favoritesgrid.FavoritesViewHolder

class FavoritesAdapter(private val presenter: FavoritesContract.Presenter) :
    RecyclerView.Adapter<FavoritesViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FavoritesViewHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.item_favorite_movie, parent, false)
        return FavoritesViewHolder(view)
    }

    override fun getItemCount(): Int = presenter.getFavoriteMovieListSize()

    override fun onBindViewHolder(holder: FavoritesViewHolder, position: Int) {
        presenter.onBindFavoriteMovieAtPosition(holder, position)
    }
}