package osc.androiddevacademy.movieapp.ui.favoritesgrid

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.item_favorite_movie.*
import osc.androiddevacademy.movieapp.common.loadImage
import osc.androiddevacademy.movieapp.model.Movie

class FavoritesViewHolder(override val containerView: View) :
    RecyclerView.ViewHolder(containerView), LayoutContainer, FavoritesViewHolderContract {
    override fun setMovie(movie: Movie) {
        movieImage.loadImage(movie.poster)
    }
}