import characters.*
import models.Option
import models.combat.Armor
import models.combat.magic.base_magic.Spell
import models.combat.weapons.Bow
import models.combat.weapons.Claws
import models.combat.weapons.Longsword
import models.combat.weapons.StoneHands
import models.consumables.HealthPotion
import kotlin.system.exitProcess

fun main() {
    var heroAlive: Boolean
    var userInput: Int?
    var options: Set<Option>

    //region Weapons
    val longsword = Longsword(id = 1, name = "Glamdring", minDamage = 100, maxDamage = 450)

    val daedricSword = Longsword(id = 2, name = "Greater daedric sword", minDamage = 200, maxDamage = 201)

    val elvenBow = Bow(id = 3, name = "Elven bow", minDamage = 25, maxDamage = 250)

    val helsing = Bow(id = 4, name = "Helsing", minDamage = 25, maxDamage = 350)

    val setOfWeapons = setOf(longsword, elvenBow, daedricSword, helsing)

    val setOfWeaponIds = setOfWeapons.map { it.id }.toSet()
    //endregion

    //region User weapon selection
    var userWeaponInput: Int?
    do {
        println("Please enter the number of the weapon you want to use: ")
        setOfWeapons.sortedBy { it.id }
            .forEach { println("\t\t\t${it.id} -> ${it.name} - miniumum damage: ${it.minDamage}  - maximum damage: ${it.maxDamage}") }
        print("Choose your weapon: ")
        userWeaponInput = readLine()?.toInt()
    } while (!setOfWeaponIds.contains(userWeaponInput))

    println(
        "You have chosen: ${setOfWeapons.find { it.id == userWeaponInput }!!.name} with ${setOfWeapons.find { it.id == userWeaponInput }!!.minDamage} minimal damage and ${setOfWeapons.find { it.id == userWeaponInput }!!.maxDamage} maximum damage."
    )

    val testHero = Hero(
        "Geralt of Rivia",
        10000,
        mutableListOf<HealthPotion>(),
        Armor("Griffin school", 1000, 100),
        setOfWeapons.find { it.id == userWeaponInput }!!,
        setOf<Spell>()
    )
    testHero.generateConsumables()

    newlineGenerator(5)

    println("You have chosen ${testHero.name} with weapon ${testHero.weapon.name} which deals ${testHero.weapon.minDamage} minimum damage and ${testHero.weapon.maxDamage} maximum damage.")
    println("\t\t\t\tYou have ${testHero.inventory.count()} healing potions at your disposal. Happy fighting!")

    newlineGenerator(5)
    //endregion


    // region Wolf attack
    // region Wolfpack
    val wolf1 = Enemy(
        "Wolf", 1000,
        Armor("None", 0, 0),
        Claws(4, "Claws", 20, 30)
    )

    val wolf2 = Enemy(
        "Wolf", 1000,
        Armor("None", 0, 0),
        Claws(4, "Claws", 20, 30)
    )

    val wolf3 = Enemy(
        "Wolf", 1000,
        Armor("None", 0, 0),
        Claws(4, "Claws", 20, 30)
    )

    val wolf4 = Enemy(
        "Wolf", 1000,
        Armor("None", 0, 0),
        Claws(4, "Claws", 20, 30)
    )
    val wolf5 = Enemy(
        "Wolf", 1000,
        Armor("None", 0, 0),
        Claws(5, "Claws", 20, 30)
    )

    val wolfPack = mutableSetOf<Enemy>(wolf1, wolf2, wolf3, wolf4, wolf5)


    heroAlive = testHero.fightEnemies(wolfPack)

    testHero.isHeroAlive(testHero, heroAlive)


    println("You have successfully beaten the wolf pack. Now you can rest while you are at peace.")
    testHero.resetHp()


    //endregion
    //endregion

    options = setOf(
        Option(1, "Proceed on your path to glory."),
        Option(2, "Go back home because you don't see the point of fighting.")
    )

    do {

        options.forEach { println("${it.id} - ${it.optionText}") }
        userInput = readLine()?.toInt()
    } while (!options.map { it.id }.contains(userInput))

    if (userInput == options.elementAt(1).id) {
        exitProcess(-1)
    }

    val bear1 = Enemy(
        "Bear", 1000,
        Armor("Fur", 50, 0),
        Claws(5, "Claws", 20, 30)
    )
    val bear2 = Enemy(
        "Bear", 1000,
        Armor("Fur", 50, 0),
        Claws(6, "Claws", 20, 30)
    )

    println("Two bears appeared behind the bushes. Quickly, do something")

    val twoBears = mutableSetOf<Enemy>(bear1, bear2)
    heroAlive = testHero.fightEnemies(twoBears)
    testHero.isHeroAlive(testHero, heroAlive)

    println("You have successfully beaten few bears. Now you can rest while you are at peace.")
    testHero.resetHp()


    println("Only one step remains on your journey to end the reign of the stone king.")

    options = setOf(
        Option(1, "Continue your noble quest."),
        Option(2, "Run away and forget everything.")
    )

    do {

        options.forEach { println("${it.id} - ${it.optionText}") }
        userInput = readLine()?.toInt()
    } while (!options.map { it.id }.contains(userInput))

    if (userInput == options.elementAt(1).id) {
        exitProcess(-1)
    }

    val stoneGolem = Enemy(
        "Stone golem", 7000,
        Armor("Stone", 50, 0),
        StoneHands(6, "Stone hands", 120, 300)
    )

    val bossFightEnemy = mutableSetOf(stoneGolem)

    heroAlive = testHero.fightEnemies(bossFightEnemy)
    testHero.isHeroAlive(testHero, heroAlive)

    println("You have successfully beaten the boss. Your king will be proud. Good game!")

    println()
}



fun newlineGenerator(numberOfRows: Int) {
    for (i in 1 until numberOfRows)
        println()
}
