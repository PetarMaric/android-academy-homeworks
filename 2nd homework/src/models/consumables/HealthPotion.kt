package models.consumables

import characters.base_character.Creature
import models.consumables.base_consumable.Consumable
import models.interfaces.Heal

class HealthPotion(
    name: String,
    val healthPointsToRestore: Int
) : Consumable(name), Heal {

    override fun heal(creature: Creature) {
        creature.health += healthPointsToRestore
        println("The ${creature.name} was successfully healed for ${healthPointsToRestore} and now has ${creature.health} health points left.")
    }
}