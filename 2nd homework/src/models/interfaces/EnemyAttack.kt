package models.interfaces

import characters.Hero

interface EnemyAttack {
    fun attack(hero: Hero): Int
}