package models.interfaces

import characters.base_character.Creature

interface HeroAttack {

    fun lightAttack(creatureToDamage: Creature): Int

    fun heavyAttack(creatureToDamage: Creature): Int

    fun attack(creatureToDamage: Creature): Int
}