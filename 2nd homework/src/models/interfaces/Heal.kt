package models.interfaces

import characters.base_character.Creature

interface Heal {
    fun heal(creature: Creature)
}