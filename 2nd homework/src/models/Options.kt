package models

class Option(
    val id: Int,
    val optionText: String
)