package models.combat.magic

import models.combat.magic.base_magic.Spell

class FireSpell(
    name: String,
    damage: Int,
    val burningDamage: Int = 10,
    val burningDamageDuration: Int = 60
) : Spell(name, damage)