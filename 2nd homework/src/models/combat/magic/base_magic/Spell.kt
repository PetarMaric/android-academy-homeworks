package models.combat.magic.base_magic

abstract class Spell(
    val name: String,
    val damage: Int
)