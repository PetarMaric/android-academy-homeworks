package models.combat.magic

import models.combat.magic.base_magic.Spell

class WaterSpell(
    name: String,
    damage: Int,
    val aoeDamage: Int = 200
) : Spell(name, damage)