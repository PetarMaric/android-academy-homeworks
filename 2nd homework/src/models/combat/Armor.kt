package models.combat

class Armor(
    val name: String,
    val protection: Int,
    var durability: Int = 100
)