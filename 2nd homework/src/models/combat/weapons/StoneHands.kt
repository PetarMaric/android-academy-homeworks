package models.combat.weapons

import models.combat.weapons.base_weapon.Weapon

class StoneHands(
    id: Int,
    name: String,
    minDamage: Int,
    maxDamage: Int,
    val splashDamage: Int = 50
) : Weapon(id, name, minDamage, maxDamage)