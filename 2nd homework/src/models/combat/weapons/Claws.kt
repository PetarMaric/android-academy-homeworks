package models.combat.weapons

import models.combat.weapons.base_weapon.Weapon

class Claws(
    id: Int,
    name: String,
    minDamage: Int,
    maxDamage: Int,
    val piercingDamage: Int = 20
) : Weapon(id, name, minDamage, maxDamage) {
}