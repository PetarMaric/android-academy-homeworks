package models.combat.weapons

import models.combat.weapons.base_weapon.Weapon

class Bow(
    id: Int,
    name: String,
    minDamage: Int,
    maxDamage: Int,
    val drawLength: Int = 68,
    val bowSize: Int = 172,
    val availableArrows: Int = 50,
    val bowCondition: Enum<Condition> = Condition.WORN
) : Weapon(id, name, minDamage, maxDamage)