package models.combat.weapons

import models.combat.weapons.base_weapon.Weapon

class Longsword(
    id: Int,
    name: String,
    minDamage: Int,
    maxDamage: Int,
    var sharpness: Int = 100,
    var swordCondition: Enum<Condition> = Condition.PRISTINE,
    val length: Int = 120

) : Weapon(id, name, minDamage, maxDamage)