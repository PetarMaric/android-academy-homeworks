package models.combat.weapons.base_weapon

abstract class Weapon(
    val id: Int,
    val name: String,
    val minDamage: Int,
    val maxDamage: Int
)