package models.combat.weapons

enum class Condition {
    PRISTINE,
    WORN,
    DAMAGED,
    BADLY_DAMAGED,
    RUINED;
}