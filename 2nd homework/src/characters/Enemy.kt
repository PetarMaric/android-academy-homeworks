package characters

import characters.base_character.Creature
import models.combat.Armor
import models.combat.weapons.base_weapon.Weapon
import models.interfaces.EnemyAttack
import kotlin.random.Random

abstract class Antagonist(name: String, health: Int) : Creature(name, health)

class Enemy(
    name: String,
    health: Int,
    val armor: Armor,
    val weapon: Weapon

) : Antagonist(name, health), EnemyAttack {

    override fun attack(hero: Hero): Int {
        return Random.nextInt(weapon.minDamage, weapon.maxDamage)
    }
}