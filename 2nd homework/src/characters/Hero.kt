package characters

import CRITICAL_CHANCE
import MAX_HP_FROM_CONSUMABLE
import MAX_NUMBER_OF_CONSUMABLES
import characters.base_character.Creature
import models.Option
import models.combat.Armor
import models.combat.magic.base_magic.Spell
import models.combat.weapons.base_weapon.Weapon
import models.consumables.HealthPotion
import models.interfaces.HeroAttack
import newlineGenerator
import kotlin.random.Random
import kotlin.system.exitProcess


fun willHeroCrit() = Random.nextInt(0, 100)

fun Hero.fightEnemies(enemies: MutableSet<Enemy>): Boolean {

    var crit: Boolean = false
    var heroAlive: Boolean = true

    val enemyCount = enemies.count()

    val options = setOf(
        Option(1, "light attack"),
        Option(2, "heavy attack"),
        Option(3, "drink health potion and attack")
    )

    var enemyToEngage: Enemy

    var damageToEnemy: Int


    for (j in 0 until enemyCount) {
        damageToEnemy = 0

        while (heroAlive) {

            enemyToEngage = enemies.elementAt(j)

            var userInput: Int?

            do {
                options.forEach { println("${it.id} - ${it.optionText}") }
                userInput = readLine()?.toInt()
            } while (!options.map { it.id }.contains(userInput))

            when (userInput) {
                1 -> damageToEnemy = this.lightAttack(enemyToEngage)
                2 -> damageToEnemy = this.heavyAttack(enemyToEngage)
                else -> {
                    if (this.inventory.count() >= 1) {
                        this.inventory.first().heal(this)
                        this.inventory.removeAt(0)
                        println("You have ${this.inventory.count()} potions left in your inventory.")
                    } else {
                        println("/*/*/*/*        Your inventory is empty.        *\\*\\*\\*\\")
                        newlineGenerator(2)
                    }
                }
            }

            if (willHeroCrit() > 100 - CRITICAL_CHANCE) {
                damageToEnemy *= 3
                crit = true
            }

            enemyToEngage.health -= damageToEnemy

            println("${this.name} dealt $damageToEnemy ${if (crit) "critical" else ""} damage to the fierce enemy called ${enemyToEngage.name}.")

            println("\t${enemyToEngage.name} ${if (enemyToEngage.health > 0) "has ${enemyToEngage.health} health points remaining." else "has perished."} ")

            if (enemyToEngage.health < 0) {
                    println(
                        "*************************************************              Hooray, ${this.name} had successfully beaten ${j + 1}${when (j) {
                            0 -> "st"
                            1 -> "nd"
                            2 -> "rd"
                            else -> "th"
                        }} ${enemyToEngage.name}.             *************************************************"
                    )
                newlineGenerator(2)
                break
            }

            val damageToHero = enemyToEngage.attack(this)

            this.health -= damageToHero

            println("${enemyToEngage.name} dealt $damageToHero damage to your hero ${this.name}.")
            println("\t${this.name} ${if (this.health > 0) "has ${this.health} health points remaining." else "has perished."} ")


            if (this.health <= 0) {
                println("Your hero has died in battle.")
                heroAlive = false
                return false
            }


            print("\u001b[H\u001b[2J")
            newlineGenerator(2)
        }


    }
    return true
}

abstract class Protagonist(name: String, health: Int) : Creature(name, health)

class Hero(
    heroName: String,
    health: Int,
    var inventory: MutableList<HealthPotion>,
    val armour: Armor,
    val weapon: Weapon,
    val spells: Set<Spell>
) : Protagonist(heroName, health), HeroAttack {


    override fun lightAttack(creatureToDamage: Creature): Int {
        return attack(creatureToDamage)
    }

    override fun heavyAttack(creatureToDamage: Creature): Int {
        return attack(creatureToDamage) * 2
    }

    override fun attack(creatureToDamage: Creature): Int {
        val damageToBeDealt = Random.nextInt(weapon.minDamage, weapon.maxDamage)
        return damageToBeDealt
    }

    fun resetHp() {
        newlineGenerator(2)
        this.health = maxHp
        println("Your hero hp has been restored to default of ${maxHp}")
        newlineGenerator(2)
    }

    fun generateConsumables() {
        var consumablesToGenerate = Random.nextInt(1, MAX_NUMBER_OF_CONSUMABLES)
        val listOfConsumables: MutableList<HealthPotion> = mutableListOf()
        var i = 0
        while (consumablesToGenerate > 0) {
            listOfConsumables.add(
                HealthPotion(
                    name = "Potion$i",
                    healthPointsToRestore = Random.nextInt(100, MAX_HP_FROM_CONSUMABLE)
                )
            )
            i++
            consumablesToGenerate--
        }
        this.inventory = listOfConsumables
    }

    fun isHeroAlive(hero: Hero, heroAlive: Boolean) {
        if (!heroAlive) {
            newlineGenerator(4)
            println("Your hero ${hero.name} has died bravely in the heart of battle.")
            exitProcess(1)
        }
    }

}



