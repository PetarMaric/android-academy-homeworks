package characters.base_character

abstract class Creature(
    val name: String,
    var health: Int
) {
    var maxHp = health

}