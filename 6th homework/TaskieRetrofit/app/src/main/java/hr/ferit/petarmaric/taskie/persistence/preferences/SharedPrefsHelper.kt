package hr.ferit.petarmaric.taskie.persistence.preferences

interface SharedPrefsHelper {

    fun getUserToken(): String

    fun storeUserToken(token: String)

    fun clearUserToken()
}