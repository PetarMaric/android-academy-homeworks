package hr.ferit.petarmaric.taskie.networking

import hr.ferit.petarmaric.taskie.R
import hr.ferit.petarmaric.taskie.Taskie
import hr.ferit.petarmaric.taskie.common.displayToast

fun showBadRequestToast() =
    Taskie.getApplicationContext().let { it.displayToast(it.getString(R.string.badRequestText)) }

fun showNotFoundToast() = Taskie.getApplicationContext().let { it.displayToast(it.getString(R.string.notFoundText)) }
fun showServerErrorToast() =
    Taskie.getApplicationContext().let { it.displayToast(it.getString(R.string.serverErrorText)) }