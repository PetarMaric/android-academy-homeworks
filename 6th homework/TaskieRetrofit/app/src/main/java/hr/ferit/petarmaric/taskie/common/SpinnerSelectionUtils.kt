package hr.ferit.petarmaric.taskie.common

import hr.ferit.petarmaric.taskie.model.PriorityColor

fun determinePriorityLocation(storedPriority: String): Int {
    val priorities = PriorityColor.values().map { it.toString() }.toSet()

    val index = priorities.indexOf(storedPriority)
    return index
}