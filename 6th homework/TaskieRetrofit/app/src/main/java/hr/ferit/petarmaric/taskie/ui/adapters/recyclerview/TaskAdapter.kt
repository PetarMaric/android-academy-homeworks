package hr.ferit.petarmaric.taskie.ui.adapters.recyclerview

import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.Adapter
import hr.ferit.petarmaric.taskie.R
import hr.ferit.petarmaric.taskie.Taskie
import hr.ferit.petarmaric.taskie.common.*
import hr.ferit.petarmaric.taskie.model.BackendTask
import hr.ferit.petarmaric.taskie.model.Task
import hr.ferit.petarmaric.taskie.model.response.GetTasksResponse
import hr.ferit.petarmaric.taskie.networking.interactors.BackendFactory
import hr.ferit.petarmaric.taskie.networking.showBadRequestToast
import hr.ferit.petarmaric.taskie.networking.showNotFoundToast
import hr.ferit.petarmaric.taskie.networking.showServerErrorToast
import hr.ferit.petarmaric.taskie.persistence.database.TasksRepository
import hr.ferit.petarmaric.taskie.persistence.database.TasksRoomRepository
import hr.ferit.petarmaric.taskie.ui.fragments.tasks.TasksFragment
import kotlinx.android.synthetic.main.fragment_tasks.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class TaskAdapter(private val onItemSelected: (BackendTask) -> Unit) : Adapter<TaskHolder>() {

    private val repository: TasksRepository = TasksRoomRepository()

    private val interactor = BackendFactory.getTaskieInteractor()

    private val data: MutableList<BackendTask> = mutableListOf()

    companion object {
        lateinit var recyclerViewInstance: RecyclerView
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TaskHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.item_task, parent, false)
        return TaskHolder(v)
    }

    override fun getItemCount() = data.size

    override fun onBindViewHolder(holder: TaskHolder, position: Int) {
        holder.bindData(data[position], onItemSelected)
    }

    override fun onAttachedToRecyclerView(recyclerView: RecyclerView) {
        super.onAttachedToRecyclerView(recyclerView)

        recyclerViewInstance = recyclerView
    }

    fun setData(data: List<BackendTask>) {
        this.data.clear()
        this.data.addAll(data)
        notifyDataSetChanged()
    }

    fun sortTasks() {
        // interactor.getTasks(getTaskieCallback())
        data.sortByDescending { it.taskPriority }
        notifyDataSetChanged()
    }

    private fun getTaskieCallback(): Callback<GetTasksResponse> = object : Callback<GetTasksResponse> {
        override fun onFailure(call: Call<GetTasksResponse>?, t: Throwable?) {
            Log.d(TasksFragment.REQUEST_ERROR, t?.localizedMessage)
            Log.d(TasksFragment.REQUEST_ERROR, call?.request().toString())
        }

        override fun onResponse(call: Call<GetTasksResponse>?, response: Response<GetTasksResponse>) {
            if (response.isSuccessful) {
                when (response.code()) {
                    RESPONSE_OK -> handleOkResponse(response)
                    RESPONSE_BAD_REQUEST -> showBadRequestToast()
                    RESPONSE_NOT_FOUND -> showNotFoundToast()
                    SERVER_ERROR -> showServerErrorToast()
                    else -> handleSomethingWentWrong()
                }
            }
        }
    }

    private fun handleOkResponse(response: Response<GetTasksResponse>) {
        response.body()?.notes?.run {
            data.clear()
            data.addAll(this)
            notifyDataSetChanged()
        }
    }

    private fun handleSomethingWentWrong() {
        Taskie.getApplicationContext().displayToast("Something went wrong!")
    }

    fun getTaskAtPosition(position: Int) = data[position]


    fun refreshData() {
        interactor.getTasks(getTaskieCallback())
    }



    fun deleteData(item: BackendTask) {
        val deletedIndex = data.indexOf(item)

        data.remove(item)

        notifyItemRemoved(deletedIndex)

    }

    fun getData() = data
}





