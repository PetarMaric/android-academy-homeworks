package hr.ferit.petarmaric.taskie.networking.interactors

import hr.ferit.petarmaric.taskie.model.BackendTask
import hr.ferit.petarmaric.taskie.model.request.*
import hr.ferit.petarmaric.taskie.model.response.*
import retrofit2.Callback

interface TaskieInteractor {

    fun getTasks(taskieResponseCallback: Callback<GetTasksResponse>)

    fun getTask(request: TaskRequest, taskResponseCallback: Callback<BackendTask>)

    fun register(request: UserDataRequest, registerCallback: Callback<RegisterResponse>)

    fun login(request: UserDataRequest, loginCallback: Callback<LoginResponse>)

    fun save(request: AddTaskRequest, saveCallback: Callback<BackendTask>)

    fun update(request: UpdateTaskRequest, updateCallback: Callback<BackendTask>)

    fun delete(request: DeleteTaskRequest, deleteCallback: Callback<DeleteResponse>)
}