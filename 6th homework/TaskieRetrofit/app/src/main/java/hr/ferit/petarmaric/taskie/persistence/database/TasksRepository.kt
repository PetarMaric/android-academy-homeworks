package hr.ferit.petarmaric.taskie.persistence.database

import hr.ferit.petarmaric.taskie.model.Task
import hr.ferit.petarmaric.taskie.model.TaskToSave


interface TasksRepository {

    fun save(task: Task): Task

    fun get(id: String): Task

    fun getTaskByRetrofitId(stringId: String): Task

    fun getSortedTasks(): List<Task>

    fun getAllTasks(): MutableList<Task>

    fun getAllPendingTasks(): MutableList<TaskToSave>

    fun storeAllTasks(taskList: MutableList<Task>)

    fun storePendingTask(task: TaskToSave)

    fun updateTask(task: Task)

    fun deleteTask(task: Task)

    fun deleteAllTasks()

    fun deleteAllPendingTasks()

}