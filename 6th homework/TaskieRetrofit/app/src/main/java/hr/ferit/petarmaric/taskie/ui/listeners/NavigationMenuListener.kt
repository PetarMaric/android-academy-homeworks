package hr.ferit.petarmaric.taskie.ui.listeners

import android.view.MenuItem
import com.google.android.material.bottomnavigation.BottomNavigationView
import hr.ferit.petarmaric.taskie.R
import hr.ferit.petarmaric.taskie.common.showFragment
import hr.ferit.petarmaric.taskie.ui.activities.base.BaseActivity
import hr.ferit.petarmaric.taskie.ui.fragments.about.About
import hr.ferit.petarmaric.taskie.ui.fragments.tasks.TasksFragment

class NavigationMenuListener(private val context: BaseActivity) :
    BottomNavigationView.OnNavigationItemSelectedListener {


    override fun onNavigationItemSelected(item: MenuItem): Boolean {

        when (item.itemId) {
            R.id.taskList -> {
                context.showFragment(R.id.fragmentContainer, TasksFragment.newInstance())
            }
            R.id.about
            -> {
                context.showFragment(R.id.fragmentContainer, About.newInstance())
            }
        }
        return true
    }

}