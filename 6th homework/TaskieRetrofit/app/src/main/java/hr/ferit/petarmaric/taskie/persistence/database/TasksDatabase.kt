package hr.ferit.petarmaric.taskie.persistence.database

import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import hr.ferit.petarmaric.taskie.Taskie
import hr.ferit.petarmaric.taskie.model.Task
import hr.ferit.petarmaric.taskie.model.TaskToSave
import hr.ferit.petarmaric.taskie.persistence.database.daos.TasksDao
import hr.ferit.petarmaric.taskie.persistence.database.daos.TasksToSaveDao

@Database(entities = [Task::class, TaskToSave::class], version = 1, exportSchema = false)
@TypeConverters(RoomTypeConverters::class)
abstract class TasksDatabase : RoomDatabase() {

    abstract fun getTasksDao(): TasksDao
    abstract fun getTasksToSaveDao(): TasksToSaveDao

    companion object {
        private val DB_NAME = "tasks_database"
        private val dbInstance by lazy {
            Room.databaseBuilder(
                Taskie.getApplicationContext(), TasksDatabase::class.java,
                DB_NAME
            ).allowMainThreadQueries()
                .build()
        }

        val tasksDao by lazy { dbInstance.getTasksDao() }
        val tasksToSaveDao by lazy { dbInstance.getTasksToSaveDao() }
    }
}