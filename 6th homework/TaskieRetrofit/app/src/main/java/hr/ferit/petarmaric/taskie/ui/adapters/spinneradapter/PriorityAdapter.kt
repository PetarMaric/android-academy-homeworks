package hr.ferit.petarmaric.taskie.ui.adapters.spinneradapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import hr.ferit.petarmaric.taskie.R
import hr.ferit.petarmaric.taskie.model.PriorityColor


import kotlinx.android.synthetic.main.item_priority_spinner_item.view.*

class PriorityAdapter(context: Context, val layout: Int, private val priorities: List<PriorityColor>) :
    ArrayAdapter<PriorityColor>(context, layout, priorities) {

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        return initView(position, convertView, parent)
    }

    override fun getDropDownView(position: Int, convertView: View?, parent: ViewGroup): View {
        return initView(position, convertView, parent)
    }

    private fun initView(position: Int, convertView: View?, parent: ViewGroup): View {
        var view = convertView
        if (view == null) {
            view = LayoutInflater.from(context).inflate(R.layout.item_priority_spinner_item, parent, false)
        }

        val currentPriority = priorities.get(position)

        if (view != null) {
            view.priorityColor.setImageResource(currentPriority.getColor())
            view.priorityText.text = currentPriority.name

        }

        return view!!
    }
}