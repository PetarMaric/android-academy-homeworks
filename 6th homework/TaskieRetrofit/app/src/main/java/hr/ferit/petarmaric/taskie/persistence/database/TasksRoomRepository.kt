package hr.ferit.petarmaric.taskie.persistence.database

import hr.ferit.petarmaric.taskie.model.Task
import hr.ferit.petarmaric.taskie.model.TaskToSave

class TasksRoomRepository : TasksRepository {

    private val tasksDao = TasksDatabase.tasksDao

    private val tasksToSaveDao = TasksDatabase.tasksToSaveDao

    override fun save(task: Task): Task {
        val task = Task(
            title = task.title,
            content = task.content,
            taskPriority = task.taskPriority,
            isCompleted = false,
            isFavorite = false,
            retrofitTaskId = task.retrofitTaskId
        )
        tasksDao.storeTask(task)
        return task
    }

    override fun get(id: String): Task {
        return tasksDao.getTask(id)
    }

    override fun getTaskByRetrofitId(stringId: String): Task {
        return tasksDao.getTaskByRetrofitId(stringId)
    }

    override fun getAllTasks(): MutableList<Task> {
        return tasksDao.getAllTasks()
    }

    override fun storeAllTasks(taskList: MutableList<Task>) {
        tasksDao.storeAllTasks(taskList)
    }

    override fun getSortedTasks(): List<Task> {
        return tasksDao.getTasksOrderedByPriority()
    }

    override fun getAllPendingTasks(): MutableList<TaskToSave> {
        return tasksToSaveDao.getAllTasks()
    }

    override fun updateTask(task: Task) {
        tasksDao.updateTask(task)
    }

    override fun deleteTask(task: Task) {
        tasksDao.deleteTask(task)
    }

    override fun deleteAllTasks() {
        tasksDao.deleteAllTasks()
    }

    override fun deleteAllPendingTasks() {
        tasksToSaveDao.deleteAllPendingTasks()
    }

    override fun storePendingTask(task: TaskToSave) {
        tasksToSaveDao.storeTask(task)
    }

}