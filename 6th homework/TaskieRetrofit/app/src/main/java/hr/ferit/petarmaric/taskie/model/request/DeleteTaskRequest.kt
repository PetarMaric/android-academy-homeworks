package hr.ferit.petarmaric.taskie.model.request

data class DeleteTaskRequest(val taskNoteId: String)