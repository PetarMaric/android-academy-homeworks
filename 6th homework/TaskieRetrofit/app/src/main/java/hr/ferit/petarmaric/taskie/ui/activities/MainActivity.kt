package hr.ferit.petarmaric.taskie.ui.activities

import android.app.AlertDialog
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import hr.ferit.petarmaric.taskie.R
import hr.ferit.petarmaric.taskie.Taskie
import hr.ferit.petarmaric.taskie.common.*
import hr.ferit.petarmaric.taskie.model.request.DeleteTaskRequest
import hr.ferit.petarmaric.taskie.model.response.DeleteResponse
import hr.ferit.petarmaric.taskie.networking.interactors.BackendFactory
import hr.ferit.petarmaric.taskie.networking.interactors.TaskieInteractorImplementation
import hr.ferit.petarmaric.taskie.networking.showBadRequestToast
import hr.ferit.petarmaric.taskie.networking.showNotFoundToast
import hr.ferit.petarmaric.taskie.networking.showServerErrorToast
import hr.ferit.petarmaric.taskie.persistence.database.TasksRepository
import hr.ferit.petarmaric.taskie.persistence.database.TasksRoomRepository
import hr.ferit.petarmaric.taskie.ui.activities.base.BaseActivity
import hr.ferit.petarmaric.taskie.ui.adapters.recyclerview.TaskAdapter
import hr.ferit.petarmaric.taskie.ui.fragments.tasks.TasksFragment
import hr.ferit.petarmaric.taskie.ui.listeners.NavigationMenuListener
import kotlinx.android.synthetic.main.navigation_bottom_navigation.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MainActivity : BaseActivity() {

    private val repository: TasksRepository = TasksRoomRepository()
    private val interactor = BackendFactory.getTaskieInteractor()

    override fun getLayoutResourceId() = R.layout.activity_main

    override fun setUpUi() {

        showFragment(TasksFragment.newInstance())

        bottomNavigationMaterial.setOnNavigationItemSelectedListener(NavigationMenuListener(this))

    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        super.onCreateOptionsMenu(menu)
        menuInflater.inflate(R.menu.menu_options, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.sortTasksByPriority -> sortTasksByPriority()
            R.id.deleteAllTasks -> deleteAllTasks()
        }
        return super.onOptionsItemSelected(item)
    }

    private fun deleteAllTasks() {
        val recyclerViewAdapterInstance = (TaskAdapter.recyclerViewInstance.adapter as TaskAdapter)

        AlertDialog.Builder(this, R.style.AppCompatAlertDialogStyle)
            .setTitle("Do you want to delete all tasks?")
            .setPositiveButton("All of 'em") { _, _ ->

                AlertDialog.Builder(this, R.style.AppCompatAlertDialogStyle)
                    .setTitle("Are you sure? List can not be recreated!")
                    .setPositiveButton("Delete") { _, _ ->
                        val dataList = recyclerViewAdapterInstance.getData()

                        dataList.forEach {
                            // recyclerViewAdapterInstance.deleteData(it)  - u kombinaciji s donjom naredbom crasha aplikaciju s java.util.ConcurrentModificationException greškom pa se odabralo rješenje pokazano u handleDeletionOkResponse metodi
                            interactor.delete(DeleteTaskRequest(it.id), deleteCallback())
                        }
                    }
                    .setNegativeButton("Cancel") { _, _ ->
                    }
                    .create()
                    .show()

            }
            .setNegativeButton("Cancel") { _, _ ->
            }
            .create()
            .show()
    }

    private fun deleteCallback(): Callback<DeleteResponse> = object : Callback<DeleteResponse> {
        override fun onFailure(call: Call<DeleteResponse>?, t: Throwable?) {
            Log.d(TasksFragment.REQUEST_ERROR, t?.localizedMessage)
            Log.d(TasksFragment.REQUEST_ERROR, call?.request().toString())
        }

        override fun onResponse(call: Call<DeleteResponse>?, response: Response<DeleteResponse>) {
            if (response.isSuccessful) {
                when (response.code()) {
                    RESPONSE_OK -> handleDeletionOkResponse()
                    RESPONSE_BAD_REQUEST -> showBadRequestToast()
                    RESPONSE_NOT_FOUND -> showNotFoundToast()
                    SERVER_ERROR -> showServerErrorToast()
                    else -> handleSomethingWentWrongWithDeletion()
                }
            }
        }
    }

    private fun handleDeletionOkResponse() {
        val recyclerViewAdapterInstance = (TaskAdapter.recyclerViewInstance.adapter as TaskAdapter)
        recyclerViewAdapterInstance.refreshData()
        Taskie.getApplicationContext().displayToast("Task was successfully deleted.")
    }

    private fun handleSomethingWentWrongWithDeletion() =
        Taskie.getApplicationContext().displayToast("Something went wrong.")

    private fun sortTasksByPriority() {
        (TaskAdapter.recyclerViewInstance.adapter as TaskAdapter).sortTasks()
    }
}