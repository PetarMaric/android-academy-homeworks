package hr.ferit.petarmaric.taskie.persistence.preferences

import hr.ferit.petarmaric.taskie.Taskie

const val KEY_USER_TOKEN = "user_token"
const val PREFERENCES_NAME = "taskie_prefs"

class SharedPrefsHelperImplementation : SharedPrefsHelper {

    private val preferences = Taskie.getApplicationContext().providePreferences()

    // API authentication token
    override fun getUserToken(): String = preferences.getString(KEY_USER_TOKEN, "")

    override fun storeUserToken(token: String) = preferences.edit().putString(KEY_USER_TOKEN, token).apply()

    override fun clearUserToken() = preferences.edit().remove(KEY_USER_TOKEN).apply()
}

fun provideSharedPrefs(): SharedPrefsHelper {
    return SharedPrefsHelperImplementation()
}