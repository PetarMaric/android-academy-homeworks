package hr.ferit.petarmaric.taskie.model.response

data class DeleteResponse(val message: String)