package hr.ferit.petarmaric.taskie.persistence.preferences

import android.content.Context
import android.preference.PreferenceManager
import hr.ferit.petarmaric.taskie.BuildConfig
import hr.ferit.petarmaric.taskie.Taskie

object TaskPrefs {
    private const val sharedPreferencesFile = BuildConfig.APPLICATION_ID

    private fun sharedPrefs() =
        Taskie.getApplicationContext().getSharedPreferences(sharedPreferencesFile, Context.MODE_PRIVATE)

    fun storePrefs(key: String, value: String) = sharedPrefs().edit().putString(key, value).apply()

    fun getPrefsString(key: String, defaultValue: String) = sharedPrefs().getString(key, defaultValue)
}