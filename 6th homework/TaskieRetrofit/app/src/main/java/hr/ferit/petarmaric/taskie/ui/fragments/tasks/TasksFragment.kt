package hr.ferit.petarmaric.taskie.ui.fragments.tasks

import android.app.AlertDialog
import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import hr.ferit.petarmaric.taskie.R
import hr.ferit.petarmaric.taskie.Taskie
import hr.ferit.petarmaric.taskie.common.*
import hr.ferit.petarmaric.taskie.model.BackendTask
import hr.ferit.petarmaric.taskie.model.request.AddTaskRequest
import hr.ferit.petarmaric.taskie.model.request.DeleteTaskRequest
import hr.ferit.petarmaric.taskie.model.response.DeleteResponse
import hr.ferit.petarmaric.taskie.model.response.GetTasksResponse
import hr.ferit.petarmaric.taskie.networking.connectionmanager.ConnectionManager
import hr.ferit.petarmaric.taskie.networking.interactors.BackendFactory
import hr.ferit.petarmaric.taskie.networking.showBadRequestToast
import hr.ferit.petarmaric.taskie.networking.showNotFoundToast
import hr.ferit.petarmaric.taskie.networking.showServerErrorToast
import hr.ferit.petarmaric.taskie.persistence.database.TasksRepository
import hr.ferit.petarmaric.taskie.persistence.database.TasksRoomRepository
import hr.ferit.petarmaric.taskie.ui.adapters.recyclerview.TaskAdapter
import hr.ferit.petarmaric.taskie.ui.fragments.base.BaseFragment
import kotlinx.android.synthetic.main.fragment_tasks.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class TasksFragment : BaseFragment(),
    AddTaskFragmentDialog.TaskAddedListener {

    private val repository: TasksRepository = TasksRoomRepository()

    private val interactor = BackendFactory.getTaskieInteractor()

    private val adapter by lazy { TaskAdapter { onItemSelected(it) } }
    private lateinit var contextInstance: Context


    companion object {
        fun newInstance(): Fragment {
            return TasksFragment()
        }

        const val TASKS_TO_SAVE = "TASKS_TO_SAVE"
        const val REQUEST_ERROR = "REQUEST_ERROR"
    }


    override fun getLayoutResourceId() = R.layout.fragment_tasks

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initUi()
        initListeners()
    }

    override fun onAttach(context: Context) {
        contextInstance = context
        super.onAttach(context)
    }

    private fun initUi() {
        progress?.visible()
        noData?.visible()


        tasksRecyclerView.layoutManager = LinearLayoutManager(context)
        tasksRecyclerView.adapter = adapter

        setRecyclerViewItemTouchListener()

        val connected = ConnectionManager.getNetworkInfo()?.isConnected ?: false

        if (connected) {

            storePendingTasks()

            getAllTasks()
        } else {
            activity?.displayToast("No internet connection. Showing last stored server response.")
            val tasksFromDatabase = repository.getAllTasks()

            val tasksBackendTask = taskToBackendTaskMapper(tasksFromDatabase)
            adapter.setData(tasksBackendTask)
        }


    }

    private fun storePendingTasks() {
        val pendingTasks = repository.getAllPendingTasks()
        if (pendingTasks.size == 0) {
            return
        }

        Log.d(TASKS_TO_SAVE, pendingTasks.toString())

        pendingTasks.forEach {
            interactor.save(
                AddTaskRequest(
                    title = it.title,
                    content = it.content,
                    taskPriority = it.priority
                ), pendingTaskCallback()
            )
        }

        repository.deleteAllPendingTasks()
    }

    private fun pendingTaskCallback(): Callback<BackendTask> = object : Callback<BackendTask> {
        override fun onFailure(call: Call<BackendTask>, t: Throwable) {
            progress?.gone()

            Log.d(REQUEST_ERROR, t.localizedMessage)
            Log.d(REQUEST_ERROR, call.request().toString())

        }

        override fun onResponse(call: Call<BackendTask>, response: Response<BackendTask>) {
            progress?.gone()
            noData?.gone()
            if (response.isSuccessful) {
                when (response.code()) {
                    RESPONSE_OK -> Log.d("TASKS_TO_SAVE", "Saved task: {${response.body()}}\n\r")
                    RESPONSE_BAD_REQUEST -> showBadRequestToast()
                    RESPONSE_NOT_FOUND -> showNotFoundToast()
                    SERVER_ERROR -> showServerErrorToast()
                    else -> handleSomethingWentWrong()
                }
            }
        }

    }

    private fun getAllTasks() {
        progress?.visible()
        interactor.getTasks(getTaskieCallback())
    }

    private fun getTaskieCallback(): Callback<GetTasksResponse> = object : Callback<GetTasksResponse> {
        override fun onFailure(call: Call<GetTasksResponse>?, t: Throwable?) {
            progress?.gone()

            Log.d(REQUEST_ERROR, t?.localizedMessage)
            Log.d(REQUEST_ERROR, call?.request().toString())
        }

        override fun onResponse(call: Call<GetTasksResponse>?, response: Response<GetTasksResponse>) {
            progress?.gone()
            noData?.gone()
            if (response.isSuccessful) {
                when (response.code()) {
                    RESPONSE_OK -> handleOkResponse(response)
                    RESPONSE_BAD_REQUEST -> showBadRequestToast()
                    RESPONSE_NOT_FOUND -> showNotFoundToast()
                    SERVER_ERROR -> showServerErrorToast()
                    else -> handleSomethingWentWrong()
                }
            }
        }
    }

    private fun onTaskiesReceived(taskies: MutableList<BackendTask>) = adapter.setData(taskies)

    private fun handleOkResponse(response: Response<GetTasksResponse>) {
        response.body()?.notes?.run {
            checkList(this)
            onTaskiesReceived(this)
            swipeRefreshLayout.isRefreshing = false

            repository.deleteAllTasks()


            val backendTasks = backendTaskToTaskMapper(this)
            repository.storeAllTasks(backendTasks)

        }
    }

    private fun checkList(notes: MutableList<BackendTask>) {
        if (notes.isEmpty()) {
            noData?.visible()
        } else {
            noData?.gone()
        }
    }


    private fun handleSomethingWentWrong() {
        swipeRefreshLayout.isRefreshing = false
        this.activity?.displayToast("Something went wrong!")
    }

    private fun setRecyclerViewItemTouchListener() {
        val itemTouchCallback = object : ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT) {
            override fun onMove(
                recyclerView: RecyclerView,
                viewHolder: RecyclerView.ViewHolder,
                target: RecyclerView.ViewHolder
            ): Boolean {
                return false
            }

            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
                val position = viewHolder.adapterPosition
                val task = adapter.getTaskAtPosition(position)

                showDeleteConfirmationDialog(task)

            }

        }

        val itemTouchHelper = ItemTouchHelper(itemTouchCallback)
        itemTouchHelper.attachToRecyclerView(tasksRecyclerView)

    }

    private fun showDeleteConfirmationDialog(task: BackendTask) {
        AlertDialog.Builder(context, R.style.AppCompatAlertDialogStyle)
            .setTitle("Do you want to delete task ${task.title} with ${task.taskPriority} priority?")
            .setPositiveButton("Delete") { _, _ ->

                val deleteRequest = DeleteTaskRequest(task.id)
                interactor.delete(deleteRequest, deleteCallback())

                adapter.deleteData(task)
            }
            .setNegativeButton("Cancel") { _, _ ->
                adapter.notifyDataSetChanged()
            }
            .create()
            .show()
    }

    private fun deleteCallback(): Callback<DeleteResponse> = object : Callback<DeleteResponse> {
        override fun onFailure(call: Call<DeleteResponse>?, t: Throwable?) {
            Log.d(REQUEST_ERROR, t?.localizedMessage)
            Log.d(REQUEST_ERROR, call?.request().toString())
        }

        override fun onResponse(call: Call<DeleteResponse>?, response: Response<DeleteResponse>) {
            if (response.isSuccessful) {
                when (response.code()) {
                    RESPONSE_OK -> handleDeletionOkResponse()
                    RESPONSE_BAD_REQUEST -> showBadRequestToast()
                    RESPONSE_NOT_FOUND -> showNotFoundToast()
                    SERVER_ERROR -> showServerErrorToast()
                    else -> handleSomethingWentWrongWithDeletion()
                }
            }
        }
    }

    private fun handleDeletionOkResponse() {
        Taskie.getApplicationContext().displayToast("Task was successfully deleted.")
    }

    private fun handleSomethingWentWrongWithDeletion() =
        Taskie.getApplicationContext().displayToast("Something went wrong.")


    private fun initListeners() {
        addTask.setOnClickListener { addTask() }

        swipeRefreshLayout.setOnRefreshListener { refreshTasks() }
    }

    private fun onItemSelected(task: BackendTask) {

        activity?.showFragment(R.id.fragmentContainer, TaskDetailsFragment.newInstance(task.id), shouldAddToBackStack = true)

    }

    private fun refreshTasks() {
        progress?.gone()
        interactor.getTasks(getTaskieCallback())
    }

    private fun addTask() {
        val dialog = AddTaskFragmentDialog.newInstance()
        dialog.setTaskAddedListener(this)
        dialog.show(childFragmentManager, dialog.tag)
    }

    override fun onTaskAdded(task: BackendTask) {
        refreshTasks()
    }

}