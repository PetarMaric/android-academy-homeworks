package hr.ferit.petarmaric.taskie.model.response

import hr.ferit.petarmaric.taskie.model.BackendTask

data class GetTasksResponse(val notes: MutableList<BackendTask>? = mutableListOf())