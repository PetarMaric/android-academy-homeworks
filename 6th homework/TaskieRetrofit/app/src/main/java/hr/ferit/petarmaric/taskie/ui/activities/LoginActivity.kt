package hr.ferit.petarmaric.taskie.ui.activities

import android.content.Intent
import android.util.Log
import hr.ferit.petarmaric.taskie.R
import hr.ferit.petarmaric.taskie.common.*
import hr.ferit.petarmaric.taskie.model.request.UserDataRequest
import hr.ferit.petarmaric.taskie.model.response.LoginResponse
import hr.ferit.petarmaric.taskie.networking.interactors.BackendFactory
import hr.ferit.petarmaric.taskie.networking.showBadRequestToast
import hr.ferit.petarmaric.taskie.networking.showNotFoundToast
import hr.ferit.petarmaric.taskie.networking.showServerErrorToast
import hr.ferit.petarmaric.taskie.persistence.preferences.provideSharedPrefs
import hr.ferit.petarmaric.taskie.ui.activities.base.BaseActivity
import hr.ferit.petarmaric.taskie.ui.fragments.tasks.TasksFragment
import kotlinx.android.synthetic.main.activity_login.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class LoginActivity : BaseActivity() {

    private val interactor = BackendFactory.getTaskieInteractor()
    private val prefs = provideSharedPrefs()

    override fun getLayoutResourceId(): Int = R.layout.activity_login

    override fun setUpUi() {
        login.onClick { signInClicked() }
        goToRegister.onClick { goToRegistrationClicked() }
    }

    private fun signInClicked() {
        interactor.login(
            request = UserDataRequest(password = password.text.toString(), email = email.text.toString()),
            loginCallback = loginCallback()
        )
    }

    private fun loginCallback(): Callback<LoginResponse> = object : Callback<LoginResponse> {
        override fun onFailure(call: Call<LoginResponse>?, t: Throwable?) {
            Log.d(TasksFragment.REQUEST_ERROR, t?.localizedMessage)
            Log.d(TasksFragment.REQUEST_ERROR, call?.request().toString())
        }

        override fun onResponse(call: Call<LoginResponse>?, response: Response<LoginResponse>) {
            if (response.isSuccessful) {
                when (response.code()) {
                    RESPONSE_OK -> handleOkResponse(response.body())
                    RESPONSE_BAD_REQUEST -> showBadRequestToast()
                    RESPONSE_NOT_FOUND -> showNotFoundToast()
                    SERVER_ERROR -> showServerErrorToast()
                    else -> handleSomethingWentWrong()
                }
            }
        }
    }

    private fun goToRegistrationClicked() {
        val intent = Intent(this, RegisterActivity::class.java)
        startActivity(intent)
        finish()
    }

    private fun handleOkResponse(loginReponse: LoginResponse?) {
        this.displayToast("Successfully logged in!")
        loginReponse?.token?.let { prefs.storeUserToken(it) }
        val intent = Intent(this, MainActivity::class.java)
        startActivity(intent)
        finish()
    }

    private fun handleSomethingWentWrong() = this.displayToast("Something went wrong!")
}