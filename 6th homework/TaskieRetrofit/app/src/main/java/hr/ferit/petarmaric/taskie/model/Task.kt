package hr.ferit.petarmaric.taskie.model

import androidx.room.Entity
import androidx.room.PrimaryKey



@Entity(tableName = "tasks")
data class Task(
    @PrimaryKey(autoGenerate = true) var id: Int = 0,
    val retrofitTaskId: String,
    val title: String,
    val content: String,
    val isFavorite: Boolean,
    val taskPriority: Int,
    val isCompleted: Boolean
)