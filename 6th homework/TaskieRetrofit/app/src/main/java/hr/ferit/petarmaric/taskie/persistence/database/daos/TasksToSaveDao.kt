package hr.ferit.petarmaric.taskie.persistence.database.daos

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import hr.ferit.petarmaric.taskie.model.TaskToSave

@Dao
interface TasksToSaveDao {
    @Query("SELECT * FROM pending_tasks")
    fun getAllTasks(): MutableList<TaskToSave>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun storeTask(task: TaskToSave)

    @Query("DELETE from pending_tasks")
    fun deleteAllPendingTasks()
}