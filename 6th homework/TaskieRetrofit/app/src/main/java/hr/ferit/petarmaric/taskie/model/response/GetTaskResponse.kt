package hr.ferit.petarmaric.taskie.model.response

import hr.ferit.petarmaric.taskie.model.BackendTask

data class GetTaskResponse(val task: BackendTask = BackendTask(content = "-1", id = "-1", userId = "-1", title = "-1", isFavorite = false, isCompleted = false, taskPriority = 0))