package hr.ferit.petarmaric.taskie.ui.fragments.tasks

import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.TextView
import hr.ferit.petarmaric.taskie.R
import hr.ferit.petarmaric.taskie.Taskie
import hr.ferit.petarmaric.taskie.common.*
import hr.ferit.petarmaric.taskie.model.BackendTask
import hr.ferit.petarmaric.taskie.model.PriorityColor
import hr.ferit.petarmaric.taskie.model.Task
import hr.ferit.petarmaric.taskie.model.request.TaskRequest
import hr.ferit.petarmaric.taskie.model.request.UpdateTaskRequest
import hr.ferit.petarmaric.taskie.model.response.GetTaskResponse
import hr.ferit.petarmaric.taskie.networking.interactors.BackendFactory
import hr.ferit.petarmaric.taskie.networking.showBadRequestToast
import hr.ferit.petarmaric.taskie.networking.showNotFoundToast
import hr.ferit.petarmaric.taskie.networking.showServerErrorToast
import hr.ferit.petarmaric.taskie.persistence.database.TasksRepository
import hr.ferit.petarmaric.taskie.persistence.database.TasksRoomRepository
import hr.ferit.petarmaric.taskie.ui.adapters.spinneradapter.PriorityAdapter
import hr.ferit.petarmaric.taskie.ui.fragments.base.BaseFragment
import kotlinx.android.synthetic.main.fragment_task_details.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class TaskDetailsFragment : BaseFragment() {
    private val repository: TasksRepository = TasksRoomRepository()

    private val interactor = BackendFactory.getTaskieInteractor()


    private var taskID = NO_TASK_STRING

    private lateinit var originalTask: BackendTask

    override fun getLayoutResourceId(): Int {
        return R.layout.fragment_task_details
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        arguments?.getString(EXTRA_TASK_ID)?.let { taskID = it }
        tryDisplayTask(taskID)
    }

    private fun tryDisplayTask(id: String) {
        try {
            interactor.getTask(TaskRequest(id), getTaskCallback())
        } catch (e: NoSuchElementException) {
            context?.displayToast(getString(R.string.noTaskFound))
        }
    }

    private fun getTaskCallback(): Callback<BackendTask> = object : Callback<BackendTask> {
        override fun onFailure(call: Call<BackendTask>, t: Throwable) {
            Log.d(TasksFragment.REQUEST_ERROR, t?.localizedMessage)
            Log.d(TasksFragment.REQUEST_ERROR, call?.request().toString())
        }

        override fun onResponse(call: Call<BackendTask>, response: Response<BackendTask>) {

            if (response.isSuccessful) {
                when (response.code()) {
                    RESPONSE_OK -> handleOkResponse(response?.body())
                    RESPONSE_BAD_REQUEST -> showBadRequestToast()
                    RESPONSE_NOT_FOUND -> showNotFoundToast()
                    SERVER_ERROR -> showServerErrorToast()
                    else -> handleSomethingWentWrong()
                }
            }
        }

    }

    private fun handleSomethingWentWrong() = Taskie.getApplicationContext().displayToast("Something went wrong.")

    private fun handleOkResponse(task: BackendTask?) {
        originalTask = task ?: BackendTask(
            content = "-1",
            id = "-1",
            userId = "-1",
            title = "-1",
            isFavorite = false,
            isCompleted = false,
            taskPriority = 0
        )
        task?.run { displayTask(this) }
    }

    private fun displayTask(task: BackendTask) {

        detailsTaskTitle.setText(task.title, TextView.BufferType.EDITABLE)
        detailsTaskDescription.setText(task.content, TextView.BufferType.EDITABLE)

        spinnerPriorities.adapter = PriorityAdapter(
            Taskie.getApplicationContext(),
            R.layout.item_priority_spinner_item,
            PriorityColor.values().toList()
        )

        spinnerPriorities.setSelection(
            when (task.taskPriority) {
                1 -> {
                    0
                }
                2 -> {
                    1
                }
                else -> {
                    2
                }
            }
        )

        submitTask.setOnClickListener { updateTask() }
    }


    private fun updateTask() {
        if (isInputEmpty()) {
            context?.displayToast(getString(R.string.emptyFields))
            return
        }

        val title = detailsTaskTitle.text.toString()
        val description = detailsTaskDescription.text.toString()
        val priorityBackendPriorityTask = spinnerPriorities.priorityFactory()

        interactor.update(
            UpdateTaskRequest(
                id = originalTask.id,
                title = title,
                content = description,
                taskPriority = priorityBackendPriorityTask.getValue()
            ), updateCallback = updateTaskCallback()
        )
        activity?.showFragment(R.id.fragmentContainer, TasksFragment.newInstance())
    }

    private fun updateTaskCallback(): Callback<BackendTask> = object : Callback<BackendTask> {
        override fun onFailure(call: Call<BackendTask>, t: Throwable) {
            Log.d(TasksFragment.REQUEST_ERROR, t?.localizedMessage)
            Log.d(TasksFragment.REQUEST_ERROR, call?.request().toString())
        }

        override fun onResponse(call: Call<BackendTask>, response: Response<BackendTask>) {

            if (response.isSuccessful) {
                when (response.code()) {
                    RESPONSE_OK -> handleUpdateOkResponse(response?.body())
                    RESPONSE_BAD_REQUEST -> showBadRequestToast()
                    RESPONSE_NOT_FOUND -> showNotFoundToast()
                    SERVER_ERROR -> showServerErrorToast()
                    else -> handleSomethingWentWrong()
                }
            }
        }

    }

    private fun handleUpdateOkResponse(task: BackendTask?) {
        context?.displayToast("Updated task.")
    }

    private fun isInputEmpty(): Boolean = TextUtils.isEmpty(detailsTaskTitle.text) || TextUtils.isEmpty(
        detailsTaskDescription.text
    )

    companion object {
        const val NO_TASK = -1
        const val NO_TASK_STRING = "No text"

        fun newInstance(taskId: String): TaskDetailsFragment {
            val bundle = Bundle().apply { putString(EXTRA_TASK_ID, taskId) }
            return TaskDetailsFragment().apply { arguments = bundle }
        }
    }
}
