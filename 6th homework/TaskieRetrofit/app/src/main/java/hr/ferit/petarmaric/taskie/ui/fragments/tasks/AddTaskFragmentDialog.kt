package hr.ferit.petarmaric.taskie.ui.fragments.tasks

import android.os.Bundle
import android.text.TextUtils.isEmpty
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.ArrayAdapter
import androidx.fragment.app.DialogFragment
import hr.ferit.petarmaric.taskie.R
import hr.ferit.petarmaric.taskie.Taskie
import hr.ferit.petarmaric.taskie.common.*
import hr.ferit.petarmaric.taskie.model.BackendTask
import hr.ferit.petarmaric.taskie.model.PriorityColor
import hr.ferit.petarmaric.taskie.model.Task
import hr.ferit.petarmaric.taskie.model.TaskToSave
import hr.ferit.petarmaric.taskie.model.request.AddTaskRequest
import hr.ferit.petarmaric.taskie.networking.connectionmanager.ConnectionManager
import hr.ferit.petarmaric.taskie.networking.interactors.BackendFactory
import hr.ferit.petarmaric.taskie.networking.showBadRequestToast
import hr.ferit.petarmaric.taskie.networking.showNotFoundToast
import hr.ferit.petarmaric.taskie.networking.showServerErrorToast
import hr.ferit.petarmaric.taskie.persistence.database.TasksRepository
import hr.ferit.petarmaric.taskie.persistence.database.TasksRoomRepository
import hr.ferit.petarmaric.taskie.persistence.preferences.TaskPrefs
import kotlinx.android.synthetic.main.fragment_dialog_new_task.*
import kotlinx.android.synthetic.main.item_task.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.sql.Connection

class AddTaskFragmentDialog : DialogFragment() {

    private var taskAddedListener: TaskAddedListener? = null

    private val interactor = BackendFactory.getTaskieInteractor()
    private val repository: TasksRepository = TasksRoomRepository()

    companion object {
        fun newInstance(): AddTaskFragmentDialog {
            return AddTaskFragmentDialog()
        }

        val DEFAULT_PRIORITY = PriorityColor.values().first().toString()
        const val STORED_PRIORITY = "STORED_PRIORITY"
        const val TASK_TO_SAVE = "TASK_TO_SAVE"
    }

    interface TaskAddedListener {
        fun onTaskAdded(task: BackendTask)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NO_TITLE, R.style.FragmentDialogTheme)

    }

    fun setTaskAddedListener(listener: TaskAddedListener) {
        taskAddedListener = listener
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_dialog_new_task, container)
    }

    override fun onStart() {
        super.onStart()
        dialog?.window?.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initUi()
        initListeners()
    }

    private fun initUi() {


        context?.let {
            prioritySelector.adapter =
                ArrayAdapter<PriorityColor>(it, android.R.layout.simple_spinner_dropdown_item, PriorityColor.values())

            val storedPriority = TaskPrefs.getPrefsString(STORED_PRIORITY, DEFAULT_PRIORITY)

            prioritySelector.setSelection(determinePriorityLocation(storedPriority))


        }
    }

    private fun initListeners() {
        saveTaskAction.setOnClickListener { saveTask() }
    }

    private fun saveTask() {
        if (isInputEmpty()) {
            context?.displayToast(getString(R.string.emptyFields))
            return
        }

        val title = newTaskTitleInput.text.toString()
        val description = newTaskDescriptionInput.text.toString()

        val priority = prioritySelector.selectedItem as PriorityColor


        val priorityBackendPriorityTask = prioritySelector.priorityFactory()


        val connected = ConnectionManager.getNetworkInfo()?.isConnected ?: false

        if (connected) {
            interactor.save(
                AddTaskRequest(title, description, priorityBackendPriorityTask.getValue()),
                addTaskCallback()
            )
        }
        else {
            Taskie.getApplicationContext().displayToast(getString(R.string.locallySavedTaskText))
            val taskToSave = TaskToSave(title = title, content = description, priority = priority.getPriorityOrder())
            Log.d(TASK_TO_SAVE, taskToSave.toString())
            repository.storePendingTask(taskToSave)
        }



        storePriority(priority)

        clearUi()

        dismiss()
    }

    private fun addTaskCallback(): Callback<BackendTask> = object : Callback<BackendTask> {
        override fun onFailure(call: Call<BackendTask>?, t: Throwable?) {
            Log.d(TasksFragment.REQUEST_ERROR, t?.localizedMessage)
            Log.d(TasksFragment.REQUEST_ERROR, call?.request().toString())
        }

        override fun onResponse(call: Call<BackendTask>?, response: Response<BackendTask>) {
            if (response.isSuccessful) {
                when (response.code()) {
                    RESPONSE_OK -> handleOkResponse(response.body())
                    RESPONSE_BAD_REQUEST -> showBadRequestToast()
                    RESPONSE_NOT_FOUND -> showNotFoundToast()
                    SERVER_ERROR -> showServerErrorToast()
                    else -> handleSomethingWentWrong()
                }
            }
        }
    }

    private fun storePriority(priority: PriorityColor) {
        TaskPrefs.storePrefs(STORED_PRIORITY, priority.toString())
    }

    private fun clearUi() {
        newTaskTitleInput.text.clear()
        newTaskDescriptionInput.text.clear()
        prioritySelector.setSelection(0)
    }

    private fun isInputEmpty(): Boolean = isEmpty(newTaskTitleInput.text) || isEmpty(newTaskDescriptionInput.text)

    private fun handleOkResponse(task: BackendTask?) = task?.run { onTaskiesReceived(this) }

    private fun handleSomethingWentWrong() = this.activity?.displayToast("Something went wrong!")

    private fun onTaskiesReceived(task: BackendTask) {
        taskAddedListener?.onTaskAdded(task)
        dismiss()
    }


}