package hr.ferit.petarmaric.taskie.ui.fragments.about

import android.os.Bundle
import android.view.View
import hr.ferit.petarmaric.taskie.R
import hr.ferit.petarmaric.taskie.ui.adapters.fragment.AboutFragmentAdapter
import hr.ferit.petarmaric.taskie.ui.fragments.base.BaseFragment
import kotlinx.android.synthetic.main.fragment_about.*


class About : BaseFragment() {

    companion object {
        fun newInstance(): About = About()
    }

    override fun getLayoutResourceId(): Int = R.layout.fragment_about

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewPager.adapter = AboutFragmentAdapter(childFragmentManager)
        tabLayout.setupWithViewPager(viewPager)
    }

}