package hr.ferit.petarmaric.taskie.networking

import hr.ferit.petarmaric.taskie.model.BackendTask
import hr.ferit.petarmaric.taskie.model.request.AddTaskRequest
import hr.ferit.petarmaric.taskie.model.request.UpdateTaskRequest
import hr.ferit.petarmaric.taskie.model.request.UserDataRequest
import hr.ferit.petarmaric.taskie.model.response.*
import retrofit2.Call
import retrofit2.http.*

interface TaskieApiService {

    @POST("/api/register")
    fun register(@Body userData: UserDataRequest): Call<RegisterResponse>

    @POST("/api/login")
    fun login(@Body userData: UserDataRequest): Call<LoginResponse>

    @GET("/api/note")
    fun getTasks(): Call<GetTasksResponse>

    @GET("/api/note/{noteId}")
    fun getTask(@Path("noteId") taskNoteId: String): Call<BackendTask>

    @POST("/api/note/edit")
    fun updateTask(@Body task: UpdateTaskRequest): Call<BackendTask>

    @POST("/api/note")
    fun save(@Body taskData: AddTaskRequest): Call<BackendTask>

    @POST("/api/note/delete")
    fun deleteTask(@Query("id") taskNoteId: String): Call<DeleteResponse>
}