package hr.ferit.petarmaric.taskie.model.response

data class LoginResponse(val token: String? = "")