package hr.ferit.petarmaric.taskie.persistence.database

import androidx.room.TypeConverter
import hr.ferit.petarmaric.taskie.model.PriorityColor

class RoomTypeConverters {
    companion object {

        @TypeConverter
        @JvmStatic
        fun fromPriority(priority: PriorityColor): String {
            return priority.name
        }

        @TypeConverter
        @JvmStatic
        fun toPriority(priority: String): PriorityColor {
            return PriorityColor.values().first { it.name == priority }
        }
    }
}