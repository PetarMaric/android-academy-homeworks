package hr.ferit.petarmaric.taskie

import android.app.Application
import android.content.Context
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatDelegate
import hr.ferit.petarmaric.taskie.persistence.preferences.PREFERENCES_NAME

class Taskie : Application() {

    companion object {
        private lateinit var instance: Taskie


        fun getApplicationContext() = instance
    }

    override fun onCreate() {
        super.onCreate()
        instance = this
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true)
    }

    fun providePreferences(): SharedPreferences = instance.getSharedPreferences(PREFERENCES_NAME, Context.MODE_PRIVATE)
}