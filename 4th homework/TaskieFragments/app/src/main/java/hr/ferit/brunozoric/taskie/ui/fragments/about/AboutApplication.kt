package hr.ferit.brunozoric.taskie.ui.fragments.about

import hr.ferit.brunozoric.taskie.R
import hr.ferit.brunozoric.taskie.ui.fragments.base.BaseFragment

class AboutApplication : BaseFragment() {
    override fun getLayoutResourceId(): Int = R.layout.fragment_about_application

    companion object {
        fun getInstance(): AboutApplication = AboutApplication()
    }

}