package hr.ferit.brunozoric.taskie.ui.fragments.about

import hr.ferit.brunozoric.taskie.R
import hr.ferit.brunozoric.taskie.ui.fragments.base.BaseFragment

class AboutAuthor : BaseFragment() {
    override fun getLayoutResourceId(): Int = R.layout.fragment_about_author

    companion object {
        fun getInstance(): AboutAuthor = AboutAuthor()
    }
}