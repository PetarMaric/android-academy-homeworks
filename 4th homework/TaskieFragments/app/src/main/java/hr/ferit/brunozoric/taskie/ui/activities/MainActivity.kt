package hr.ferit.brunozoric.taskie.ui.activities

import hr.ferit.brunozoric.taskie.R
import hr.ferit.brunozoric.taskie.ui.activities.base.BaseActivity
import hr.ferit.brunozoric.taskie.ui.fragments.tasks.TasksFragment
import hr.ferit.brunozoric.taskie.ui.listener.NavigationMenuListener
import kotlinx.android.synthetic.main.navigation_bottom_navigation.*

class MainActivity : BaseActivity() {

    override fun getLayoutResourceId() = R.layout.activity_main

    override fun setUpUi() {

        showFragment(TasksFragment.newInstance())

        bottomNavigationMaterial.setOnNavigationItemSelectedListener(NavigationMenuListener(this))

    }

}