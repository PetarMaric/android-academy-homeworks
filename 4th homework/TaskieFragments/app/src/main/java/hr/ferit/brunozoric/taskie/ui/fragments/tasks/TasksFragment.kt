package hr.ferit.brunozoric.taskie.ui.fragments.tasks

import android.content.Context
import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import hr.ferit.brunozoric.taskie.R
import hr.ferit.brunozoric.taskie.common.*
import hr.ferit.brunozoric.taskie.model.Task
import hr.ferit.brunozoric.taskie.persistence.Repository
import hr.ferit.brunozoric.taskie.ui.adapters.TaskAdapter
import hr.ferit.brunozoric.taskie.ui.fragments.base.BaseFragment
import kotlinx.android.synthetic.main.fragment_tasks.*

class TasksFragment : BaseFragment(),
    AddTaskFragmentDialog.TaskAddedListener {

    private val repository = Repository
    private val adapter by lazy { TaskAdapter { onItemSelected(it) } }
    private lateinit var contextInstance: Context

    override fun getLayoutResourceId() = R.layout.fragment_tasks

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initUi()
        initListeners()
        refreshTasks()
    }

    override fun onAttach(context: Context) {
        contextInstance = context
        super.onAttach(context)
    }

    private fun initUi() {
        progress.visible()
        noData.visible()
        tasksRecyclerView.layoutManager = LinearLayoutManager(context)
        tasksRecyclerView.adapter = adapter
    }

    private fun initListeners() {
        addTask.setOnClickListener { addTask() }
    }

    private fun onItemSelected(task: Task) {

        /**
         *      - Pretpostavilo se da je container za task details onaj container koji se nalazi unutar main activitya.
         *      - Uklonilo se otvaranje detalja o zadacima pomocu intenta kako bi se omogucila glatka navigacija pomocu bottom navigation viewa - u protivnom aplikacija mijenja activity (iskoci preko trenutnog activitya) i nemoguce je na lijep nacin rabiti bottom navigation view
         *      - Pokusalo se napraviti da i activity za detalje ima bottom navigation layout gdje bi se klikom na odredeni item intentom vracalo nazad na fragment listu taskova ili fragment za informacije o aplikaciji i autoru -> prilikom upita predavaca Zorica bi li to bilo adekvatno rjesenje, dobio sam odgovor da nije, te sam napravio zadacu na ovakav nacin
         */

        activity?.showFragment(R.id.fragmentContainer, TaskDetailsFragment.newInstance(task.id))

    }

    private fun refreshTasks() {
        progress.gone()
        val data = repository.getAllTasks()
        if (data.isNotEmpty()) {
            noData.gone()
        } else {
            noData.visible()
        }
        adapter.setData(data)
    }

    private fun addTask() {
        val dialog = AddTaskFragmentDialog.newInstance()
        dialog.setTaskAddedListener(this)
        dialog.show(childFragmentManager, dialog.tag)
    }

    override fun onTaskAdded(task: Task) {
        refreshTasks()
    }

    companion object {
        fun newInstance(): Fragment {
            return TasksFragment()
        }
    }
}