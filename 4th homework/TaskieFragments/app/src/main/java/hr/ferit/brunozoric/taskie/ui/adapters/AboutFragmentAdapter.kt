package hr.ferit.brunozoric.taskie.ui.adapters

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import hr.ferit.brunozoric.taskie.ui.fragments.about.AboutApplication
import hr.ferit.brunozoric.taskie.ui.fragments.about.AboutAuthor

class AboutFragmentAdapter(fragmentManager: FragmentManager) : FragmentPagerAdapter(fragmentManager) {

    private val aboutFragments: List<Fragment> =
        listOf(AboutApplication.getInstance(), AboutAuthor.getInstance())

    private val titles = arrayOf("About application", "About author")

    override fun getItem(position: Int): Fragment {
        return aboutFragments[position]
    }

    override fun getCount(): Int = aboutFragments.size

    override fun getPageTitle(position: Int): CharSequence? = titles[position]


}