package hr.ferit.petarmaric.shortbiography.ui.Listener

import android.provider.DocumentsContract
import android.view.View
import hr.ferit.petarmaric.shortbiography.R
import hr.ferit.petarmaric.shortbiography.ui.rootAppActivity.RootAppActivity
import hr.ferit.petarmaric.shortbiography.util.showToast

object ClickListener : View.OnClickListener, View.OnLongClickListener {

    override fun onClick(v: View?) {
        RootAppActivity.ApplicationContext.showToast(RootAppActivity.ApplicationContext.getString(R.string.clickListenerText))
    }

    override fun onLongClick(v: View?): Boolean {
        RootAppActivity.ApplicationContext.showToast(RootAppActivity.ApplicationContext.getString(R.string.longClickListenerText))
        return true
    }

}