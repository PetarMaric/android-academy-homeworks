package hr.ferit.petarmaric.shortbiography.ui.common

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity

abstract class BaseActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(getViewResourceId())

        setUpUi()
    }

    abstract fun getViewResourceId(): Int

    abstract fun setUpUi()
}