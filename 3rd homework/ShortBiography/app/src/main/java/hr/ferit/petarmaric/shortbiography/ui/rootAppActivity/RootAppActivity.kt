package hr.ferit.petarmaric.shortbiography.ui.rootAppActivity

import android.app.Application
import android.content.Context

class RootAppActivity : Application() {

    companion object {
        lateinit var ApplicationContext: Context
            private set
    }

    override fun onCreate() {
        super.onCreate()
        ApplicationContext = this
    }
}