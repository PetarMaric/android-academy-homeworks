package hr.ferit.petarmaric.shortbiography.ui.biographyActivity

import hr.ferit.petarmaric.shortbiography.R
import hr.ferit.petarmaric.shortbiography.ui.Listener.ClickListener
import hr.ferit.petarmaric.shortbiography.ui.common.BaseActivity
import kotlinx.android.synthetic.main.activity_biography.*

class BiographyActivity : BaseActivity() {
    override fun getViewResourceId(): Int = R.layout.activity_biography

    override fun setUpUi() {
        profilePicture.setOnClickListener(ClickListener)
        profilePicture.setOnLongClickListener(ClickListener)
    }

}
