package hr.osc.ada.taskmanagement.persistence

import hr.osc.ada.taskmanagement.model.Task

object Repository {

    private val tasks = mutableListOf<Task>()
    private var currentId = 0

    fun saveTask(task: Task): Int {
        val newTask = Task(id = currentId, title = task.title, description = task.description, priority = task.priority)

        tasks.add(newTask)

        currentId++

        return newTask.id
    }

    fun getTaskById(taskId: Int): Task = tasks.get(taskId)

    fun getAllTasks(): MutableList<Task> = tasks

    fun deleteTask(task: Task) {
        tasks.remove(task)
    }
}