package hr.osc.ada.taskmanagement.model

data class Task(
    val id: Int = 0,
    val description: String,
    val title: String,
    val priority: Priority
)