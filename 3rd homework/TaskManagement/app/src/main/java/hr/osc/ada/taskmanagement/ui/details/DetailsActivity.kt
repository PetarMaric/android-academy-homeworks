package hr.osc.ada.taskmanagement.ui.details

import hr.osc.ada.taskmanagement.R
import hr.osc.ada.taskmanagement.model.Task
import hr.osc.ada.taskmanagement.persistence.Repository
import hr.osc.ada.taskmanagement.ui.common.BaseActivity
import hr.osc.ada.taskmanagement.ui.rootAppActivity.RootAppActivity
import kotlinx.android.synthetic.main.activity_details.*

class DetailsActivity : BaseActivity() {
    private val repository = Repository

    private var tasksList: MutableList<Task> = mutableListOf()

    override fun getViewResourceId(): Int = R.layout.activity_details

    override fun setUpUi() {

        tasksList = repository.getAllTasks()

        savedTasksList.adapter = TaskListAdapter(RootAppActivity.ApplicationContext, R.layout.holder_task, tasksList)
    }
}
