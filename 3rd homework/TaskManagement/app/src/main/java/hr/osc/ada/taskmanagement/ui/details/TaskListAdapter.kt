package hr.osc.ada.taskmanagement.ui.details

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView
import hr.osc.ada.taskmanagement.R
import hr.osc.ada.taskmanagement.model.Task
import hr.osc.ada.taskmanagement.persistence.Repository
import kotlinx.android.synthetic.main.holder_task.view.*

// korišten dio informacija s https://www.zoftino.com/android-kotlin-listview-example
class TaskListAdapter(context: Context, val layout: Int, val taskList: MutableList<Task>) :
    ArrayAdapter<Task>(context, R.layout.holder_task, taskList) {

    private class ViewHolder {
        internal var taskTitle: TextView? = null
        internal var taskDescription: TextView? = null
        internal var taskPriority: TextView? = null
    }

    private val repository = Repository

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        var view = convertView
        val viewHolder: ViewHolder

        if (convertView == null) {
            val inflater = LayoutInflater.from(context)
            view = inflater.inflate(layout, parent, false)
            viewHolder = ViewHolder()
            viewHolder.taskTitle = view.taskTitle
            viewHolder.taskDescription = view.taskDescription
            viewHolder.taskPriority = view.taskPriority
        } else viewHolder = view?.tag as ViewHolder

        val currentTask = taskList.get(position)

        viewHolder.taskTitle?.text = currentTask.title
        viewHolder.taskPriority?.text = currentTask.priority.toString()
        viewHolder.taskDescription?.text = currentTask.description


        view!!.tag = viewHolder

        view.setOnLongClickListener { deleteSelectedTask(currentTask); notifyDataSetChanged(); true }

        return view
    }

    private fun deleteSelectedTask(task: Task) {
        repository.deleteTask(task)
    }

}