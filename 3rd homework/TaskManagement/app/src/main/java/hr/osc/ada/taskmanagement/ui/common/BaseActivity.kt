package hr.osc.ada.taskmanagement.ui.common

import android.os.Bundle
import android.support.v7.app.AppCompatActivity


abstract class BaseActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(getViewResourceId())

        setUpUi()
    }

    abstract fun getViewResourceId(): Int

    abstract fun setUpUi()
}