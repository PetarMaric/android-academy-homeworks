package hr.osc.ada.taskmanagement.ui.newTask

import android.content.Intent
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.widget.ArrayAdapter
import hr.osc.ada.taskmanagement.R
import hr.osc.ada.taskmanagement.model.Priority
import hr.osc.ada.taskmanagement.model.Task
import hr.osc.ada.taskmanagement.persistence.Repository
import hr.osc.ada.taskmanagement.ui.common.BaseActivity
import hr.osc.ada.taskmanagement.ui.details.DetailsActivity
import hr.osc.ada.taskmanagement.ui.rootAppActivity.RootAppActivity
import kotlinx.android.synthetic.main.activity_new_task.*
import java.lang.Exception

class NewTaskActivity : BaseActivity() {

    companion object {
        const val TAG = "Taskie"
    }

    private val repository = Repository

    private var validTaskTitle: Boolean = false
    private var validTaskDescription: Boolean = false

    override fun getViewResourceId(): Int = R.layout.activity_new_task

    override fun setUpUi() {

        checkNewTaskActivityValidity(validTaskTitle, validTaskDescription)

        addNewTaskButton.setOnClickListener {
            Log.d(TAG, getString(R.string.app_name))
            saveTask()
        }

        newTaskPriority.adapter = ArrayAdapter<Priority>(
            RootAppActivity.ApplicationContext,
            android.R.layout.simple_spinner_dropdown_item,
            Priority.values()
        )

        val taskTitleWatcher = object : TextWatcher {
            private var tempTitle = ""
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            }

            override fun afterTextChanged(s: Editable?) {
                try {
                    tempTitle = s.toString()
                    validTaskTitle = !tempTitle.isBlank()

                } catch (e: Exception) {
                    Log.e(TAG, e.toString())
                }

                checkNewTaskActivityValidity(validTaskTitle, validTaskDescription)
            }

        }

        val taskDescriptionWatcher = object : TextWatcher {
            private var tempDescription = ""
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            }

            override fun afterTextChanged(s: Editable?) {
                try {
                    tempDescription = s.toString()
                    validTaskDescription = !tempDescription.isBlank()

                } catch (e: Exception) {
                    Log.e(TAG, e.toString())
                }

                checkNewTaskActivityValidity(validTaskTitle, validTaskDescription)
            }

        }

        newTaskTitleInput.addTextChangedListener(taskTitleWatcher)
        newTaskDescriptionInput.addTextChangedListener(taskDescriptionWatcher)

    }

    private fun saveTask() {

        val title = newTaskTitleInput.text.toString()
        val description = newTaskDescriptionInput.text.toString()
        val priority = newTaskPriority.selectedItem as Priority
        cleanInputFields()

        val task = Task(title = title, description = description, priority = priority)

        val id = repository.saveTask(task)

        Log.d(TAG, id.toString())
        navigateToDetails()

    }

    private fun cleanInputFields() {
        newTaskTitleInput.text.clear()
        newTaskDescriptionInput.text.clear()
    }

    private fun navigateToDetails() {

        val detailsIntent = Intent(this, DetailsActivity::class.java)
        startActivity(detailsIntent)
    }

    private fun checkNewTaskActivityValidity(validTaskTitle: Boolean, validTaskDescription: Boolean) {
        addNewTaskButton.isEnabled = validTaskTitle && validTaskDescription
    }


}
