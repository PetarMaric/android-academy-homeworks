package hr.osc.ada.taskmanagement.model

enum class Priority {
    LOW,
    MEDIUM,
    HIGH;
}
