package hr.ferit.petarmaric.bmiapp.ui.userInputActivity

import hr.ferit.petarmaric.bmiapp.R
import hr.ferit.petarmaric.bmiapp.bmiCalculator.BMICalculator
import hr.ferit.petarmaric.bmiapp.util.maxWeight
import hr.ferit.petarmaric.bmiapp.util.minHeight
import hr.ferit.petarmaric.bmiapp.util.minWeight
import hr.ferit.petarmaric.bmiapp.ui.common.BaseActivity
import hr.ferit.petarmaric.bmiapp.ui.listeners.HeightInputWatcher
import hr.ferit.petarmaric.bmiapp.ui.listeners.WeightInputWatcher
import hr.ferit.petarmaric.thoughts.common.showToast
import kotlinx.android.synthetic.main.activity_user_input.*

class DataInputActivity : BaseActivity() {
    private var heightValid: Boolean = false
    private var weightValid: Boolean = false

    private var height: Double = 0.0
    private var weight: Double = 0.0

    override fun getLayoutResourceId(): Int = R.layout.activity_user_input

    override fun setUpUi() {
        checkFormValidity()
        setEditTextListeners()
    }

    private fun setEditTextListeners() {
        heightInput.addTextChangedListener(HeightInputWatcher(this))
        weightInput.addTextChangedListener(WeightInputWatcher(this))

        calculateBmiButton.setOnClickListener { calculateBmi() }
    }

    private fun calculateBmi() {
        height = heightInput.text.toString().toDouble()
        weight = weightInput.text.toString().toDouble()
        BMICalculator.calculateBmi(height = height, weight = weight)
    }

    internal fun setWeightInputValidity(weightValidity: Boolean) {
        weightValid = weightValidity
        if (!weightValid) {
            showToast(getString(R.string.weightOutsideRangeMessage))
            weightInput.error = getString(
                R.string.weightSetErrorText,
                minWeight,
                maxWeight
            )
        }
        checkFormValidity()
    }

    internal fun setHeightInputValidity(heightValidity: Boolean) {
        heightValid = heightValidity
        if (!heightValidity) {
            showToast(getString(R.string.heightOutsideRangeMessage))
            heightInput.error = getString(
                R.string.heightSetErrorText,
                minHeight,
                minHeight
            )

        }
        checkFormValidity()
    }

    private fun checkFormValidity() {
        calculateBmiButton.isEnabled = heightValid && weightValid
    }
}
