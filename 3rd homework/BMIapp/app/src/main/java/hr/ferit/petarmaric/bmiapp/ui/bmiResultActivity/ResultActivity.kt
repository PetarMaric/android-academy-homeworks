package hr.ferit.petarmaric.bmiapp.ui.bmiResultActivity

import hr.ferit.petarmaric.bmiapp.R
import hr.ferit.petarmaric.bmiapp.ui.common.BaseActivity
import hr.ferit.petarmaric.bmiapp.ui.listeners.HeightInputWatcher
import kotlinx.android.synthetic.main.activity_bmi_result.*
import kotlinx.android.synthetic.main.activity_user_input.*

class ResultActivity : BaseActivity() {

    companion object {
        const val BMI_VALUE = "bmi_value"
        const val BMI_CATEGORY = "bmi_category"
        const val BMI_DESCRIPTION = "bmi_description"
        const val BMI_PICTURE = "bmi_picture"

    }

    override fun getLayoutResourceId(): Int = R.layout.activity_bmi_result

    override fun setUpUi() {
        handleIntent()
    }

    private fun handleIntent() {
        val intent = intent

        bmiValue.text = getString(R.string.bmiValueTemplate, intent.getDoubleExtra(BMI_VALUE, -1.0))
        bmiCategory.text = getString(intent.getIntExtra(BMI_CATEGORY, -1))
        bmiDescription.text = getString(intent.getIntExtra(BMI_DESCRIPTION, -1))
        bmiPicture.setImageResource(intent.getIntExtra(BMI_PICTURE, -1))


    }

}