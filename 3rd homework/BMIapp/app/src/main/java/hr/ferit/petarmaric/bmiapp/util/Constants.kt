package hr.ferit.petarmaric.bmiapp.util

const val minHeight = 0.54
const val maxHeight = 2.5

const val minWeight = 30.0
const val maxWeight = 300.0