package hr.ferit.petarmaric.bmiapp.ui.listeners

import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import hr.ferit.petarmaric.bmiapp.R
import hr.ferit.petarmaric.bmiapp.util.maxWeight
import hr.ferit.petarmaric.bmiapp.util.minWeight
import hr.ferit.petarmaric.bmiapp.ui.userInputActivity.DataInputActivity
import hr.ferit.petarmaric.thoughts.common.showToast
import java.lang.NumberFormatException

class WeightInputWatcher(view: DataInputActivity) : TextWatcher {

    private val activityReference: DataInputActivity = view
    private var tempWeight: Double = 0.0
    private var validWeight = false

    companion object {
        const val TAG = "WEIGHT_EXCEPTION"
    }

    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
    }

    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

    }

    override fun afterTextChanged(s: Editable?) {
        try {
            tempWeight = s.toString().toDouble()
            validWeight = if (tempWeight < minWeight || tempWeight > maxWeight) {
                activityReference.showToast(activityReference.getString(R.string.weightOutsideRangeMessage))
                false
            } else {
                true
            }

            activityReference.setWeightInputValidity(validWeight)

        } catch (numberFormatException: NumberFormatException) {
            Log.e(TAG, numberFormatException.toString())
        }


    }

}