package hr.ferit.petarmaric.bmiapp.ui.listeners

import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import hr.ferit.petarmaric.bmiapp.util.maxHeight
import hr.ferit.petarmaric.bmiapp.util.minHeight
import hr.ferit.petarmaric.bmiapp.ui.userInputActivity.DataInputActivity
import java.lang.NumberFormatException

class HeightInputWatcher(view: DataInputActivity) : TextWatcher {

    private val activityReference: DataInputActivity = view
    private var tempHeight: Double = 0.0
    private var validHeight = false

    companion object {
        const val TAG = "HEIGHT_EXCEPTION"
    }

    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
    }

    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

    }

    override fun afterTextChanged(s: Editable?) {
        try {
            tempHeight = s.toString().toDouble()
            validHeight = !(tempHeight < minHeight || tempHeight > maxHeight)
            activityReference.setHeightInputValidity(validHeight)

        } catch (numberFormatException: NumberFormatException) {
            Log.e(TAG, numberFormatException.toString())
        }


    }
}