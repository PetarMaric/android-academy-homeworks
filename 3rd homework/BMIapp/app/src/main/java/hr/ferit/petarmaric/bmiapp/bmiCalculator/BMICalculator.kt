package hr.ferit.petarmaric.bmiapp.bmiCalculator

import android.content.Intent
import hr.ferit.petarmaric.bmiapp.R
import hr.ferit.petarmaric.bmiapp.ui.bmiResultActivity.ResultActivity
import hr.ferit.petarmaric.bmiapp.ui.rootAppActivity.RootAppActivity


object BMICalculator {
    private var bmiValue: Double = 0.0
    private var bmiCategoryResourceId: Int = 0
    private var bmiDescriptionResourceId: Int = 0
    private var bmiPicture: Int = 0

    fun calculateBmi(height: Double, weight: Double) {

        bmiValue = weight / Math.pow(height, 2.0)

        if (bmiValue < 18.5 && bmiValue > 0) {
            bmiCategoryResourceId = R.string.bmiUnderweight
            bmiDescriptionResourceId = R.string.underweightCategoryDescription
            // http://www.orthohyd.com/home/know-your-disease/bmi---body-mass-index
            bmiPicture = R.mipmap.bmi_underweight
        } else if (bmiValue in 18.5..24.9) {
            bmiCategoryResourceId = R.string.bmiNormal
            bmiDescriptionResourceId = R.string.normalCategoryDescription
            // http://www.orthohyd.com/home/know-your-disease/bmi---body-mass-index
            bmiPicture = R.mipmap.bmi_normalweight
        } else if (bmiValue in 25.0..29.9) {
            bmiCategoryResourceId = R.string.bmiOverweight
            bmiDescriptionResourceId = R.string.overweightCategoryDescription
            // http://www.orthohyd.com/home/know-your-disease/bmi---body-mass-index
            bmiPicture = R.mipmap.bmi_overweight
        } else if (bmiValue >= 30) {
            bmiCategoryResourceId = R.string.bmiObese
            bmiDescriptionResourceId = R.string.obeseCategoryDescription
            // http://www.orthohyd.com/home/know-your-disease/bmi---body-mass-index
            bmiPicture = R.mipmap.bmi_obese
        }

        sendIntent()
    }

    private fun sendIntent() {

        val intent = Intent(RootAppActivity.ApplicationContext, ResultActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
        intent.putExtra(ResultActivity.BMI_VALUE, bmiValue)
        intent.putExtra(ResultActivity.BMI_CATEGORY, bmiCategoryResourceId)
        intent.putExtra(ResultActivity.BMI_DESCRIPTION, bmiDescriptionResourceId)
        intent.putExtra(ResultActivity.BMI_PICTURE, bmiPicture)

        RootAppActivity.ApplicationContext.startActivity(intent)

    }

}