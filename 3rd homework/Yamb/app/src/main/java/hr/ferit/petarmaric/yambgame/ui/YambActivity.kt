package hr.ferit.petarmaric.yambgame.ui

import android.util.Log
import hr.ferit.petarmaric.yambgame.R
import hr.ferit.petarmaric.yambgame.yamb.model.Dice
import hr.ferit.petarmaric.yambgame.ui.common.BaseActivity
import hr.ferit.petarmaric.yambgame.ui.listener.ClickListener
import hr.ferit.petarmaric.yambgame.yamb.YambGame
import kotlinx.android.synthetic.main.activity_yamb.*

class YambActivity : BaseActivity() {

    override fun getLayoutResourceId(): Int = R.layout.activity_yamb

    override fun setUpUi() {

        buttonRollDices.setOnClickListener { rollDices() }

        firstDice.setOnClickListener(ClickListener())
        secondDice.setOnClickListener(ClickListener())
        thirdDice.setOnClickListener(ClickListener())
        fourthDice.setOnClickListener(ClickListener())
        fifthDice.setOnClickListener(ClickListener())
        sixthDice.setOnClickListener(ClickListener())

    }

    private fun rollDices() {
        val dices = YambGame.throwDices()
        setDicesBackground(dices)
    }

    private fun setDicesBackground(dices: List<Dice>) {
        firstDice.setImageResource(dices[0].icon)
        secondDice.setImageResource(dices[1].icon)
        thirdDice.setImageResource(dices[2].icon)
        fourthDice.setImageResource(dices[3].icon)
        fifthDice.setImageResource(dices[4].icon)
        sixthDice.setImageResource(dices[5].icon)
    }

}
