package hr.ferit.petarmaric.yambgame.ui.listener

import android.view.View
import hr.ferit.petarmaric.yambgame.yamb.YambGame

class ClickListener : View.OnClickListener {

    override fun onClick(v: View?) {
        YambGame.selectedDice(v)
    }
}