package hr.ferit.petarmaric.yambgame.yamb

import android.util.Log
import android.view.View
import android.widget.ImageView
import hr.ferit.petarmaric.yambgame.R
import hr.ferit.petarmaric.yambgame.ui.rootAppActivity.RootAppActivity
import hr.ferit.petarmaric.yambgame.util.showToast
import hr.ferit.petarmaric.yambgame.yamb.model.Dice
import hr.ferit.petarmaric.yambgame.yamb.model.Icons
import hr.ferit.petarmaric.yambgame.yamb.model.defaultIcon
import kotlin.random.Random

object YambGame {
    private var numberOfTurns = 0

    private val listOfDiceObjects = listOf(
        Dice(), Dice(), Dice(), Dice(), Dice(), Dice()
    )

    fun throwDices(): List<Dice> {

        return when (numberOfTurns) {
            0 -> {
                listOfDiceObjects.forEach {
                    it.randomValue = generateRandomNumber()
                    it.icon = determineDiceIcon(it)
                }
                numberOfTurns++
                listOfDiceObjects
            }
            3 -> {
                numberOfTurns = 0
                RootAppActivity.ApplicationContext.showToast(
                    "${RootAppActivity.ApplicationContext.getString(R.string.endGameText)} ${calculatePoints(
                        listOfDiceObjects
                    )}"
                )
                resetDices()
            }
            else -> {
                listOfDiceObjects.forEach {
                    if (!it.selected) {
                        it.randomValue = generateRandomNumber()
                        it.icon = determineDiceIcon(it)
                    } else it.icon = determineDiceIcon(it)
                }
                numberOfTurns++
                listOfDiceObjects
            }
        }
    }

    private fun calculatePoints(dice: List<Dice>): Int {
        var points: Int = 0
        dice.forEach { points += it.randomValue }
        return points
    }

    private fun resetDices(): List<Dice> {
        listOfDiceObjects.forEach {
            it.selected = false
            it.randomValue = 0
            it.icon = defaultIcon
        }
        return listOfDiceObjects
    }

    private fun generateRandomNumber() = Random.nextInt(1, 6)

    private fun determineDiceIcon(dice: Dice) =
        when (dice.randomValue) {
            1 -> if (dice.selected) Icons.FIRST.selectedIcon else Icons.FIRST.defaultIcon
            2 -> if (dice.selected) Icons.SECOND.selectedIcon else Icons.SECOND.defaultIcon
            3 -> if (dice.selected) Icons.THIRD.selectedIcon else Icons.THIRD.defaultIcon
            4 -> if (dice.selected) Icons.FOURTH.selectedIcon else Icons.FOURTH.defaultIcon
            5 -> if (dice.selected) Icons.FIFTH.selectedIcon else Icons.FIFTH.defaultIcon
            else -> {
                if (dice.selected) Icons.SIXTH.selectedIcon else Icons.SIXTH.defaultIcon
            }
        }

    fun selectedDice(v: View?) {
        if (numberOfTurns == 0) {
            return
        }
        val button = v as ImageView
        when (button.id) {
            R.id.firstDice -> {
                listOfDiceObjects[0].selected = !listOfDiceObjects[0].selected
                button.setImageResource(determineDiceIcon(listOfDiceObjects[0]))
            }
            R.id.secondDice -> {
                listOfDiceObjects[1].selected = !listOfDiceObjects[1].selected
                button.setImageResource(determineDiceIcon(listOfDiceObjects[1]))
            }
            R.id.thirdDice -> {
                listOfDiceObjects[2].selected = !listOfDiceObjects[2].selected
                button.setImageResource(determineDiceIcon(listOfDiceObjects[2]))
            }
            R.id.fourthDice -> {
                listOfDiceObjects[3].selected = !listOfDiceObjects[3].selected
                button.setImageResource(determineDiceIcon(listOfDiceObjects[3]))
            }
            R.id.fifthDice -> {
                listOfDiceObjects[4].selected = !listOfDiceObjects[4].selected
                button.setImageResource(determineDiceIcon(listOfDiceObjects[4]))
            }
            else -> {
                listOfDiceObjects[5].selected = !listOfDiceObjects[5].selected
                button.setImageResource(determineDiceIcon(listOfDiceObjects[5]))
            }

        }
    }


}