package hr.ferit.petarmaric.yambgame.yamb.model

data class Dice(
    var randomValue: Int = 0,
    var selected: Boolean = false,
    var icon: Int = 0
)