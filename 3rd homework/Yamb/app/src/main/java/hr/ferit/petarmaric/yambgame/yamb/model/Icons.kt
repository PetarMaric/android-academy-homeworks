package hr.ferit.petarmaric.yambgame.yamb.model

import hr.ferit.petarmaric.yambgame.R

internal const val defaultIcon = R.drawable.ic_question

enum class Icons(val defaultIcon: Int, val selectedIcon: Int) {
    FIRST(R.drawable.ic_one_default, R.drawable.ic_one_selected),
    SECOND(R.drawable.ic_two_default, R.drawable.ic_two_selected),
    THIRD(R.drawable.ic_three_default, R.drawable.ic_three_selected),
    FOURTH(R.drawable.ic_four_default, R.drawable.ic_four_selected),
    FIFTH(R.drawable.ic_five_default, R.drawable.ic_five_selected),
    SIXTH(R.drawable.ic_six_default, R.drawable.ic_six_selected)
}

