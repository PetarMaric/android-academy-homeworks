package exercises

import kotlin.random.Random

/*
Create a collection of integers

Use Random to fill the collection with 100 random numbers between 1 and 100.

Go through the collection from start to end and print its elements up to the point where an element is less than or equal to 10
*/


fun main() {
    val intCollection = mutableListOf<Int>()

    for (i in 1..100) {
        intCollection.add(Random.nextInt(1, 100))
    }

    println()
    intCollection.forEach({ print("$it ") })
    println()


    // going through whole collection and printing only those elements which are less than or equal to 10
    println()
    intCollection.filter { it <= 10 }
            .forEach({ print("$it ") })
    println()

    // taking elements up to the first value greater than 10 and then printing all numbers which satisfy previous condition
    println()
    intCollection.takeWhile { it >= 10 }
            .forEach { print("$it ") }
    println()

}