//package exercises
//
///*
//2. Exercise
//Check out Kotlin Koans and try to solve as many exercises as you can (with emphasis on collections)
//https://play.kotlinlang.org/koans/overview
// */
//
//
///**
// *
// *                  Introduction
// *
// */
////region Introduction
//
//fun start(): String = "OK"
//
//fun joinOptions(options: Collection<String>) = options.joinToString(prefix = "[", postfix = "]")
//
//fun foo(name: String, number: Int = 42, toUpperCase: Boolean = false) =
//    (if (toUpperCase) name.toUpperCase() else name) + number
//
//fun containsEven(collection: Collection<Int>): Boolean = collection.any { it % 2 == 0}
//
//fun getPattern(): String = """\d{2}\s${month}\s\d{4}"""
//
//data class Person(val name: String, val age: Int)
//
//fun sendMessageToClient(
//    client: Client?, message: String?, mailer: Mailer
//){
//    val email = client?.personalInfo?.email
//    if(email != null && message != null) {
//        mailer.sendMessage(email, message)
//    }
//
//}
//
//
///**
// *
// *
// * Why not
// * fun eval(expr: Expr): Int =
//    when (expr) {
//    is Num -> expr.value
//    is Sum -> { val a: Int? = expr.left as? Int
//                val b: Int? = expr.right as? Int
//                if(a!= null && b!= null) a+b else 0}
//    else -> throw IllegalArgumentException("Unknown expression")
//    }
// *
// */
//fun eval(expr: Expr): Int =
//    when (expr) {
//        is Num -> expr.value
//        is Sum -> eval(expr.left) + eval(expr.right)
//        else -> throw IllegalArgumentException("Unknown expression")
//    }
//
//
//fun Int.r(): RationalNumber = RationalNumber(this, 1)
//// https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-pair/index.html
//fun Pair<Int, Int>.r(): RationalNumber = RationalNumber(first, second)
//
//Collections.sort(arrayList,
//object: Comparator<Int> {
//    override fun compare(p0: Int, p1: Int) = p1 - p0
//})
//
//Collections.sort(arrayList, { x, y -> y - x })
//
//fun getList(): List<Int> {
//    return arrayListOf(1, 5, 2).sortedDescending()
//}
//
////endregion
//
//
//
///**
// *
// *                  Conventions
// *
// */
////region Conventions
////endregion
//
//
//
//
//
//
//
///**
// *
// *                  Collections
// *
// */
////region Collections
//
//
////region Introduction
//
//fun Shop.getSetOfCustomers(): Set<Customer> = customers.toSet()
//
////endregion
//
////region Filter map
//
//// Return the set of cities the customers are from
//fun Shop.getCitiesCustomersAreFrom(): Set<City> = customers.map { it.city }.toSet()
//
//// Return a list of the customers who live in the given city
//fun Shop.getCustomersFrom(city: City): List<Customer> = customers.filter { it.city == city }
//
////endregion
//
////region All Any and other predicates
//
//// Return true if all customers are from the given city
//fun Shop.checkAllCustomersAreFrom(city: City): Boolean = customers.all { it.city == city }
//
//// Return true if there is at least one customer from the given city
//fun Shop.hasCustomerFrom(city: City): Boolean = customers.any { it.city == city }
//
//// Return the number of customers from the given city
//fun Shop.countCustomersFrom(city: City): Int = customers.count { it.city == city }
//
//// Return a customer who lives in the given city, or null if there is none
//fun Shop.findAnyCustomerFrom(city: City): Customer? = customers.find { it.city == city }
//
////endregion
//
////region FlatMap
//
//// Return all products this customer has ordered
//val Customer.orderedProducts: Set<Product>
//    get() {
//        return orders.flatMap { it.products }.toSet()
//    }
//
//// Return all products that were ordered by at least one customer
//val Shop.allOrderedProducts: Set<Product>
//    get() {
//        return customers.flatMap { it.orderedProducts }.toSet()
//    }
//
////endregion
//
//
////region Max min
//
//// Return a customer whose order count is the highest among all customers
//fun Shop.getCustomerWithMaximumNumberOfOrders(): Customer? = customers.maxBy { it.orders.size }
//
//// Return the most expensive product which has been ordered
//fun Customer.getMostExpensiveOrderedProduct(): Product? = orders.flatMap { it.products }.maxBy { it.price }
//
////endregion
//
////region Sort
//
//// Return a list of customers, sorted by the ascending number of orders they made
//fun Shop.getCustomersSortedByNumberOfOrders(): List<Customer> = customers.sortedBy { it.orders.size }
//
////endregion
//
////region Sum
//
//// Return the sum of prices of all products that a customer has ordered.
//// Note: the customer may order the same product for several times.
//fun Customer.getTotalOrderPrice(): Double = orders.flatMap { it.products }.sumByDouble { it.price }
//
////endregion
//
////region GroupBy
//
//// Return a map of the customers living in each city
//fun Shop.groupCustomersByCity(): Map<City, List<Customer>> = customers.groupBy { it.city }
//
////endregion
//
////region Partition
//
//// magic; particiju uglavnom destrukturirati u dvje varijable
//
//// Return customers who have more undelivered orders than delivered
//fun Shop.getCustomersWithMoreUndeliveredOrdersThanDelivered(): Set<Customer> = customers.filter {
//    val (delivered, undelivered) = it.orders.partition { it.isDelivered }
//    undelivered.size > delivered.size
//}.toSet()
//
////endregion
//
////region Fold
//X
////endregion
//
////region Compound tasks
//
//// Return the most expensive product among all delivered products
//// (use the Order.isDelivered flag)
//fun Customer.getMostExpensiveDeliveredProduct(): Product? {
//    return orders.filter { it.isDelivered }.flatMap { it.products }.maxBy { it.price }
//}
//
////endregion
//
//
////endregion
//
//
//
///**
// *
// *                  Properties
// *
// */
////region Properties
////endregion
//
//
///**
// *
// *                  Builders
// *
// */
////region Builders
////endregion
//
//
///**
// *
// *                  Generics
// *
// */
////region Generics
////endregion