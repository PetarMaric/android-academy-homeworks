package hr.ferit.petarmaric.taskie.ui.activities

import android.app.AlertDialog
import android.view.Menu
import android.view.MenuItem
import hr.ferit.petarmaric.taskie.R
import hr.ferit.petarmaric.taskie.persistence.database.TasksRepository
import hr.ferit.petarmaric.taskie.persistence.database.TasksRoomRepository
import hr.ferit.petarmaric.taskie.ui.activities.base.BaseActivity
import hr.ferit.petarmaric.taskie.ui.adapters.recyclerview.TaskAdapter
import hr.ferit.petarmaric.taskie.ui.fragments.tasks.TasksFragment
import hr.ferit.petarmaric.taskie.ui.listeners.NavigationMenuListener
import kotlinx.android.synthetic.main.navigation_bottom_navigation.*

class MainActivity : BaseActivity() {

    private val repository: TasksRepository = TasksRoomRepository()

    override fun getLayoutResourceId() = R.layout.activity_main

    override fun setUpUi() {

        showFragment(TasksFragment.newInstance())

        bottomNavigationMaterial.setOnNavigationItemSelectedListener(NavigationMenuListener(this))

    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        super.onCreateOptionsMenu(menu)
        menuInflater.inflate(R.menu.menu_options, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.sortTasksByPriority -> sortTasksByPriority()
            R.id.deleteAllTasks -> deleteAllTasks()
        }
        return super.onOptionsItemSelected(item)
    }

    private fun deleteAllTasks() {
        // nije se nasla bolja alternativa pristupu recycler viewu, tj. njegovom adapteru
        val recyclerViewAdapterInstance = (TaskAdapter.recyclerViewInstance.adapter as TaskAdapter)
        AlertDialog.Builder(this, R.style.AppCompatAlertDialogStyle)
            .setTitle("Do you want to delete all tasks?")
            .setPositiveButton("All of 'em") { _, _ ->

                // drugi alert dodan je kao dodatno upozorenje; takoder se htjelo isprobati ugnjezdivanje alert dijaloga
                AlertDialog.Builder(this, R.style.AppCompatAlertDialogStyle)
                    .setTitle("Are you sure? List can not be recreated!")
                    .setPositiveButton("Delete") { _, _ ->
                        repository.deleteAllTasks()
                        recyclerViewAdapterInstance.refreshData()

                    }
                    .setNegativeButton("Cancel") { _, _ ->
                        recyclerViewAdapterInstance.notifyDataSetChanged()
                    }
                    .create()
                    .show()

            }
            .setNegativeButton("Cancel") { _, _ ->
                recyclerViewAdapterInstance.notifyDataSetChanged()
            }
            .create()
            .show()
    }

    private fun sortTasksByPriority() {
        (TaskAdapter.recyclerViewInstance.adapter as TaskAdapter).sortTasks()
    }
}