package hr.ferit.petarmaric.taskie.ui.fragments.about

import hr.ferit.petarmaric.taskie.R
import hr.ferit.petarmaric.taskie.ui.fragments.base.BaseFragment

class AboutApplication : BaseFragment() {
    override fun getLayoutResourceId(): Int = R.layout.fragment_about_application

    companion object {
        fun getInstance(): AboutApplication = AboutApplication()
    }

}