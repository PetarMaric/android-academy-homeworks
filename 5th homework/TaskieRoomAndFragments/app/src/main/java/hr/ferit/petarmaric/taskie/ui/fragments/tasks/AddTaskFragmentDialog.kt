package hr.ferit.petarmaric.taskie.ui.fragments.tasks


import android.os.Bundle
import android.text.TextUtils.isEmpty
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.ArrayAdapter
import androidx.fragment.app.DialogFragment
import hr.ferit.petarmaric.taskie.R
import hr.ferit.petarmaric.taskie.common.determinePriorityLocation
import hr.ferit.petarmaric.taskie.common.displayToast
import hr.ferit.petarmaric.taskie.model.Priority
import hr.ferit.petarmaric.taskie.model.Task
import hr.ferit.petarmaric.taskie.persistence.database.TasksRepository
import hr.ferit.petarmaric.taskie.persistence.database.TasksRoomRepository
import hr.ferit.petarmaric.taskie.persistence.defaultpreferences.TaskPrefs
import kotlinx.android.synthetic.main.fragment_dialog_new_task.*

class AddTaskFragmentDialog : DialogFragment() {

    private var taskAddedListener: TaskAddedListener? = null

    private val repository: TasksRepository = TasksRoomRepository()

    companion object {
        fun newInstance(): AddTaskFragmentDialog {
            return AddTaskFragmentDialog()
        }

        val DEFAULT_PRIORITY = Priority.values().first().toString()
        const val STORED_PRIORITY = "STORED_PRIORITY"
    }

    interface TaskAddedListener {
        fun onTaskAdded(task: Task)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NO_TITLE, R.style.FragmentDialogTheme)
    }

    fun setTaskAddedListener(listener: TaskAddedListener) {
        taskAddedListener = listener
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_dialog_new_task, container)
    }

    override fun onStart() {
        super.onStart()
        dialog?.window?.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initUi()
        initListeners()
    }

    private fun initUi() {


        context?.let {
            prioritySelector.adapter =
                ArrayAdapter<Priority>(it, android.R.layout.simple_spinner_dropdown_item, Priority.values())

            val storedPriority = TaskPrefs.getPrefsString(STORED_PRIORITY, DEFAULT_PRIORITY)


            prioritySelector.setSelection(determinePriorityLocation(storedPriority))


        }
    }

    private fun initListeners() {
        saveTaskAction.setOnClickListener { saveTask() }
    }

    private fun saveTask() {
        if (isInputEmpty()) {
            context?.displayToast(getString(R.string.emptyFields))
            return
        }

        val title = newTaskTitleInput.text.toString()
        val description = newTaskDescriptionInput.text.toString()
        val priority = prioritySelector.selectedItem as Priority
        val task = repository.save(title, description, priority)

        storePriority(priority)

        clearUi()

        taskAddedListener?.onTaskAdded(task)
        dismiss()
    }

    private fun storePriority(priority: Priority) {

        Log.d(STORED_PRIORITY, priority.toString())

        TaskPrefs.storePrefs(STORED_PRIORITY, priority.toString())
    }

    private fun clearUi() {
        newTaskTitleInput.text.clear()
        newTaskDescriptionInput.text.clear()
        prioritySelector.setSelection(0)
    }

    private fun isInputEmpty(): Boolean = isEmpty(newTaskTitleInput.text) || isEmpty(newTaskDescriptionInput.text)


}