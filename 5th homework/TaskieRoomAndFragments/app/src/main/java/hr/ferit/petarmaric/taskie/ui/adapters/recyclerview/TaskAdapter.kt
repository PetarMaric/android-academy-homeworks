package hr.ferit.petarmaric.taskie.ui.adapters.recyclerview

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.Adapter
import hr.ferit.petarmaric.taskie.R
import hr.ferit.petarmaric.taskie.model.Task
import hr.ferit.petarmaric.taskie.persistence.database.TasksRepository
import hr.ferit.petarmaric.taskie.persistence.database.TasksRoomRepository

class TaskAdapter(private val onItemSelected: (Task) -> Unit) : Adapter<TaskHolder>() {

    private val repository: TasksRepository = TasksRoomRepository()

    private val data: MutableList<Task> = mutableListOf()

    companion object {
        lateinit var recyclerViewInstance: RecyclerView
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TaskHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.item_task, parent, false)
        return TaskHolder(v)
    }

    override fun getItemCount() = data.size

    override fun onBindViewHolder(holder: TaskHolder, position: Int) {
        holder.bindData(data[position], onItemSelected)
    }

    override fun onAttachedToRecyclerView(recyclerView: RecyclerView) {
        super.onAttachedToRecyclerView(recyclerView)

        recyclerViewInstance = recyclerView
    }

    fun setData(data: List<Task>) {
        this.data.clear()
        this.data.addAll(data)
        notifyDataSetChanged()
    }

    fun removeTask(task: Task) {
        data.remove(task)
    }

    fun sortTasks() {
        data.clear()
        data.addAll(repository.getAllTasks())
        data.sortByDescending { it.priority.getPriorityOrder() }
        notifyDataSetChanged()
    }

    fun getTaskAtPosition(position: Int) = data[position]

    fun refreshData() {
        data.clear()
        data.addAll(repository.getAllTasks())
        notifyDataSetChanged()
    }
}





