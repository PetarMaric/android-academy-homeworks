package hr.ferit.petarmaric.taskie.persistence.database

import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import hr.ferit.petarmaric.taskie.Taskie
import hr.ferit.petarmaric.taskie.model.Task

@Database(entities = [Task::class], version = 1, exportSchema = false)
@TypeConverters(RoomTypeConverters::class)
abstract class TasksDatabase : RoomDatabase() {

    abstract fun getTasksDao(): TasksDao

    companion object {
        private val DB_NAME = "tasks_database"
        private val dbInstance by lazy {
            Room.databaseBuilder(
                Taskie.getApplicationContext(), TasksDatabase::class.java,
                DB_NAME
            ).allowMainThreadQueries()
                .build()
        }

        val tasksDao by lazy { dbInstance.getTasksDao() }
    }
}