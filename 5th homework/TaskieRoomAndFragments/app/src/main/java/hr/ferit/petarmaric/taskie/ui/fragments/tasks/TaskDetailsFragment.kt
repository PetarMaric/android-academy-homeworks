package hr.ferit.petarmaric.taskie.ui.fragments.tasks

import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.TextView
import hr.ferit.petarmaric.taskie.R
import hr.ferit.petarmaric.taskie.Taskie
import hr.ferit.petarmaric.taskie.common.EXTRA_TASK_ID
import hr.ferit.petarmaric.taskie.common.determinePriorityLocation
import hr.ferit.petarmaric.taskie.common.displayToast
import hr.ferit.petarmaric.taskie.common.showFragment
import hr.ferit.petarmaric.taskie.model.Priority
import hr.ferit.petarmaric.taskie.model.Task
import hr.ferit.petarmaric.taskie.persistence.database.TasksRepository
import hr.ferit.petarmaric.taskie.persistence.database.TasksRoomRepository
import hr.ferit.petarmaric.taskie.ui.adapters.spinneradapter.PriorityAdapter
import hr.ferit.petarmaric.taskie.ui.fragments.base.BaseFragment
import kotlinx.android.synthetic.main.fragment_task_details.*

class TaskDetailsFragment : BaseFragment() {
    private val repository: TasksRepository = TasksRoomRepository()

    private var taskID = NO_TASK

    private lateinit var originalTask: Task

    override fun getLayoutResourceId(): Int {
        return R.layout.fragment_task_details
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        arguments?.getInt(EXTRA_TASK_ID)?.let { taskID = it }
        tryDisplayTask(taskID)
    }

    private fun tryDisplayTask(id: Int) {
        try {
            val task = repository.get(id)
            originalTask = task
            displayTask(task)
        } catch (e: NoSuchElementException) {
            context?.displayToast(getString(R.string.noTaskFound))
        }
    }

    private fun displayTask(task: Task) {
        detailsTaskTitle.setText(task.title, TextView.BufferType.EDITABLE)
        detailsTaskDescription.setText(task.description, TextView.BufferType.EDITABLE)

        spinnerPriorities.adapter = PriorityAdapter(
            Taskie.getApplicationContext(),
            R.layout.item_priority_spinner_item,
            Priority.values().toList()
        )
        spinnerPriorities.setSelection(determinePriorityLocation(task.priority.name))

        submitTask.setOnClickListener { updateTask() }
    }


    private fun updateTask() {
        if (isInputEmpty()) {
            context?.displayToast(getString(R.string.emptyFields))
            return
        }

        val title = detailsTaskTitle.text.toString()
        val description = detailsTaskDescription.text.toString()
        val priority = spinnerPriorities.selectedItem as Priority

        val task = Task(id = originalTask.id, title = title, description = description, priority = priority)

        repository.updateTask(task)
        context?.displayToast(getString(R.string.taskUpdatedText))

        activity?.showFragment(R.id.fragmentContainer, TasksFragment.newInstance())

    }

    private fun isInputEmpty(): Boolean = TextUtils.isEmpty(detailsTaskTitle.text) || TextUtils.isEmpty(
        detailsTaskDescription.text
    )

    companion object {
        const val NO_TASK = -1

        fun newInstance(taskId: Int): TaskDetailsFragment {
            val bundle = Bundle().apply { putInt(EXTRA_TASK_ID, taskId) }
            return TaskDetailsFragment().apply { arguments = bundle }
        }
    }
}
