package hr.ferit.petarmaric.taskie.model

import androidx.annotation.ColorRes
import hr.ferit.petarmaric.taskie.R

enum class Priority(@ColorRes private val colorRes: Int, private val priorityOrder: Int) {
    LOW(R.color.colorLow, 1),
    MEDIUM(R.color.colorMedium, 2),
    HIGH(R.color.colorHigh,3);

    fun getColor(): Int = colorRes

    fun getPriorityOrder(): Int = priorityOrder
}

