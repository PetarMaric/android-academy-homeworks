package hr.ferit.petarmaric.taskie.persistence.database

import androidx.room.TypeConverter
import hr.ferit.petarmaric.taskie.model.Priority

class RoomTypeConverters {
    companion object {

        @TypeConverter
        @JvmStatic
        fun fromPriority(priority: Priority): String {
            return priority.name
        }

        @TypeConverter
        @JvmStatic
        fun toPriority(priority: String): Priority {
            return Priority.values().first { it.name == priority }
        }
    }
}