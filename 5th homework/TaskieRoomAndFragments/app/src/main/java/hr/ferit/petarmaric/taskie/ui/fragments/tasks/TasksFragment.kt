package hr.ferit.petarmaric.taskie.ui.fragments.tasks

import android.app.AlertDialog
import android.content.Context
import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import hr.ferit.petarmaric.taskie.R
import hr.ferit.petarmaric.taskie.common.*
import hr.ferit.petarmaric.taskie.model.Task
import hr.ferit.petarmaric.taskie.persistence.database.TasksRepository
import hr.ferit.petarmaric.taskie.persistence.database.TasksRoomRepository
import hr.ferit.petarmaric.taskie.ui.adapters.recyclerview.TaskAdapter
import hr.ferit.petarmaric.taskie.ui.fragments.base.BaseFragment
import kotlinx.android.synthetic.main.fragment_tasks.*

class TasksFragment : BaseFragment(),
    AddTaskFragmentDialog.TaskAddedListener {

    private val repository: TasksRepository = TasksRoomRepository()

    private val adapter by lazy { TaskAdapter { onItemSelected(it) } }
    private lateinit var contextInstance: Context

    override fun getLayoutResourceId() = R.layout.fragment_tasks

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initUi()
        initListeners()
        refreshTasks()
    }

    override fun onAttach(context: Context) {
        contextInstance = context
        super.onAttach(context)
    }

    private fun initUi() {
        progress.visible()
        noData.visible()


        tasksRecyclerView.layoutManager = LinearLayoutManager(context)
        tasksRecyclerView.adapter = adapter

        setRecyclerViewItemTouchListener()
    }

    private fun setRecyclerViewItemTouchListener() {
        val itemTouchCallback = object : ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT) {
            override fun onMove(
                recyclerView: RecyclerView,
                viewHolder: RecyclerView.ViewHolder,
                target: RecyclerView.ViewHolder
            ): Boolean {
                return false
            }

            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
                val position = viewHolder.adapterPosition
                val task = adapter.getTaskAtPosition(position)

                showDeleteConfirmationDialog(task)

            }

        }

        val itemTouchHelper = ItemTouchHelper(itemTouchCallback)
        itemTouchHelper.attachToRecyclerView(tasksRecyclerView)

    }

    private fun showDeleteConfirmationDialog(task: Task) {
        AlertDialog.Builder(context, R.style.AppCompatAlertDialogStyle)
            .setTitle("Do you want to delete task ${task.title} with ${task.priority} priority?")
            .setPositiveButton("Delete") { _, _ ->
                adapter.removeTask(task)
                repository.deleteTask(task)
                adapter.refreshData()
            }
            .setNegativeButton("Cancel") { _, _ ->
                adapter.notifyDataSetChanged()
            }
            .create()
            .show()
    }

    private fun initListeners() {
        addTask.setOnClickListener { addTask() }
    }

    private fun onItemSelected(task: Task) {

        activity?.showFragment(
            R.id.fragmentContainer,
            TaskDetailsFragment.newInstance(task.id),
            shouldAddToBackStack = true
        )

    }

    private fun refreshTasks() {
        progress.gone()
        val data = repository.getAllTasks()
        if (data.isNotEmpty()) {
            noData.gone()
        } else {
            noData.visible()
        }
        adapter.setData(data)
    }

    private fun addTask() {
        val dialog = AddTaskFragmentDialog.newInstance()
        dialog.setTaskAddedListener(this)
        dialog.show(childFragmentManager, dialog.tag)
    }

    override fun onTaskAdded(task: Task) {
        refreshTasks()
    }

    companion object {
        fun newInstance(): Fragment {
            return TasksFragment()
        }
    }
}