package hr.ferit.petarmaric.taskie.common

import hr.ferit.petarmaric.taskie.model.Priority

fun determinePriorityLocation(storedPriority: String): Int {
    val priorities = Priority.values().map { it.toString() }.toSet()

    val index = priorities.indexOf(storedPriority)
    return index
}