package hr.ferit.petarmaric.taskie.persistence.defaultpreferences

import android.content.Context
import android.content.SharedPreferences
import android.preference.PreferenceManager
import hr.ferit.petarmaric.taskie.BuildConfig
import hr.ferit.petarmaric.taskie.Taskie

object TaskPrefs {
    /*
    private fun sharedPrefs() = PreferenceManager.getDefaultSharedPreferences(Taskie.getApplicationContext())

    fun storePrefs(key: String, value: String) = sharedPrefs().edit().putString(key, value).apply()
    fun getPrefsString(key: String, defaultValue: String) = sharedPrefs().getString(key, defaultValue)
    */

    private const val sharedPreferencesFile = BuildConfig.APPLICATION_ID

    private fun sharedPrefs() = Taskie.getApplicationContext().getSharedPreferences(sharedPreferencesFile, Context.MODE_PRIVATE)

    fun storePrefs(key: String, value: String) = sharedPrefs().edit().putString(key, value).apply()

    fun getPrefsString(key: String, defaultValue: String) = sharedPrefs().getString(key, defaultValue)
}