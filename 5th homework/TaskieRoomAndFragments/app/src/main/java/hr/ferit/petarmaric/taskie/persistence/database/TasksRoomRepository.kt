package hr.ferit.petarmaric.taskie.persistence.database

import hr.ferit.petarmaric.taskie.model.Priority
import hr.ferit.petarmaric.taskie.model.Task

class TasksRoomRepository : TasksRepository {

    private val tasksDao = TasksDatabase.tasksDao

    override fun save(title: String, description: String, priority: Priority): Task {
        val task = Task(title = title, description = description, priority = priority)
        tasksDao.storeTask(task)
        return task
    }

    override fun get(id: Int): Task {
        return tasksDao.getTask(id)
    }

    override fun getAllTasks(): List<Task> {
        return tasksDao.getAllTasks()
    }

    override fun getSortedTasks(): List<Task> {
        return tasksDao.getTasksOrderedByPriority()
    }

    override fun updateTask(task: Task) {
        tasksDao.updateTask(task)
    }

    override fun deleteTask(task: Task) {
        tasksDao.deleteTask(task)
    }

    override fun deleteAllTasks() {
        tasksDao.deleteAllTasks()
    }

}