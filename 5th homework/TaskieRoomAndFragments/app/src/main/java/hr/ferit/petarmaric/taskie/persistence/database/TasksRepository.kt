package hr.ferit.petarmaric.taskie.persistence.database

import hr.ferit.petarmaric.taskie.model.Priority
import hr.ferit.petarmaric.taskie.model.Task


interface TasksRepository {

    fun save(title: String, description: String, priority: Priority): Task

    fun get(id: Int): Task

    fun getSortedTasks(): List<Task>

    fun getAllTasks(): List<Task>

    fun updateTask(task: Task)

    fun deleteTask(task: Task)

    fun deleteAllTasks()

}