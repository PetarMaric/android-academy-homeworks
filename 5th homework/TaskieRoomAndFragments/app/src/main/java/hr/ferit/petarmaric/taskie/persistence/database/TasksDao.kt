package hr.ferit.petarmaric.taskie.persistence.database

import androidx.room.*
import hr.ferit.petarmaric.taskie.model.Task

@Dao
interface TasksDao {

    @Query("SELECT * FROM tasks")
    fun getAllTasks(): List<Task>

    @Query("SELECT * FROM tasks WHERE id=:id")
    fun getTask(id: Int): Task

    @Query("SELECT * FROM tasks ORDER BY priority")
    fun getTasksOrderedByPriority(): List<Task>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun storeTask(task:Task)

    @Update
    fun updateTask(task: Task)

    @Query("DELETE from tasks")
    fun deleteAllTasks()

    @Delete
    fun deleteTask(task: Task)
}