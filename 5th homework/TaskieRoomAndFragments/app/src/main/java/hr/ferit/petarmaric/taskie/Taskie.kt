package hr.ferit.petarmaric.taskie

import android.app.Application
import androidx.appcompat.app.AppCompatDelegate

class Taskie : Application() {

    companion object {
        private lateinit var instance: Taskie


        fun getApplicationContext() = instance.applicationContext
    }

    override fun onCreate() {
        super.onCreate()
        instance = this
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true)
    }
}