package hr.ferit.petarmaric.taskie.ui.fragments.about

import hr.ferit.petarmaric.taskie.R
import hr.ferit.petarmaric.taskie.ui.fragments.base.BaseFragment

class AboutAuthor : BaseFragment() {
    override fun getLayoutResourceId(): Int = R.layout.fragment_about_author

    companion object {
        fun getInstance(): AboutAuthor = AboutAuthor()
    }
}